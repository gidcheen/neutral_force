use std::error::Error;
use std::fs::create_dir_all;
use std::fs::read_dir;
use std::path::Path;
use ui_dev::shader;

fn main() -> Result<(), Box<dyn Error>> {
	for entry in read_dir("shaders")? {
		let entry = entry?;
		let extension = entry.path().extension().unwrap().to_str().unwrap().to_owned();
		let spv_extension = extension.to_owned() + ".spv";
		if entry.file_type()?.is_file() && (extension == "vert" || extension == "frag") {
			let shader_file = entry.path();
			let spv_file = Path::new("../../assets").join(entry.path().with_extension(spv_extension));

			// if !spv_file.exists() || shader_file.metadata()?.modified()? < spv_file.metadata()?.modified()? {
			create_dir_all(spv_file.parent().unwrap())?;
			shader::compile(&shader_file, &spv_file)?;
			// }
			
			println!("cargo:rerun-if-changed={}", shader_file.to_str().unwrap());
			println!("cargo:rerun-if-changed={}", spv_file.to_str().unwrap());
		}
	}

	Ok(())
}
