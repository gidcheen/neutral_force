use algebra::Vector;
use state::*;
use std::fs;
use ui::mesh::Index;
use ui::mesh::Vertex;
use ui::window::*;
use ui::Ui;

pub struct View {
	ui: Ui,
	window: Window,
}

impl View {
	pub fn new() -> Self {
		let ui = Ui::new("Dusk and Dawn");

		let position = Box::new(|p| println!("{p:?}"));

		let window = ui.window("ahhh", WindowSize { width: 500, height: 500 }, ui::window::WindowState::Windowed, Events { position });

		{
			let _mat = ui.material(&[1, 2, 3, 4, 5]);
			let _tex = ui.texture(&[1u8, 1, 1, 1], 4, 2);
			let _frm = ui.frame(1024, 1024);
			let _pip = ui.pipeline(
				0,
				false,
				&fs::read("assets/shaders/default.vert.spv").unwrap(),
				&fs::read("assets/shaders/default.frag.spv").unwrap(),
			);
			let vertices = vec![
				Vertex {
					position: Vector::new([-0.5, -0.5, 0.3]),
					color: Vector::new([1.0, 1.0, 0.0]),
					..Vertex::default()
				},
				Vertex {
					position: Vector::new([0.5, -0.5, 0.3]),
					color: Vector::new([1.0, 1.0, 0.0]),
					..Vertex::default()
				},
				Vertex {
					position: Vector::new([0.0, 0.5, 0.3]),
					color: Vector::new([1.0, 1.0, 0.0]),
					..Vertex::default()
				},
				Vertex {
					position: Vector::new([-0.5 + 1.2, -0.5, -2.5]),
					color: Vector::new([1.0, 0.0, 0.0]),
					..Vertex::default()
				},
				Vertex {
					position: Vector::new([0.5 + 1.2, -0.5, -2.5]),
					color: Vector::new([1.0, 0.0, 0.0]),
					..Vertex::default()
				},
				Vertex {
					position: Vector::new([0.0 + 1.2, 0.5, -2.5]),
					color: Vector::new([1.0, 0.0, 0.0]),
					..Vertex::default()
				},
				Vertex {
					position: Vector::new([-0.5 - 1.2, -0.5, -3.8]),
					color: Vector::new([1.0, 0.0, 1.0]),
					..Vertex::default()
				},
				Vertex {
					position: Vector::new([0.5 - 1.2, -0.5, -3.8]),
					color: Vector::new([1.0, 0.0, 1.0]),
					..Vertex::default()
				},
				Vertex {
					position: Vector::new([0.0 - 1.2, 0.5, -3.8]),
					color: Vector::new([1.0, 0.0, 1.0]),
					..Vertex::default()
				},
			];
			let indices = vec![Index { face: [0, 1, 2] }];
			let _me = ui.mesh(&vertices, &indices);
		}

		Self { ui, window }
	}

	pub fn update(&mut self, state: &mut State, _dt: f32) {
		if self.window.should_close() {
			state.quit();
		}
		self.ui.poll_events();
	}
}
