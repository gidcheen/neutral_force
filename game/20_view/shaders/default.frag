#version 450 core

#include <frag.glsl>

// uniform sampler2D tex;

// uniform sampler2D colorTexture;
// uniform sampler2D metallicRoughnessTexture;
// uniform sampler2D normalTexture;
// uniform sampler2D occlusionTexture;
// uniform sampler2D emissiveTexture;

// uniform vec4 color;
// uniform float metalness;
// uniform float roughness;
// uniform vec4 emission;

SurfaceData GetSurfaceData()
{
	return SurfaceData(
		vec4(0,0,0,0),	//color * texture(colorTexture, vertToFrag.uv).rgba,
		vec3(0,0,0),	//metalness * texture(metallicRoughnessTexture, vertToFrag.uv).rgb,
		0,	//roughness * texture(metallicRoughnessTexture, vertToFrag.uv).a,
		vec3(0,0,0),	//emission * texture(colorTexture, vertToFrag.uv).rgb,
		vec3(0,0,0)	//texture(normalTexture, vertToFrag.uv).rgb
	);
}
