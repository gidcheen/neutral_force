use std::collections::VecDeque;
use std::time::*;

pub struct DeltaTime {
	durations: VecDeque<Duration>,
	last: Instant,
}

impl DeltaTime {
	pub fn new(smoothing: u16) -> Self {
		let d = Duration::from_secs_f64(1f64 / 64f64);
		let mut durations = VecDeque::with_capacity(smoothing as _);
		for _ in 0..smoothing {
			durations.push_back(d);
		}
		Self {
			durations,
			last: Instant::now() - d,
		}
	}

	pub fn step(&mut self) -> f64 {
		let now = Instant::now();
		let delta = now - self.last;
		self.last = now;

		self.durations.pop_back();
		self.durations.push_front(delta);

		let smoothed = self.durations.iter().sum::<Duration>();
		let smoothed = smoothed / self.durations.len() as u32;

		smoothed.as_secs_f64()
	}
}
