
// fn main() {
// let graphics = Graphics::new();
//
// let window = Window::new(
// 	graphics.clone(),
// 	"dusk and dawn",
// 	Size { width: 1600, height: 900 },
// 	WindowCreation::Maximized,
// );
// window.set_vsync(true);
// window.maximize();
//
// let mut renderer = Renderer::new(graphics.clone());
// let n1 = renderer.node(Vector3f::new([1.0, 1.0, 1.0]), Quaternion::identity(), Vector3f::new([1.0, 1.0, 1.0]), None);
// let n2 = renderer.node(forward() * 3.0, Quaternion::identity(), Vector3f::new([1.0, 1.0, 1.0]), Some(n1));
//
// let executor = Executor::new(graphics.clone());
//
// let vert = Program::new(graphics.clone(), &[&std::fs::read_to_string("assets/shaders/test.vert").unwrap()], ProgramKind::Vertex);
// let frag = Program::new(graphics.clone(), &[&std::fs::read_to_string("assets/shaders/test.frag").unwrap()], ProgramKind::Fragment);
// let pipeline = Pipeline::new(graphics.clone(), PipelineConfig {
// 	blending: Some((PipelineBlendFunc::SrcAlpha, PipelineBlendFunc::OneMinusSrcAlpha)),
// 	culling: None,
// 	depth_testing: Some(PipelineDepthFunc::Less),
// 	depth_writing: true,
// });
// pipeline.set_program(vert);
// pipeline.set_program(frag);
//
// let Size { width, height } = window.size();
//
// // let frame_size = TextureSize::Image2D { width, height };
// let frame_size = TextureSize::Multisample { width, height, samples: 8 };
// let color_texture = Texture::new(graphics.clone(), frame_size, TextureChannels::Rgba);
// let depth_texture = Texture::new(graphics.clone(), frame_size, TextureChannels::Depth);
// let mut frame = Frame::new(graphics.clone(), FrameSize { width, height });
// frame.set_color(0, color_texture);
// frame.set_depth(depth_texture);
//
// #[repr(C)]
// #[derive(Copy, Clone, Default)]
// struct Vertex {
// 	pos: Vector<f32, 3>,
// 	_pad1: f32,
// 	col: Vector<f32, 3>,
// 	_pad2: f32,
// }
//
// let vertices = [
// 	Vertex { pos: Vector::new([-0.5, -0.5, 0.3]), col: Vector::new([1.0, 1.0, 0.0]), ..Vertex::default() },
// 	Vertex { pos: Vector::new([0.5, -0.5, 0.3]), col: Vector::new([1.0, 1.0, 0.0]), ..Vertex::default() },
// 	Vertex { pos: Vector::new([0.0, 0.5, 0.3]), col: Vector::new([1.0, 1.0, 0.0]), ..Vertex::default() },
// 	Vertex { pos: Vector::new([-0.5 + 1.2, -0.5, -2.5]), col: Vector::new([1.0, 0.0, 0.0]), ..Vertex::default() },
// 	Vertex { pos: Vector::new([0.5 + 1.2, -0.5, -2.5]), col: Vector::new([1.0, 0.0, 0.0]), ..Vertex::default() },
// 	Vertex { pos: Vector::new([0.0 + 1.2, 0.5, -2.5]), col: Vector::new([1.0, 0.0, 0.0]), ..Vertex::default() },
// 	Vertex { pos: Vector::new([-0.5 - 1.2, -0.5, -3.8]), col: Vector::new([1.0, 0.0, 1.0]), ..Vertex::default() },
// 	Vertex { pos: Vector::new([0.5 - 1.2, -0.5, -3.8]), col: Vector::new([1.0, 0.0, 1.0]), ..Vertex::default() },
// 	Vertex { pos: Vector::new([0.0 - 1.2, 0.5, -3.8]), col: Vector::new([1.0, 0.0, 1.0]), ..Vertex::default() },
// ];
// let vertex_buffer = Buffer::new(graphics.clone(), size_of_val(&vertices), BufferKind::Vertex);
// vertex_buffer.set(&vertices);
//
// let indices = [0u16, 1, 2, 3, 4, 5, 6, 7, 8];
// let index_buffer = Buffer::new(graphics.clone(), size_of_val(&indices), BufferKind::Index);
// index_buffer.set(&indices);
//
// let mesh = Mesh::new(graphics.clone());
// mesh.set_vertices(0, MeshVertices { buffer: vertex_buffer.clone(), offset: 0, stride: size_of_val(&vertices[0]), divisor: None });
// mesh.set_vertices(1, MeshVertices { buffer: vertex_buffer.clone(), offset: size_of_val(&vertices[0].pos) + size_of::<f32>(), stride: size_of_val(&vertices[0]), divisor: None });
// mesh.set_indices(MeshIndices { buffer: index_buffer.clone(), format: MeshIndexFormat::Short });
// mesh.set_count(indices.len());
//
// let mut times = [1.0 / 75.0; 6];
// let mut fps_time = 0.0;
// let mut rot = 0.0;
//
// while !window.should_close() {
// 	let start = Instant::now();
//
// 	graphics.poll_events();
// 	for e in window.consume_events() {
// 		match e {
// 			Event::KeyboardKey(KeyboardKey::Escape, InputState::Down) => {
// 				window.close()
// 			}
// 			Event::KeyboardKey(KeyboardKey::KeyD, InputState::Down) => {
// 				if window.is_fullscreen() {
// 					window.leave_fullscreen(Position { x: 100, y: 100 }, Size { width: 1600, height: 900 });
// 				} else {
// 					window.make_fullscreen(0, None);
// 				}
// 			}
// 			Event::KeyboardKey(KeyboardKey::KeyA, InputState::Down) => {
// 				window.maximize();
// 			}
// 			Event::KeyboardKey(KeyboardKey::KeyS, InputState::Down) => {
// 				window.restore();
// 			}
// 			Event::KeyboardKey(KeyboardKey::KeyW, InputState::Down) => {
// 				window.minimize();
// 			}
// 			Event::WindowSize(Size { width, height }) => {
// 				if width >= 1 && height >= 1 {
// 					// let frame_size = TextureSize::Image2D { width, height };
// 					let frame_size = TextureSize::Multisample { width, height, samples: 8 };
// 					let color_texture = Texture::new(graphics.clone(), frame_size, TextureChannels::Rgba);
// 					let depth_texture = Texture::new(graphics.clone(), frame_size, TextureChannels::Depth);
// 					frame = Frame::new(graphics.clone(), FrameSize { width, height });
// 					frame.set_color(0, color_texture.clone());
// 					frame.set_depth(depth_texture);
// 				}
// 			}
// 			_ => {}
// 		}
// 	}
//
// 	let dt = times.iter().sum::<f32>() / times.len() as f32;
// 	fps_time += dt;
// 	rot += dt;
// 	let aspect = frame.size().width as f32 / frame.size().height as f32;
//
// 	let commands = [
// 		Command::Clear {
// 			color: ClearColor::default(),
// 			frame: frame.clone(),
// 		},
// 		Command::Bind {
// 			pipeline: pipeline.clone(),
// 			uniforms: vec![
// 				Binding {
// 					location: CString::new("model").unwrap(),
// 					value: Uniform::Matrix4(trs(
// 						Vector::new([0.0, 0.0, -5.0]),
// 						Quaternion::from_axis_angle(Vector::new([0.0, 1.0, 0.0]), rot),
// 						Vector::new([1.0, 1.0, 1.0]),
// 					).elements),
// 				},
// 				Binding {
// 					location: CString::new("projection").unwrap(),
// 					value: Uniform::Matrix4(perspective(90f32, aspect, 0.1, 90.0).elements),
// 				},
// 			],
// 			buffers: vec![],
// 			textures: vec![],
// 		},
// 		Command::Draw {
// 			frame: frame.clone(),
// 			mesh: mesh.clone(),
// 			instances: None,
// 		},
// 		Command::Present {
// 			frame: frame.clone(),
// 			window: window.clone(),
// 		},
// 	];
//
// 	executor.run(&commands);
//
// 	if fps_time >= 0.5 {
// 		window.set_title(&format!("dusk and dawn fps: {}", 1.0 / dt));
// 		fps_time = 0.0;
// 	}
//
// 	times.rotate_right(1);
// 	let end = Instant::now();
// 	times[0] = (end - start).as_secs_f32();
// }
// }
