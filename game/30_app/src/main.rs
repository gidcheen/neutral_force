use delta_time::DeltaTime;
use state::*;
use std::thread;
use std::time::Duration;
use view::*;

mod delta_time;

fn main() {
	let mut state = State::new();
	let mut view = View::new();
	let mut dt = DeltaTime::new(10);
	
	while state.is_running() {
		let dt = dt.step() as f32;
		state.update(dt);
		view.update(&mut state, dt);

		// println!("{dt}");
		thread::sleep(Duration::from_millis(16));
	}
}
