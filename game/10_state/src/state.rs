mod ai;
mod entity;
mod level;

use std::ops::{Add, Mul};

use crate::entity::{BulletEntity, EnemyEntity, PlayerEntity};
use crate::level::Level;

pub struct State {
	running: bool,
	time: f32,
	level: Level,
	player: PlayerEntity,
	enemy_entities: Vec<EnemyEntity>,
	bullets_player: Vec<BulletEntity>,
	// what weapon does the player have ?
	is_group_1: bool,
	// indeces in enemy_entities after update call.
	enemy_entities_spawns: Vec<usize>,
	// indeces in enemy_ deaths after update call.
	enemy_entities_deaths: Vec<usize>,

	// indeces in enemy_ deaths after update call.
	bullets_deaths: Vec<usize>,
}

impl State {
	pub fn new() -> Self {
		Self {
			running: true,
			time: 0.0,
			level: Level::new(),
			player: PlayerEntity::new(),
			enemy_entities: Vec::new(),
			bullets_player: Vec::new(),
			is_group_1: true,
			enemy_entities_spawns: Vec::new(),
			enemy_entities_deaths: Vec::new(),
			bullets_deaths: Vec::new(),
		}
	}

	pub fn update(&mut self, dt: f32) {
		self.time += dt;
		if self.time > 60.0 {
			self.running = false;
		}

		// cleanup: remove dead Entities.
		self.enemy_entities.retain(|e| e.entity().is_alive());

		// cleanup: remove dead Bullets.
		self.bullets_player.retain(|e| e.entity().is_alive());

		// process bullet enemy and player travelling.
		self.move_entities(dt);

		// process if any bullet hit's an entity (in group_1 or group_2).
		self.process_bullet_hits();

		//self.process_hits();
		// check if an entity hits the player.
		// process player ship travel.
		// process group_1 and group_2 travel.

		self.process_level_step();
	}

	pub fn is_running(&self) -> bool {
		self.running
	}

	pub fn quit(&mut self) {
		self.running = false;
	}

	// process level steps
	pub fn process_level_step(&mut self) {
		self.enemy_entities_spawns = self.level.do_spawns(self.time, &mut self.enemy_entities);
	}

	fn move_entities(&mut self, dt: f32) {
		for bullet in &mut self.bullets_player {
			bullet.entity_mut().move_entity(dt);
		}

		for enemy in &mut self.enemy_entities {
			enemy.entity_mut().move_entity(dt);
		}

		self.player.entity_mut().move_entity(dt);
	}

	fn process_bullet_hits(&mut self) {
		let bullet_radius = 10;
		let enemey_radius = 10; //enemy radius might increase with bullet size.
		let inc_per_hit: f32 = 0.2;

		self.bullets_deaths.clear();

		for e in &mut self.enemy_entities {
			for ib in 0..self.bullets_player.len() {
				let b = &mut self.bullets_player[ib];
				let enemy_val = e.current_value();
				let bullettype = b.is_positive();
				let e_pos = e.entity().position();
				if e_pos.add(b.entity().position().mul(-1.0)).len() <= bullet_radius + enemey_radius {
					println!("bullet hit entity at {} {} - {}", e_pos[0], e_pos[1], enemy_val);
					// the bullet dies anyway.
					// TODO: remember bullet hits for UI Processing.
					b.entity_mut().set_death();
					if bullettype {
						e.hit_enemy(inc_per_hit);
					} else {
						e.hit_enemy(-inc_per_hit);
					}
					self.bullets_deaths.push(ib);
				}
			}
		}
	}
}
