use algebra::Vector2f;

use crate::entity::*;

struct LevelSpawn {
	spawn_time: f32,
	spawn_position: Vector2f,

	// value that determines the how neutral the spawned entity is.
	// usual values between -1 and +1.
	// higher/lower values are more like bosses.
	value: f32,
}

pub struct Level {
	spawns: Vec<LevelSpawn>,
	current_index: usize,
}

impl Level {
	pub fn new() -> Self {
		Self {
			spawns: vec![LevelSpawn {
				spawn_time: 10.0,
				value: 1.0,
				spawn_position: Vector2f::new([10.0, 0.0]),
			}],
			current_index: 0,
		}

		// self.spawns.push(LevelSpawn { spawn_time: 10.0, spawn_position: Vector2f::new([10.0, 10.0])});
	}

	/**
	 * spawns the entites
	 * returns a vector with the indezes of spawned entities.
	 */
	pub fn do_spawns(&mut self, time: f32, enemies: &mut Vec<EnemyEntity>) -> Vec<usize> {
		let mut result = Vec::new();

		while self.current_index < self.spawns.len() {
			let spawn = &self.spawns[self.current_index];
			if spawn.spawn_time <= time {
				let x = spawn.spawn_position.x();
				let y = spawn.spawn_position.y();
				let e = EnemyEntity::new(x, y, spawn.value);
				result.push(enemies.len());
				enemies.push(e);
				println!("spawned enemy at {time} on {x} - {y}");
				self.current_index += 1;
			} else {
				// nothing to do anymore.
				return result;
			}
		}

		return result;
	}
}
