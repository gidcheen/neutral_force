// mod state;

use algebra::Vector2f;

use crate::entity::{EnemyEntity, PlayerEntity};
//use crate::state::State;

pub trait AI {
	// returns a desired direction for movement.
	// the caller takes care of restrictions like max velocity.
	fn get_movement(&mut self, entity: &EnemyEntity, player: &PlayerEntity) -> Vector2f;
}

pub struct FollowPlayerAI {}

impl AI for FollowPlayerAI {
	fn get_movement(&mut self, entity: &EnemyEntity, player: &PlayerEntity) -> Vector2f {
		*player.entity().position() - *entity.entity().position()
	}
}
