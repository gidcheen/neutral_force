use algebra::Vector2f;

#[derive(PartialEq)]
pub enum EntityType {
	Player,
	// state of the enemy. game target is to reach near zero. value between -1..1 are normal, higher values are possible.
	Enemy(f32),
	// this entity is marked as dead, and should be deleted at the beginning of next tick.
	Dead,
	// bool: f32 time to live
	Bullet(bool, f32),
}

pub struct Entity {
	position: Vector2f,
	velocity: Vector2f,
	is_dead: bool, // entitytype: EntityType,
	               // dead Entities should be collected.
}

pub struct BulletEntity {
	entity: Entity,

	// positive or negative bullet ?
	is_positive: bool,
	// maximum time to stay alive.
	max_time: f32,
}

pub struct EnemyEntity {
	entity: Entity,
	current_value: f32,
}

pub struct PlayerEntity {
	entity: Entity,
}

impl PlayerEntity {
	pub fn new() -> Self {
		Self { entity: Entity::new() }
	}

	pub fn entity_mut(&mut self) -> &mut Entity {
		&mut self.entity
	}

	pub fn entity(&self) -> &Entity {
		&self.entity
	}
}

impl EnemyEntity {
	pub fn new(x: f32, y: f32, value: f32) -> Self {
		Self {
			entity: Entity::new_xy(x, y),
			current_value: value,
		}
	}

	pub fn hit_enemy(&mut self, hit_value: f32) {
		self.current_value += hit_value;
	}

	pub fn entity_mut(&mut self) -> &mut Entity {
		&mut self.entity
	}

	pub fn entity(&self) -> &Entity {
		&self.entity
	}

	pub fn current_value(&self) -> f32 {
		self.current_value
	}
}

impl BulletEntity {
	pub fn is_positive(&self) -> bool {
		self.is_positive
	}
	pub fn entity_mut(&mut self) -> &mut Entity {
		&mut self.entity
	}

	pub fn entity(&self) -> &Entity {
		&self.entity
	}
}

impl Entity {
	pub fn new() -> Self {
		Self {
			position: Vector2f::zeroed(),
			velocity: Vector2f::zeroed(),
			is_dead: false,
		}
	}

	pub fn new_xy(x: f32, y: f32) -> Self {
		Self {
			position: Vector2f::new([x, y]),
			velocity: Vector2f::zeroed(),
			is_dead: false,
		}
	}

	pub fn move_entity(&mut self, dt: f32) {
		self.position[0] = self.position[0] + self.velocity[0] * dt;
		self.position[1] = self.position[1] + self.velocity[1] * dt;
	}

	pub fn position(&self) -> &Vector2f {
		return &self.position;
	}

	pub fn is_alive(&self) -> bool {
		return !self.is_dead;
	}

	pub fn set_death(&mut self) {
		self.is_dead = true;
	}
}
