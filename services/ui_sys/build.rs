use std::error::Error;
use std::fs;
use std::{env, path::*, process::*, str::from_utf8};

#[cfg(debug_assertions)]
const DEBUG_ARG: &str = "-g";
#[cfg(not(debug_assertions))]
const DEBUG_ARG: &str = "-O2";

pub const BINDGEN: &str = "bindgen";

const LIB_NAME: &str = "ui";

#[cfg(target_os = "linux")]
mod platform {
	pub const CLANG: &'static str = "clang";
	pub const AR: &'static str = "ar";
	pub const LIB_PREFIX: &str = "lib";
	pub const LIB_ENDING: &str = "a";
	pub const ARGS: &'static [&str] = &["-DLINUX", "-fPIC"];
	pub const LIBS: &[&'static str] = &["glfw3", "vulkan", "X11", "Xcursor"];
}
#[cfg(target_os = "macos")]
mod platform {
	pub const CLANG: &'static str = "clang";
	pub const AR: &'static str = "ar";
	pub const LIB_PREFIX: &str = "lib";
	pub const LIB_ENDING: &str = "a";
	pub const ARGS: &'static [&str] = &["-DMACOS", "-fPIC", "-xobjective-c"];
	pub const LIBS: &[&'static str] = &["glfw3", "vulkan", "framework=Cocoa", "framework=IOKit"];
}
#[cfg(target_os = "windows")]
mod platform {
	pub const CLANG: &'static str = "clang";
	pub const AR: &'static str = "llvm-ar";
	pub const LIB_PREFIX: &str = "";
	pub const LIB_ENDING: &str = "lib";
	pub const ARGS: &'static [&str] = &["-DWINDOWS"];
	pub const LIBS: &[&'static str] = &["glfw3", "vulkan-1", "kernel32", "user32", "gdi32", "shell32"];
}

use platform::*;

fn main() -> Result<(), Box<dyn Error>> {
	#[cfg(target_os = "windows")]
	let vulkan_sdk = std::env::var("VULKAN_SDK").unwrap();
	#[cfg(target_os = "windows")]
	println!("cargo:rustc-link-search={vulkan_sdk}/lib");

	let args: &[String] = &[
		#[cfg(target_os = "windows")]
		format!("-I{vulkan_sdk}/Include"),
	];

	let out_dir = PathBuf::new().join(&env::var("OUT_DIR")?);
	fs::create_dir_all(&out_dir)?;

	let h_file = "src/ui.h";
	let c_file = "src/ui.c";
	let o_file = out_dir.join(c_file).with_extension("o");
	let bind_file = "src/bindings.rs";
	let lib_file = out_dir.join(format!("{LIB_PREFIX}{LIB_NAME}.{LIB_ENDING}"));

	println!("cargo:rerun-if-changed={c_file}");

	fs::create_dir_all(o_file.parent().unwrap())?;
	let output = Command::new(CLANG)
		.arg(c_file)
		.arg("-c")
		.arg(DEBUG_ARG)
		.arg("-std=c17")
		.args(ARGS)
		.args(args)
		.arg("-o")
		.arg(&o_file)
		.output()?;
	check_output(&output)?;

	let output = Command::new(AR).arg("r").arg(&lib_file).arg(o_file).output()?;
	check_output(&output)?;

	let out_dir = out_dir.to_str().unwrap();
	println!("cargo:rustc-link-search={out_dir}");
	println!("cargo:rustc-link-lib={LIB_NAME}");

	for lib in LIBS {
		println!("cargo:rustc-link-lib={lib}");
	}
	println!(
		"cargo:rustc-link-search={lib_dir}",
		lib_dir = fs::canonicalize(Path::new("lib").join(env::consts::OS)).unwrap().to_str().unwrap()
	);

	let output = Command::new(BINDGEN)
		.arg("--no-prepend-enum-name")
		.arg("--no-layout-tests")
		.arg("--with-derive-default")
		.arg("--default-enum-style")
		.arg("rust")
		.arg("--size_t-is-usize")
		.arg("--raw-line")
		.arg("#![allow(non_upper_case_globals)]")
		.arg("--raw-line")
		.arg("#![allow(non_camel_case_types)]")
		.arg("--raw-line")
		.arg("#![allow(non_snake_case)]")
		.arg("--raw-line")
		.arg("#![allow(dead_code)]")
		.arg("--whitelist-function")
		.arg("[vV][kK].*")
		.arg("--whitelist-type")
		.arg("[vV][kK].*")
		.arg("--whitelist-var")
		.arg("[vV][kK].*")
		.arg("--whitelist-function")
		.arg("[gG][lL][fF][wW].*")
		.arg("--whitelist-type")
		.arg("[gG][lL][fF][wW].*")
		.arg("--whitelist-var")
		.arg("[gG][lL][fF][wW].*")
		.arg("--whitelist-function")
		.arg("[sS][tT][bB].*")
		.arg("--whitelist-type")
		.arg("[sS][tT][bB].*")
		.arg("--whitelist-var")
		.arg("[sS][tT][bB].*")
		.arg("--whitelist-function")
		.arg("[mM][aA].*")
		.arg("--whitelist-type")
		.arg("[mM][aA].*")
		.arg("--whitelist-var")
		.arg("[mM][aA].*")
		.arg("-o")
		.arg(bind_file)
		.arg(h_file)
		.arg("--")
		.args(args)
		.output()?;
	check_output(&output)?;

	Ok(())
}

fn check_output(output: &Output) -> Result<(), Box<dyn Error>> {
	if !output.status.success() {
		panic!("{}", from_utf8(&output.stderr)?);
	}
	Ok(())
}
