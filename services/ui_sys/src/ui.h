#pragma once

#include <stdint.h>

#define STBI_NO_PNM
#define STBI_NO_GIF
#define STBI_NO_PIC
#define STBI_NO_PSD
#define STBI_NO_TGA
#define STBI_NO_BMP
#define STBIW_WINDOWS_UTF8
#define STB_VORBIS_HEADER_ONLY

#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN

#include "stb_image.h"
#include "stb_image_resize.h"
#include "stb_image_write.h"
#include "stb_vorbis.c" 
#include "miniaudio.h"
#include "glfw3.h"
#include <vulkan/vulkan.h>

uint32_t vkMakeApiVersion(uint32_t variant, uint32_t major, uint32_t minor, uint32_t patch);
