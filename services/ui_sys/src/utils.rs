use crate::VkResult;

impl VkResult {
	#[inline]
	pub fn success(self) {
		assert_eq!(self, VkResult::VK_SUCCESS);
	}
}
