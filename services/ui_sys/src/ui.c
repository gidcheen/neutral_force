#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#define MINIAUDIO_IMPLEMENTATION

#include "ui.h"

#undef STB_VORBIS_HEADER_ONLY
#include "stb_vorbis.c"

uint32_t vkMakeApiVersion(uint32_t variant, uint32_t major, uint32_t minor, uint32_t patch) {
	return VK_MAKE_API_VERSION(variant, major, minor, patch);
}

unsigned long NvOptimusEnablement = 1;
int AmdPowerXpressRequestHighPerformance = 1;
