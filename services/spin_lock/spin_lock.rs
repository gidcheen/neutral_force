use std::cell::UnsafeCell;
use std::hint::spin_loop;
use std::ops::{Deref, DerefMut};
use std::sync::atomic::*;

pub struct SpinLock<T: ?Sized> {
	lock: AtomicBool,
	data: UnsafeCell<T>,
}

pub struct SpinLockGuard<'a, T: ?Sized + 'a> {
	spin_lock: &'a SpinLock<T>,
}

impl<T> SpinLock<T> {
	pub const fn new(value: T) -> Self {
		Self {
			data: UnsafeCell::new(value),
			lock: AtomicBool::new(false),
		}
	}
}

impl<T: ?Sized> SpinLock<T> {
	pub fn lock(&self) -> SpinLockGuard<T> {
		SpinLockGuard::new(self)
	}
}

impl<'a, T: ?Sized> SpinLockGuard<'a, T> {
	pub fn new(spin_lock: &'a SpinLock<T>) -> Self {
		while spin_lock.lock.compare_exchange(false, true, Ordering::Acquire, Ordering::Relaxed) != Ok(false) {
			spin_loop();
		}

		Self { spin_lock }
	}
}

impl<'a, T: ?Sized> Drop for SpinLockGuard<'a, T> {
	fn drop(&mut self) {
		self.spin_lock.lock.store(false, Ordering::Release);
	}
}

impl<'a, T> Deref for SpinLockGuard<'a, T> {
	type Target = T;

	fn deref(&self) -> &Self::Target {
		unsafe { &*self.spin_lock.data.get() }
	}
}

impl<'a, T> DerefMut for SpinLockGuard<'a, T> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		unsafe { &mut *self.spin_lock.data.get() }
	}
}

unsafe impl<T> Send for SpinLock<T> {}
unsafe impl<T> Sync for SpinLock<T> {}
