use std::marker::PhantomData;
use std::mem::swap;

pub struct StorageHandle<T> {
	index: usize,
	gen: usize,
	_phantom: PhantomData<T>,
}

impl<T> StorageHandle<T> {
	fn new(index: usize, gen: usize) -> StorageHandle<T> {
		StorageHandle {
			index,
			gen,
			_phantom: PhantomData,
		}
	}
}

impl<T> Clone for StorageHandle<T> {
	fn clone(&self) -> Self {
		Self {
			index: self.index,
			gen: self.gen,
			_phantom: PhantomData,
		}
	}
}

impl<T> Eq for StorageHandle<T> {}

impl<T> PartialEq for StorageHandle<T> {
	fn eq(&self, other: &Self) -> bool {
		self.index == other.index && self.gen == other.gen
	}
}

#[derive(Clone)]
struct StorageElement<T> {
	gen: usize,
	value: Option<T>,
}

#[derive(Clone)]
pub struct Storage<T> {
	elements: Vec<StorageElement<T>>,
}

impl<T> Storage<T> {
	pub fn new() -> Self {
		Self { elements: Vec::new() }
	}

	pub fn insert(&mut self, value: T) -> StorageHandle<T> {
		for (i, mut element) in self.elements.iter_mut().enumerate() {
			let is_free = element.value.is_none();
			if is_free {
				element.gen += 1;
				element.value = Some(value);
				return StorageHandle::new(i, element.gen);
			}
		}
		self.elements.push(StorageElement { gen: 0, value: Some(value) });
		return StorageHandle::new(self.elements.len() - 1, 0);
	}

	pub fn remove(&mut self, handle: StorageHandle<T>) -> Option<T> {
		if let Some(element) = self.elements.get_mut(handle.index) {
			if element.gen == handle.gen {
				let mut ret = None;
				swap(&mut element.value, &mut ret);
				ret
			} else {
				None
			}
		} else {
			None
		}
	}

	pub fn get(&self, handle: StorageHandle<T>) -> Option<&T> {
		let element = self.elements.get(handle.index)?;
		if element.gen == handle.gen {
			element.value.as_ref()
		} else {
			None
		}
	}

	pub fn get_mut(&mut self, handle: StorageHandle<T>) -> Option<&mut T> {
		if let Some(element) = self.elements.get_mut(handle.index) {
			if element.gen == handle.gen {
				element.value.as_mut()
			} else {
				None
			}
		} else {
			None
		}
	}

	pub fn iter(&self) -> impl Iterator<Item = &T> {
		self.elements.iter().filter_map(|e| e.value.as_ref())
	}

	pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> {
		self.elements.iter_mut().filter_map(|e| e.value.as_mut())
	}
}

// impl <T> IntoIterator for Storage<T>{
//     type Item = T;
//     type IntoIter = ();
//
//     fn into_iter(self) -> Self::IntoIter {
//         todo!()
//     }
// }

impl<T> Default for Storage<T> {
	fn default() -> Self {
		Self::new()
	}
}
