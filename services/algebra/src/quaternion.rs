use std::ops::*;

use crate::value::Val;
use crate::Vector;

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct Quaternion<T: Val> {
	elements: [T; 4],
}

impl<T: Val> Quaternion<T> {
	pub fn new(elements: [T; 4]) -> Self {
		Self {
			elements,
		}
	}

	pub fn identity() -> Self {
		Self {
			elements: [T::ZERO, T::ZERO, T::ZERO, T::ONE],
		}
	}

	pub fn from_axis_angle(axis: Vector<T, 3>, angle: T) -> Self {
		let axis = axis.normalized();
		let half_angle = angle * T::HALF;

		let vector3 = axis * (half_angle).sin();
		let w = (half_angle).cos();

		Self::new([vector3.x(), vector3.y(), vector3.z(), w]).normalized()
	}

	pub fn x(&self) -> T {
		self[0]
	}
	pub fn y(&self) -> T {
		self[1]
	}
	pub fn z(&self) -> T {
		self[2]
	}
	pub fn w(&self) -> T {
		self[3]
	}
	pub fn x_ref(&self) -> &T {
		&self[0]
	}
	pub fn y_ref(&self) -> &T {
		&self[1]
	}
	pub fn z_ref(&self) -> &T {
		&self[2]
	}
	pub fn w_ref(&self) -> &T {
		&self[3]
	}
	pub fn x_mut(&mut self) -> &mut T {
		&mut self[0]
	}
	pub fn y_mut(&mut self) -> &mut T {
		&mut self[1]
	}
	pub fn z_mut(&mut self) -> &mut T {
		&mut self[2]
	}
	pub fn w_mut(&mut self) -> &mut T {
		&mut self[3]
	}

	pub fn length_squared(&self) -> T {
		self.iter().fold(T::ZERO, |e, v| e + *v * *v)
	}

	pub fn length(&self) -> T {
		self.length_squared().sqrt()
	}

	pub fn invert(&mut self) {
		*self.x_mut() *= -T::ONE;
		*self.y_mut() *= -T::ONE;
		*self.z_mut() *= -T::ONE;
	}

	pub fn inverted(mut self) -> Self {
		self.invert();
		self
	}

	pub fn normalize(&mut self) {
		let inverse = T::ONE / self.to_vector().length();
		*self.x_mut() *= inverse;
		*self.y_mut() *= inverse;
		*self.z_mut() *= inverse;
		*self.w_mut() *= inverse;
	}

	pub fn normalized(mut self) -> Self {
		self.normalize();
		self
	}

	pub fn to_vector(self) -> Vector<T, 4> {
		Vector::new([self.x(), self.y(), self.z(), self.w()])
	}
}

impl<T: Val> Default for Quaternion<T> {
	fn default() -> Self {
		Self::identity()
	}
}

impl<T: Val> From<[T; 4]> for Quaternion<T> {
	fn from(v: [T; 4]) -> Self {
		Quaternion::new(v)
	}
}

impl<T: Val> Deref for Quaternion<T> {
	type Target = [T; 4];

	fn deref(&self) -> &Self::Target {
		&self.elements
	}
}

impl<T: Val> DerefMut for Quaternion<T> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.elements
	}
}

impl<T: Val> MulAssign<Quaternion<T>> for Quaternion<T> {
	fn mul_assign(&mut self, rhs: Quaternion<T>) {
		let lhs = *self;

		let lhs_v = Vector::<T, 3>::new([lhs.x(), lhs.y(), lhs.z()]);
		let rhs_v = Vector::<T, 3>::new([rhs.x(), rhs.y(), rhs.z()]);

		let w = lhs.w() * rhs.w() - lhs_v.dot(rhs_v);

		let a = lhs_v * rhs.w();
		let b = rhs_v * lhs.w();
		let c = lhs_v.cross(rhs_v);
		let abc = (a + b) + c;

		*self = Quaternion::new([abc.x(), abc.y(), abc.z(), w]);
	}
}

impl<T: Val> DivAssign<Quaternion<T>> for Quaternion<T> {
	fn div_assign(&mut self, rhs: Quaternion<T>) {
		*self *= rhs.inverted()
	}
}

impl<T: Val> Mul for Quaternion<T> {
	type Output = Quaternion<T>;

	fn mul(mut self, rhs: Self) -> Self::Output {
		self *= rhs;
		self
	}
}

impl<T: Val> Div for Quaternion<T> {
	type Output = Quaternion<T>;

	fn div(mut self, rhs: Self) -> Self::Output {
		self /= rhs;
		self
	}
}
