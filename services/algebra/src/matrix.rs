use std::fmt::Debug;
use std::ops::*;

use crate::value::Val;
use crate::Vector;

#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct Matrix<T: Val, const X: usize, const Y: usize> {
	elements: [[T; X]; Y],
}

impl<T: Val, const X: usize, const Y: usize> Matrix<T, X, Y> {
	pub fn new(elements: [[T; X]; Y]) -> Self {
		Self { elements }
	}

	pub fn zeroed() -> Self {
		Self { elements: [[T::ZERO; X]; Y] }
	}

	pub fn identity() -> Self {
		Self::with_diagonal(T::ONE)
	}

	pub fn with_diagonal(value: T) -> Self {
		let mut ret = Self::zeroed();
		for d in 0..Y.min(X) {
			ret[d][d] = value;
		}
		ret
	}

	pub fn transposed(&self) -> Matrix<T, Y, X> {
		let mut ret = Matrix::zeroed();
		for y in 0..Y {
			for x in 0..X {
				ret[x][y] = self[y][x];
			}
		}
		ret
	}

	pub fn scale(&mut self, other: Self) {
		for y in 0..Y {
			for x in 0..X {
				self[y][x] *= other[y][x];
			}
		}
	}

	pub fn scaled(mut self, other: Self) -> Self {
		self.scale(other);
		self
	}

	pub fn column(&self, col: usize) -> [T; X] {
		let mut ret = [T::ZERO; X];
		for x in 0..X {
			ret[x] = self[col][x];
		}
		ret
	}

	pub fn row(&self, row: usize) -> [T; Y] {
		let mut ret = [T::ZERO; Y];
		for y in 0..Y {
			ret[y] = self[y][row];
		}
		ret
	}

	pub fn min(mut self, other: &Self) -> Self {
		for y in 0..Y {
			for x in 0..X {
				self[y][x] = self[y][x].min(other[y][x]);
			}
		}
		self
	}

	pub fn max(mut self, other: &Self) -> Self {
		for y in 0..Y {
			for x in 0..X {
				self[y][x] = self[y][x].max(other[y][x]);
			}
		}
		self
	}

	pub fn minor<const X2: usize, const Y2: usize>(&self, x: usize, y: usize) -> Matrix<T, X2, Y2> {
		let mut ret = Matrix::zeroed();
		let mut skip_y = 0;
		for i in 0..Y2 {
			skip_y += if i == y { 1 } else { 0 };
			let mut skip_x = 0;
			for j in 0..X2 {
				skip_x += if j == x { 1 } else { 0 };
				ret[i][j] = self[i + skip_y][j + skip_x];
			}
		}
		ret
	}

	pub fn trace(&self) -> T {
		(0..X.min(Y)).map(|i| self[i][i]).fold(T::ZERO, |e, v| e + v)
	}
}

impl<T: Val, const X: usize, const Y: usize> Default for Matrix<T, X, Y> {
	fn default() -> Self {
		Self { elements: [[T::default(); X]; Y] }
	}
}

impl<T: Val, const X: usize, const Y: usize> From<[[T; X]; Y]> for Matrix<T, X, Y> {
	fn from(v: [[T; X]; Y]) -> Self {
		Matrix::new(v)
	}
}

impl<T: Val, const X: usize, const Y: usize> Deref for Matrix<T, X, Y> {
	type Target = [[T; X]; Y];

	fn deref(&self) -> &Self::Target {
		&self.elements
	}
}

impl<T: Val, const X: usize, const Y: usize> DerefMut for Matrix<T, X, Y> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.elements
	}
}

impl<T: Val, const X: usize, const Y: usize> AddAssign<Matrix<T, X, Y>> for Matrix<T, X, Y> {
	fn add_assign(&mut self, rhs: Matrix<T, X, Y>) {
		for y in 0..Y {
			for x in 0..X {
				self[y][x] += rhs[y][x];
			}
		}
	}
}

impl<T: Val, const X: usize, const Y: usize> SubAssign<Matrix<T, X, Y>> for Matrix<T, X, Y> {
	fn sub_assign(&mut self, rhs: Matrix<T, X, Y>) {
		for y in 0..Y {
			for x in 0..X {
				self[y][x] -= rhs[y][x];
			}
		}
	}
}

impl<T: Val, const X: usize, const Y: usize> MulAssign<T> for Matrix<T, X, Y> {
	fn mul_assign(&mut self, rhs: T) {
		for y in 0..Y {
			for x in 0..X {
				self[y][x] *= rhs;
			}
		}
	}
}

impl<T: Val, const X: usize, const Y: usize> DivAssign<T> for Matrix<T, X, Y> {
	fn div_assign(&mut self, rhs: T) {
		for y in 0..Y {
			for x in 0..X {
				self[y][x] /= rhs;
			}
		}
	}
}

impl<T: Val, const X: usize, const Y: usize> Add for Matrix<T, X, Y> {
	type Output = Matrix<T, X, Y>;

	fn add(mut self, rhs: Self) -> Self::Output {
		self += rhs;
		self
	}
}

impl<T: Val, const X: usize, const Y: usize> Sub for Matrix<T, X, Y> {
	type Output = Matrix<T, X, Y>;

	fn sub(mut self, rhs: Self) -> Self::Output {
		self -= rhs;
		self
	}
}

impl<T: Val, const X: usize, const Y: usize> Mul<T> for Matrix<T, X, Y> {
	type Output = Matrix<T, X, Y>;

	fn mul(mut self, rhs: T) -> Self::Output {
		self *= rhs;
		self
	}
}

impl<T: Val, const X: usize, const Y: usize> Div<T> for Matrix<T, X, Y> {
	type Output = Matrix<T, X, Y>;

	fn div(mut self, rhs: T) -> Self::Output {
		self /= rhs;
		self
	}
}

impl<T: Val, const X: usize, const Y: usize, const Y2: usize> Mul<Matrix<T, Y, Y2>> for Matrix<T, X, Y> {
	type Output = Matrix<T, X, Y2>;

	fn mul(self, rhs: Matrix<T, Y, Y2>) -> Self::Output {
		let mut ret = Matrix::zeroed();
		for row in 0..X {
			for col in 0..Y2 {
				for el in 0..Y {
					ret[col][row] += self[el][row] * rhs[col][el];
				}
			}
		}
		ret
	}
}

// matrix square
impl<T: Val, const X: usize> Matrix<T, X, X> {
	pub fn transpose(&mut self) {
		for i in 0..X {
			for j in 0..X {
				let temp = self[i][j];
				self[i][j] = self[j][i];
				self[j][i] = temp;
			}
		}
	}
}

// todo invert 2x2 & 3x3 matrices

// matrix 4x4
impl<T: Val> Matrix<T, 4, 4> {
	pub fn inverse(mut self) -> Self {
		self.invert();
		self
	}
	
	#[rustfmt::skip]
	pub fn invert(&mut self) {
		let mut inv: Self = Matrix::zeroed();
		
		inv[0][0] =  self[1][1] * self[2][2] * self[3][3] - self[1][1] * self[2][3] * self[3][2] - self[2][1] * self[1][2] * self[3][3] + self[2][1] * self[1][3] * self[3][2] + self[3][1] * self[1][2] * self[2][3] - self[3][1] * self[1][3] * self[2][2];
		inv[1][0] = -self[1][0] * self[2][2] * self[3][3] + self[1][0] * self[2][3] * self[3][2] + self[2][0] * self[1][2] * self[3][3] - self[2][0] * self[1][3] * self[3][2] - self[3][0] * self[1][2] * self[2][3] + self[3][0] * self[1][3] * self[2][2];
		inv[2][0] =  self[1][0] * self[2][1] * self[3][3] - self[1][0] * self[2][3] * self[3][1] - self[2][0] * self[1][1] * self[3][3] + self[2][0] * self[1][3] * self[3][1] + self[3][0] * self[1][1] * self[2][3] - self[3][0] * self[1][3] * self[2][1];
		inv[3][0] = -self[1][0] * self[2][1] * self[3][2] + self[1][0] * self[2][2] * self[3][1] + self[2][0] * self[1][1] * self[3][2] - self[2][0] * self[1][2] * self[3][1] - self[3][0] * self[1][1] * self[2][2] + self[3][0] * self[1][2] * self[2][1];
		inv[0][1] = -self[0][1] * self[2][2] * self[3][3] + self[0][1] * self[2][3] * self[3][2] + self[2][1] * self[0][2] * self[3][3] - self[2][1] * self[0][3] * self[3][2] - self[3][1] * self[0][2] * self[2][3] + self[3][1] * self[0][3] * self[2][2];
		inv[1][1] =  self[0][0] * self[2][2] * self[3][3] - self[0][0] * self[2][3] * self[3][2] - self[2][0] * self[0][2] * self[3][3] + self[2][0] * self[0][3] * self[3][2] + self[3][0] * self[0][2] * self[2][3] - self[3][0] * self[0][3] * self[2][2];
		inv[2][1] = -self[0][0] * self[2][1] * self[3][3] + self[0][0] * self[2][3] * self[3][1] + self[2][0] * self[0][1] * self[3][3] - self[2][0] * self[0][3] * self[3][1] - self[3][0] * self[0][1] * self[2][3] + self[3][0] * self[0][3] * self[2][1];
		inv[3][1] =  self[0][0] * self[2][1] * self[3][2] - self[0][0] * self[2][2] * self[3][1] - self[2][0] * self[0][1] * self[3][2] + self[2][0] * self[0][2] * self[3][1] + self[3][0] * self[0][1] * self[2][2] - self[3][0] * self[0][2] * self[2][1];
		inv[0][2] =  self[0][1] * self[1][2] * self[3][3] - self[0][1] * self[1][3] * self[3][2] - self[1][1] * self[0][2] * self[3][3] + self[1][1] * self[0][3] * self[3][2] + self[3][1] * self[0][2] * self[1][3] - self[3][1] * self[0][3] * self[1][2];
		inv[1][2] = -self[0][0] * self[1][2] * self[3][3] + self[0][0] * self[1][3] * self[3][2] + self[1][0] * self[0][2] * self[3][3] - self[1][0] * self[0][3] * self[3][2] - self[3][0] * self[0][2] * self[1][3] + self[3][0] * self[0][3] * self[1][2];
		inv[2][2] =  self[0][0] * self[1][1] * self[3][3] - self[0][0] * self[1][3] * self[3][1] - self[1][0] * self[0][1] * self[3][3] + self[1][0] * self[0][3] * self[3][1] + self[3][0] * self[0][1] * self[1][3] - self[3][0] * self[0][3] * self[1][1];
		inv[3][2] = -self[0][0] * self[1][1] * self[3][2] + self[0][0] * self[1][2] * self[3][1] + self[1][0] * self[0][1] * self[3][2] - self[1][0] * self[0][2] * self[3][1] - self[3][0] * self[0][1] * self[1][2] + self[3][0] * self[0][2] * self[1][1];
		inv[0][3] = -self[0][1] * self[1][2] * self[2][3] + self[0][1] * self[1][3] * self[2][2] + self[1][1] * self[0][2] * self[2][3] - self[1][1] * self[0][3] * self[2][2] - self[2][1] * self[0][2] * self[1][3] + self[2][1] * self[0][3] * self[1][2];
		inv[1][3] =  self[0][0] * self[1][2] * self[2][3] - self[0][0] * self[1][3] * self[2][2] - self[1][0] * self[0][2] * self[2][3] + self[1][0] * self[0][3] * self[2][2] + self[2][0] * self[0][2] * self[1][3] - self[2][0] * self[0][3] * self[1][2];
		inv[2][3] = -self[0][0] * self[1][1] * self[2][3] + self[0][0] * self[1][3] * self[2][1] + self[1][0] * self[0][1] * self[2][3] - self[1][0] * self[0][3] * self[2][1] - self[2][0] * self[0][1] * self[1][3] + self[2][0] * self[0][3] * self[1][1];
		inv[3][3] =  self[0][0] * self[1][1] * self[2][2] - self[0][0] * self[1][2] * self[2][1] - self[1][0] * self[0][1] * self[2][2] + self[1][0] * self[0][2] * self[2][1] + self[2][0] * self[0][1] * self[1][2] - self[2][0] * self[0][2] * self[1][1];
	
		let inv_det = self[0][0] * inv[0][0] + self[0][1] * inv[1][0] + self[0][2] * inv[2][0] + self[0][3] * inv[3][0];
		assert_ne!(inv_det, T::ZERO);
		let det = T::ONE / inv_det;
		inv *= det;

		*self = inv;
	}
}

impl<T: Val, const X: usize> Matrix<T, X, 1> {
	pub fn to_vector_column(self) -> Vector<T, X> {
		Vector::new(self.column(0))
	}
}

impl<T: Val, const Y: usize> Matrix<T, 1, Y> {
	pub fn to_vector_row(self) -> Vector<T, Y> {
		Vector::new(self.row(0))
	}
}
