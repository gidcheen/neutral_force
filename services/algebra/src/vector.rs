use std::ops::*;

use crate::value::Val;
use crate::*;

// #[repr(C)]
#[repr(transparent)]
#[derive(Copy, Clone, Debug)]
pub struct Vector<T: Val, const X: usize> {
	elements: [T; X],
}

impl<T: Val, const X: usize> Vector<T, X> {
	pub fn new(elements: [T; X]) -> Self {
		Self { elements }
	}

	pub fn zeroed() -> Self {
		Self { elements: [T::ZERO; X] }
	}

	pub fn length_squared(&self) -> T {
		self.iter().fold(T::ZERO, |e, v| e + *v * *v)
	}

	pub fn length(&self) -> T {
		self.length_squared().sqrt()
	}

	pub fn scale(&mut self, other: Vector<T, X>) {
		for x in 0..X {
			self[x] *= other[x];
		}
	}

	pub fn scaled(mut self, other: Vector<T, X>) -> Self {
		self.scale(other);
		self
	}

	pub fn normalize(&mut self) {
		*self *= self.length();
	}

	pub fn normalized(mut self) -> Self {
		self.normalize();
		self
	}

	pub fn min(mut self, other: Self) -> Self {
		for x in 0..X {
			self[x] = self[x].min(other[x]);
		}
		self
	}

	pub fn max(mut self, other: Self) -> Self {
		for x in 0..X {
			self[x] = self[x].max(other[x]);
		}
		self
	}

	pub fn dot(self, other: Vector<T, X>) -> T {
		(self.to_matrix() * other.to_matrix().transposed())[0][0] // todo hard code vector dot
	}

	pub fn to_matrix(self) -> Matrix<T, X, 1> {
		Matrix::new([self.elements])
	}
}

impl<T: Val, const X: usize> Default for Vector<T, X> {
	fn default() -> Self {
		Self { elements: [T::default(); X] }
	}
}

impl<T: Val, const X: usize> From<[T; X]> for Vector<T, X> {
	fn from(v: [T; X]) -> Self {
		Vector::new(v)
	}
}

impl<T: Val, const X: usize> Deref for Vector<T, X> {
	type Target = [T; X];

	fn deref(&self) -> &Self::Target {
		&self.elements
	}
}

impl<T: Val, const X: usize> DerefMut for Vector<T, X> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.elements
	}
}

impl<T: Val, const X: usize> AddAssign<Vector<T, X>> for Vector<T, X> {
	fn add_assign(&mut self, rhs: Vector<T, X>) {
		for x in 0..X {
			self[x] += rhs[x];
		}
	}
}

impl<T: Val, const X: usize> SubAssign<Vector<T, X>> for Vector<T, X> {
	fn sub_assign(&mut self, rhs: Vector<T, X>) {
		for x in 0..X {
			self[x] -= rhs[x];
		}
	}
}

impl<T: Val, const X: usize> MulAssign<T> for Vector<T, X> {
	fn mul_assign(&mut self, rhs: T) {
		for x in 0..X {
			self[x] *= rhs;
		}
	}
}

impl<T: Val, const X: usize> DivAssign<T> for Vector<T, X> {
	fn div_assign(&mut self, rhs: T) {
		for x in 0..X {
			self[x] /= rhs;
		}
	}
}

impl<T: Val, const X: usize> Add for Vector<T, X> {
	type Output = Vector<T, X>;

	fn add(mut self, rhs: Self) -> Self::Output {
		self += rhs;
		self
	}
}

impl<T: Val, const X: usize> Sub for Vector<T, X> {
	type Output = Vector<T, X>;

	fn sub(mut self, rhs: Self) -> Self::Output {
		self -= rhs;
		self
	}
}

impl<T: Val, const X: usize> Mul<T> for Vector<T, X> {
	type Output = Vector<T, X>;

	fn mul(mut self, rhs: T) -> Self::Output {
		self *= rhs;
		self
	}
}

impl<T: Val, const X: usize> Div<T> for Vector<T, X> {
	type Output = Vector<T, X>;

	fn div(mut self, rhs: T) -> Self::Output {
		self /= rhs;
		self
	}
}

// vector 1
impl<T: Val> Vector<T, 1> {
	pub fn x(&self) -> T {
		self[0]
	}
	pub fn x_ref(&self) -> &T {
		&self[0]
	}
	pub fn x_mut(&mut self) -> &mut T {
		&mut self[0]
	}
}

// vector 2
impl<T: Val> Vector<T, 2> {
	pub fn x(&self) -> T {
		self[0]
	}
	pub fn y(&self) -> T {
		self[1]
	}
	pub fn x_ref(&self) -> &T {
		&self[0]
	}
	pub fn y_ref(&self) -> &T {
		&self[1]
	}
	pub fn x_mut(&mut self) -> &mut T {
		&mut self[0]
	}
	pub fn y_mut(&mut self) -> &mut T {
		&mut self[1]
	}
}

// vector 3
impl<T: Val> Vector<T, 3> {
	pub fn x(&self) -> T {
		self[0]
	}
	pub fn y(&self) -> T {
		self[1]
	}
	pub fn z(&self) -> T {
		self[2]
	}
	pub fn x_ref(&self) -> &T {
		&self[0]
	}
	pub fn y_ref(&self) -> &T {
		&self[1]
	}
	pub fn z_ref(&self) -> &T {
		&self[2]
	}
	pub fn x_mut(&mut self) -> &mut T {
		&mut self[0]
	}
	pub fn y_mut(&mut self) -> &mut T {
		&mut self[1]
	}
	pub fn z_mut(&mut self) -> &mut T {
		&mut self[2]
	}

	pub fn cross(self, other: Vector<T, 3>) -> Vector<T, 3> {
		Vector::new([
			self.y() * other.z() - self.z() * other.y(),
			self.z() * other.x() - self.x() * other.z(),
			self.x() * other.y() - self.y() * other.x(),
		])
	}
}

// vector 4
impl<T: Val> Vector<T, 4> {
	pub fn x(&self) -> T {
		self[0]
	}
	pub fn y(&self) -> T {
		self[1]
	}
	pub fn z(&self) -> T {
		self[2]
	}
	pub fn w(&self) -> T {
		self[3]
	}
	pub fn x_ref(&self) -> &T {
		&self[0]
	}
	pub fn y_ref(&self) -> &T {
		&self[1]
	}
	pub fn z_ref(&self) -> &T {
		&self[2]
	}
	pub fn w_ref(&self) -> &T {
		&self[3]
	}
	pub fn x_mut(&mut self) -> &mut T {
		&mut self[0]
	}
	pub fn y_mut(&mut self) -> &mut T {
		&mut self[1]
	}
	pub fn z_mut(&mut self) -> &mut T {
		&mut self[2]
	}
	pub fn w_mut(&mut self) -> &mut T {
		&mut self[3]
	}

	pub fn to_quaternion(self) -> Quaternion<T> {
		Quaternion::new(self.elements)
	}
}
