mod matrix;
mod quaternion;
pub mod value;
mod vector;

pub use matrix::*;
pub use quaternion::*;
pub use vector::*;

pub type Vector2<T> = Vector<T, 2>;
pub type Vector3<T> = Vector<T, 3>;
pub type Vector4<T> = Vector<T, 4>;

pub type Matrix2<T> = Matrix<T, 2, 2>;
pub type Matrix3<T> = Matrix<T, 3, 3>;
pub type Matrix4<T> = Matrix<T, 4, 4>;

pub type Vector2f = Vector<f32, 2>;
pub type Vector3f = Vector<f32, 3>;
pub type Vector4f = Vector<f32, 4>;

pub type Matrix2f = Matrix<f32, 2, 2>;
pub type Matrix3f = Matrix<f32, 3, 3>;
pub type Matrix4f = Matrix<f32, 4, 4>;

pub type Vector2d = Vector<f64, 2>;
pub type Vector3d = Vector<f64, 3>;
pub type Vector4d = Vector<f64, 4>;

pub type Matrix2d = Matrix<f64, 2, 2>;
pub type Matrix3d = Matrix<f64, 3, 3>;
pub type Matrix4d = Matrix<f64, 4, 4>;

pub type Quaternionf = Quaternion<f32>;
pub type Quaterniond = Quaternion<f64>;
