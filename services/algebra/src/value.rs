use std::f32;
use std::f64;
use std::fmt::Debug;
use std::ops::*;

impl Val for f32 {
	const ZERO: Self = 0.0;
	const ONE: Self = 1.0;
	const TWO: Self = 2.0;
	const HALF: Self = 0.5;
	const PI: Self = f32::consts::PI;
	fn min(self, other: Self) -> Self {
		self.min(other)
	}
	fn max(self, other: Self) -> Self {
		self.max(other)
	}
}

impl Math for f32 {
	fn sin(self: Self) -> Self {
		self.sin()
	}
	fn cos(self: Self) -> Self {
		self.cos()
	}
	fn tan(self: Self) -> Self {
		self.tan()
	}
	fn sinh(self: Self) -> Self {
		self.sinh()
	}
	fn cosh(self: Self) -> Self {
		self.cosh()
	}
	fn tanh(self: Self) -> Self {
		self.tanh()
	}
	fn asin(self: Self) -> Self {
		self.asin()
	}
	fn acos(self: Self) -> Self {
		self.acos()
	}
	fn atan(self: Self) -> Self {
		self.atan()
	}
	fn atan2(self: Self, other: Self) -> Self {
		self.atan2(other)
	}
	fn exp(self: Self) -> Self {
		self.exp()
	}
	fn log(self: Self, base: Self) -> Self {
		self.log(base)
	}
	fn ln(self: Self) -> Self {
		self.ln()
	}
	fn log10(self: Self) -> Self {
		self.log10()
	}
	fn powf(self: Self, y: Self) -> Self {
		self.powf(y)
	}
	fn sqrt(self: Self) -> Self {
		self.sqrt()
	}
	fn abs(self: Self) -> Self {
		self.abs()
	}
	fn ceil(self: Self) -> Self {
		self.ceil()
	}
	fn floor(self: Self) -> Self {
		self.floor()
	}
	fn copysign(self: Self, sign: Self) -> Self {
		self.copysign(sign)
	}
}

impl Val for f64 {
	const ZERO: Self = 0.0;
	const ONE: Self = 1.0;
	const TWO: Self = 2.0;
	const HALF: Self = 0.5;
	const PI: Self = f64::consts::PI;
	fn min(self, other: Self) -> Self {
		self.min(other)
	}
	fn max(self, other: Self) -> Self {
		self.max(other)
	}
}

impl Math for f64 {
	fn sin(self: Self) -> Self {
		self.sin()
	}
	fn cos(self: Self) -> Self {
		self.cos()
	}
	fn tan(self: Self) -> Self {
		self.tan()
	}
	fn sinh(self: Self) -> Self {
		self.sinh()
	}
	fn cosh(self: Self) -> Self {
		self.cosh()
	}
	fn tanh(self: Self) -> Self {
		self.tanh()
	}
	fn asin(self: Self) -> Self {
		self.asin()
	}
	fn acos(self: Self) -> Self {
		self.acos()
	}
	fn atan(self: Self) -> Self {
		self.atan()
	}
	fn atan2(self: Self, other: Self) -> Self {
		self.atan2(other)
	}
	fn exp(self: Self) -> Self {
		self.exp()
	}
	fn log(self: Self, base: Self) -> Self {
		self.log(base)
	}
	fn ln(self: Self) -> Self {
		self.ln()
	}
	fn log10(self: Self) -> Self {
		self.log10()
	}
	fn powf(self: Self, y: Self) -> Self {
		self.powf(y)
	}
	fn sqrt(self: Self) -> Self {
		self.sqrt()
	}
	fn abs(self: Self) -> Self {
		self.abs()
	}
	fn ceil(self: Self) -> Self {
		self.ceil()
	}
	fn floor(self: Self) -> Self {
		self.floor()
	}
	fn copysign(self: Self, sign: Self) -> Self {
		self.copysign(sign)
	}
}

pub trait Val:
	Copy
	+ Clone
	+ Default
	+ Debug
	+ Add<Output = Self>
	+ Sub<Output = Self>
	+ Mul<Output = Self>
	+ Div<Output = Self>
	+ AddAssign
	+ SubAssign
	+ MulAssign
	+ DivAssign
	+ Rem
	+ PartialOrd
	+ Neg<Output = Self>
	+ PartialEq
	+ Math
{
	const ZERO: Self;
	const ONE: Self;
	const TWO: Self;
	const HALF: Self;
	const PI: Self;
	fn min(self, other: Self) -> Self;
	fn max(self, other: Self) -> Self;
}

pub trait Math {
	fn sin(self: Self) -> Self;
	fn cos(self: Self) -> Self;
	fn tan(self: Self) -> Self;
	fn sinh(self: Self) -> Self;
	fn cosh(self: Self) -> Self;
	fn tanh(self: Self) -> Self;
	fn asin(self: Self) -> Self;
	fn acos(self: Self) -> Self;
	fn atan(self: Self) -> Self;
	fn atan2(self: Self, other: Self) -> Self;
	fn exp(self: Self) -> Self;
	fn log(self: Self, base: Self) -> Self;
	fn ln(self: Self) -> Self;
	fn log10(self: Self) -> Self;
	fn powf(self: Self, y: Self) -> Self;
	fn sqrt(self: Self) -> Self;
	fn abs(self: Self) -> Self;
	fn ceil(self: Self) -> Self;
	fn floor(self: Self) -> Self;
	fn copysign(self: Self, sign: Self) -> Self;
}
