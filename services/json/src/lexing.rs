use crate::escape::unescape;
use crate::Number;
use std::option::Option::Some;
use std::str::FromStr;
use std::{f64, i64, u64};

#[derive(Debug, PartialEq, Eq)]
pub(crate) enum BracketKind {
	Array,
	Object,
}

#[derive(Debug)]
pub(crate) enum BracketType {
	Open,
	Close,
}

#[derive(Debug)]
pub(crate) enum Token {
	Comma,
	Colon,
	Bracket(BracketKind, BracketType),

	Null,
	Bool(bool),
	Number(Number),
	String(String),
}

pub(crate) fn lex(mut json: &str) -> Option<Vec<Token>> {
	let mut tokens = Vec::new();
	tokens.reserve(json.len() / 3);

	let lexers = [lex_null, lex_bool, lex_number, lex_string, lex_syntax];

	'l: while json.len() > 0 {
		if let Some(c) = json.chars().nth(0) {
			if c.is_whitespace() {
				json = &json[1..];
				continue;
			} else if json.starts_with("//") {
				json = &json[2..];
				if let Some(i) = json.find(|c| c == '\n') {
					json = &json[i..];
				} else {
					break;
				}
			} else if json.starts_with("/*") {
				json = &json[2..];
				let mut comment_depth = 1usize;
				while comment_depth > 0 && json.len() > 0 {
					if json.starts_with("/*") {
						json = &json[2..];
						comment_depth += 1;
					} else if json.starts_with("*/") {
						json = &json[2..];
						comment_depth -= 1;
					} else {
						json = &json[1..];
					}
				}
			} else {
				for lexer in lexers.iter() {
					if let Some((token, new_json)) = lexer(json) {
						tokens.push(token);
						json = new_json;
						continue 'l;
					}
				}
				return None;
			}
		}
	}
	Some(tokens)
}

fn lex_null(json: &str) -> Option<(Token, &str)> {
	const NULL: &str = "null";

	if is_word_exactly(json, NULL) {
		Some((Token::Null, &json[NULL.len()..]))
	} else {
		None
	}
}

fn lex_bool(json: &str) -> Option<(Token, &str)> {
	const TRUE: &str = "true";
	const FALSE: &str = "false";

	if is_word_exactly(json, TRUE) {
		Some((Token::Bool(true), &json[TRUE.len()..]))
	} else if is_word_exactly(json, FALSE) {
		Some((Token::Bool(false), &json[FALSE.len()..]))
	} else {
		None
	}
}

fn lex_number(json: &str) -> Option<(Token, &str)> {
	match get_number_exactly(json) {
		Some(number_str) => {
			if number_str.contains(".") || number_str.contains('e') {
				match f64::from_str(number_str) {
					Ok(v) => Some((Token::Number(Number::Float(v)), &json[number_str.len()..])),
					_ => None,
				}
			} else if number_str.starts_with('-') {
				match i64::from_str(number_str) {
					Ok(v) => Some((Token::Number(Number::Int(v)), &json[number_str.len()..])),
					_ => None,
				}
			} else {
				match u64::from_str(number_str) {
					Ok(v) => Some((Token::Number(Number::UInt(v)), &json[number_str.len()..])),
					_ => None,
				}
			}
		}
		None => None,
	}
}

fn lex_string(json: &str) -> Option<(Token, &str)> {
	const QUOTE: char = '"';

	if json.len() >= 2 && json.chars().nth(0).unwrap() == QUOTE {
		let mut escaped = false;
		let end = json[1..].chars().position(|c| {
			let ret = !escaped && c == QUOTE;
			escaped = if c == '\\' { true } else { false };
			ret
		});

		if let Some(end) = end {
			Some((Token::String(unescape(&json[1..end + 1])), &json[end + 2..]))
		} else {
			None
		}
	} else {
		None
	}
}

fn lex_syntax(json: &str) -> Option<(Token, &str)> {
	json.chars()
		.nth(0)
		.map_or(None, |c| match c {
			',' => Some(Token::Comma),
			':' => Some(Token::Colon),
			'[' => Some(Token::Bracket(BracketKind::Array, BracketType::Open)),
			']' => Some(Token::Bracket(BracketKind::Array, BracketType::Close)),
			'{' => Some(Token::Bracket(BracketKind::Object, BracketType::Open)),
			'}' => Some(Token::Bracket(BracketKind::Object, BracketType::Close)),
			_ => None,
		})
		.map(|t| (t, &json[1..]))
}

fn is_word_exactly(json: &str, word: &str) -> bool {
	json.starts_with(word) && json.chars().nth(word.len() + 1).map_or(true, |c| !c.is_alphanumeric())
}

fn get_number_exactly(json: &str) -> Option<&str> {
	match json.chars().nth(0) {
		Some('-' | '0'..='9') => {
			let mut end = 1;
			for c in json.chars().skip(1) {
				match c {
					'0'..='9' | 'e' | 'E' | '.' => end += 1,
					_ => break,
				}
			}
			Some(&json[..end])
		}
		_ => None,
	}
}
