mod escape;
mod lexing;
mod parsing;

use crate::lexing::lex;
use crate::parsing::parse;
use escape::escape;
use std::collections::{hash_map, HashMap};
use std::fmt::{Display, Formatter};
use std::slice;

#[derive(Copy, Clone, Debug)]
enum Number {
	Int(i64),
	UInt(u64),
	Float(f64),
}

#[derive(Clone, Debug)]
enum Value {
	Null,
	Bool(bool),
	Number(Number),
	String(String),
	Array(Vec<Json>),
	Object(HashMap<String, Json>),
}

#[derive(Clone, Debug)]
pub struct Json {
	value: Value,
}

impl Json {
	pub fn new_null() -> Self {
		Json {
			value: Value::Null,
		}
	}
	pub fn new_bool(value: bool) -> Self {
		Json {
			value: Value::Bool(value),
		}
	}
	pub fn new_i8(value: i8) -> Self {
		Self::new_i64(value as _)
	}
	pub fn new_i16(value: i16) -> Self {
		Self::new_i64(value as _)
	}
	pub fn new_i32(value: i32) -> Self {
		Self::new_i64(value as _)
	}
	pub fn new_i64(value: i64) -> Self {
		Json {
			value: Value::Number(Number::Int(value)),
		}
	}
	pub fn new_u8(value: u8) -> Self {
		Self::new_u64(value as _)
	}
	pub fn new_u16(value: u16) -> Self {
		Self::new_u64(value as _)
	}
	pub fn new_u32(value: u32) -> Self {
		Self::new_u64(value as _)
	}
	pub fn new_u64(value: u64) -> Self {
		Json {
			value: Value::Number(Number::UInt(value)),
		}
	}
	pub fn new_f32(value: f32) -> Self {
		Self::new_f64(value as _)
	}
	pub fn new_f64(value: f64) -> Self {
		Json {
			value: Value::Number(Number::Float(value)),
		}
	}
	pub fn new_string(value: String) -> Self {
		Json {
			value: Value::String(value),
		}
	}
	pub fn new_array(value: impl Iterator<Item = Json>) -> Self {
		Self {
			value: Value::Array(value.collect()),
		}
	}
	pub fn new_object(value: impl Iterator<Item = (String, Json)>) -> Self {
		Self {
			value: Value::Object(value.collect()),
		}
	}

	pub fn set_null(&mut self) {
		*self = Self::new_null()
	}
	pub fn set_bool(&mut self, value: bool) {
		*self = Self::new_bool(value)
	}
	pub fn set_i8(&mut self, value: i8) {
		*self = Self::new_i8(value)
	}
	pub fn set_i16(&mut self, value: i16) {
		*self = Self::new_i16(value)
	}
	pub fn set_i32(&mut self, value: i32) {
		*self = Self::new_i32(value)
	}
	pub fn set_i64(&mut self, value: i64) {
		*self = Self::new_i64(value)
	}
	pub fn set_u8(&mut self, value: u8) {
		*self = Self::new_u8(value)
	}
	pub fn set_u16(&mut self, value: u16) {
		*self = Self::new_u16(value)
	}
	pub fn set_u32(&mut self, value: u32) {
		*self = Self::new_u32(value)
	}
	pub fn set_u64(&mut self, value: u64) {
		*self = Self::new_u64(value)
	}
	pub fn set_f32(&mut self, value: f32) {
		*self = Self::new_f32(value)
	}
	pub fn set_f64(&mut self, value: f64) {
		*self = Self::new_f64(value)
	}
	pub fn set_string(&mut self, value: String) {
		*self = Self::new_string(value)
	}
	pub fn set_array(&mut self, value: impl Iterator<Item = Json>) {
		*self = Self::new_array(value)
	}
	pub fn set_object(&mut self, value: impl Iterator<Item = (String, Json)>) {
		*self = Self::new_object(value)
	}

	pub fn is_null(&self) -> bool {
		if let Value::Null = &self.value {
			true
		} else {
			false
		}
	}
	pub fn is_boolean(&self) -> bool {
		match self.value {
			Value::Null | Value::Bool(_) | Value::Number(_) => true,
			_ => false,
		}
	}
	pub fn is_nummeric(&self) -> bool {
		match self.value {
			Value::Bool(_) | Value::Number(_) => true,
			_ => false,
		}
	}
	pub fn is_string(&self) -> bool {
		if let Value::String(_) = &self.value {
			true
		} else {
			false
		}
	}
	pub fn is_array(&self) -> bool {
		if let Value::Array(_) = &self.value {
			true
		} else {
			false
		}
	}
	pub fn is_object(&self) -> bool {
		if let Value::Object(_) = &self.value {
			true
		} else {
			false
		}
	}

	pub fn as_bool(&self) -> Option<bool> {
		match self.value {
			Value::Null => Some(false),
			Value::Bool(v) => Some(v),
			Value::Number(v) => match v {
				Number::Int(v) => Some(v != 0),
				Number::UInt(v) => Some(v != 0),
				Number::Float(v) => Some(v != 0.0),
			},
			_ => None,
		}
	}

	pub fn as_i8(&self) -> Option<i8> {
		self.as_i64().map(|v| v as _)
	}
	pub fn as_i16(&self) -> Option<i16> {
		self.as_i64().map(|v| v as _)
	}
	pub fn as_i32(&self) -> Option<i32> {
		self.as_i64().map(|v| v as _)
	}
	pub fn as_i64(&self) -> Option<i64> {
		match self.value {
			Value::Bool(v) => Some(if v {
				1
			} else {
				0
			}),
			Value::Number(v) => match v {
				Number::Int(v) => Some(v),
				Number::UInt(v) => Some(v as _),
				Number::Float(v) => Some(v as _),
			},
			_ => None,
		}
	}

	pub fn as_u8(&self) -> Option<u8> {
		self.as_u64().map(|v| v as _)
	}
	pub fn as_u16(&self) -> Option<u16> {
		self.as_u64().map(|v| v as _)
	}
	pub fn as_u32(&self) -> Option<u32> {
		self.as_u64().map(|v| v as _)
	}
	pub fn as_u64(&self) -> Option<u64> {
		match self.value {
			Value::Bool(v) => Some(if v {
				1
			} else {
				0
			}),
			Value::Number(v) => match v {
				Number::Int(v) => Some(v as _),
				Number::UInt(v) => Some(v),
				Number::Float(v) => Some(v as _),
			},
			_ => None,
		}
	}

	pub fn as_f32(&self) -> Option<f32> {
		self.as_f64().map(|v| v as _)
	}
	pub fn as_f64(&self) -> Option<f64> {
		match self.value {
			Value::Bool(v) => Some(if v {
				1.0
			} else {
				0.0
			}),
			Value::Number(v) => match v {
				Number::Int(v) => Some(v as _),
				Number::UInt(v) => Some(v as _),
				Number::Float(v) => Some(v),
			},
			_ => None,
		}
	}

	pub fn as_string(&self) -> Option<&str> {
		if let Value::String(v) = &self.value {
			Some(v)
		} else {
			None
		}
	}

	pub fn get_index(&self, index: usize) -> Option<&Json> {
		if let Value::Array(v) = &self.value {
			v.get(index)
		} else {
			None
		}
	}

	pub fn get_name(&self, name: &str) -> Option<&Json> {
		if let Value::Object(v) = &self.value {
			v.get(name)
		} else {
			None
		}
	}

	pub fn array_iter(&self) -> Option<slice::Iter<Json>> {
		if let Value::Array(v) = &self.value {
			Some(v.iter())
		} else {
			None
		}
	}
	pub fn array_iter_mut(&mut self) -> Option<slice::IterMut<Json>> {
		if let Value::Array(v) = &mut self.value {
			Some(v.iter_mut())
		} else {
			None
		}
	}

	pub fn object_iter(&self) -> Option<hash_map::Iter<String, Json>> {
		if let Value::Object(v) = &self.value {
			Some(v.iter())
		} else {
			None
		}
	}
	pub fn object_iter_mut(&mut self) -> Option<hash_map::IterMut<String, Json>> {
		if let Value::Object(v) = &mut self.value {
			Some(v.iter_mut())
		} else {
			None
		}
	}

	pub fn to_string(&self) -> String {
		self.value.to_string()
	}
	pub fn from_string(json: &str) -> Option<Self> {
		// todo result
		if let Some(tokens) = lex(json) {
			parse(&tokens)
		} else {
			None
		}
	}
}

impl Display for Json {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		f.write_str(&self.to_string())
	}
}

impl Value {
	pub fn to_string(&self) -> String {
		match self {
			Value::Null => "null".into(),
			Value::Bool(v) => format!("{}", *v),
			Value::Number(v) => v.to_string(),
			Value::String(v) => format!("\"{}\"", escape(v)),
			Value::Array(v) => {
				format!("[{}]", v.iter().map(|e| e.to_string()).collect::<Vec<_>>().join(", "))
			}
			Value::Object(v) => format!(
				"{{{}}}",
				v.iter()
					.map(|(n, e)| format!("\"{}\": {}", n, e.to_string()))
					.collect::<Vec<_>>()
					.join(", ")
			),
		}
	}
}

impl Number {
	pub fn to_string(self) -> String {
		match self {
			Number::Int(v) => format!("{}", v),
			Number::UInt(v) => format!("{}", v),
			Number::Float(v) => format!("{}", v),
		}
	}
}
