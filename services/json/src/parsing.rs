use crate::lexing::*;
use crate::*;

pub(crate) fn parse(tokens: &[Token]) -> Option<Json> {
	parse_json(tokens).map(|(v, _)| v)
}

fn parse_json(tokens: &[Token]) -> Option<(Json, &[Token])> {
	match tokens {
		[Token::Null, tokens @ ..] => Some((Json::new_null(), tokens)),
		[Token::Bool(b), tokens @ ..] => Some((Json::new_bool(*b), tokens)),
		[Token::Number(n), tokens @ ..] => match n {
			Number::Int(v) => Some((Json::new_i64(*v), tokens)),
			Number::UInt(v) => Some((Json::new_u64(*v), tokens)),
			Number::Float(v) => Some((Json::new_f64(*v), tokens)),
		},
		[Token::String(s), tokens @ ..] => Some((Json::new_string(s.clone()), tokens)),
		tokens => {
			if let Some((elements, tokens)) = parse_in_brackets(tokens, parse_json, BracketKind::Array) {
				Some((Json::new_array(elements.into_iter()), tokens))
			} else if let Some((elements, tokens)) = parse_in_brackets(tokens, parse_object_element, BracketKind::Object) {
				Some((Json::new_object(elements.into_iter()), tokens))
			} else {
				None
			}
		}
	}
}

fn parse_object_element(tokens: &[Token]) -> Option<((String, Json), &[Token])> {
	match tokens {
		[Token::String(name), Token::Colon, tokens @ ..] => match parse_json(tokens) {
			Some((element, tokens)) => Some(((name.clone(), element), tokens)),
			None => None,
		},
		_ => None,
	}
}

// _bracket_kind underscore bc matches!() causes unused warning
fn parse_in_brackets<T>(tokens: &[Token], element_parser: impl Fn(&[Token]) -> Option<(T, &[Token])>, bracket_kind: BracketKind) -> Option<(Vec<T>, &[Token])> {
	match tokens {
		[Token::Bracket(bk, BracketType::Open), tokens @ ..] if *bk == bracket_kind => {
			let mut tokens = tokens;
			let mut elements = Vec::new();
			while let Some((element, new_tokens)) = element_parser(tokens) {
				elements.push(element);
				tokens = new_tokens;

				match tokens {
					[Token::Bracket(bk, BracketType::Close), tokens @ ..] if *bk == bracket_kind => return Some((elements, tokens)),
					[Token::Comma, new_tokens @ ..] => tokens = new_tokens,
					_ => return None,
				}
			}
		}
		_ => (),
	};
	None
}
