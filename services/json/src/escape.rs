pub(crate) fn escape(s: &str) -> String {
	let mut ret = String::with_capacity(s.len() + 5);
	for c in s.chars() {
		match c {
			'\n' => ret.push_str("\\n"),
			'\r' => ret.push_str("\\r"),
			'\t' => ret.push_str("\\t"),
			'\"' => ret.push_str("\\\""),
			'\\' => ret.push_str("\\\\"),
			_ => ret.push(c),
		}
	}
	ret
}

pub(crate) fn unescape(s: &str) -> String {
	let mut ret = String::with_capacity(s.len());
	let mut escaped = false;
	for c in s.chars() {
		if escaped {
			escaped = false;
			ret.push(match c {
				'n' => '\n',
				'r' => '\r',
				't' => '\t',
				'"' => '\"',
				'\\' => '\\',
				_ => panic!("invalid char after escape {}", c),
			})
		} else if c == '\\' {
			escaped = true;
		} else {
			ret.push(c);
		}
	}
	ret
}
