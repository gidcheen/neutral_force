struct Camera
{
	mat4 globalToLocal;
	mat4 localToGlobal;
	mat4 localToProjection;
};

struct Surface
{
	mat4 localToGlobal;
};

struct Light
{
	mat4 globalToLocal;
	mat4 localToGlobal;
	
	vec4 color;
	float radius;
	int type;
	float cutoffInner;
	float cutoffOuter;
};

layout(std140, set = 1, binding = 0) uniform Entities {
	Camera camera;
	Surface surface;
	int lightCount;
	Light lights[8];
};

const int lightTypeAmbient = 1;
const int lightTypeDirectional = 2;
const int lightTypePoint = 3;
const int lightTypeSpot = 4;

struct VertToFrag
{
	vec3 position;
	vec4 color;
	vec2 uv;
	mat3 tbn;
};
