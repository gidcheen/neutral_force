#include "common.glsl"

layout(location = 0) in VertToFrag vertToFrag;
layout(location = 0) out vec4 outColor;

/*layout(std140, set = 1, binding = 2) uniform */bool isLit = true;
/*layout(std140, set = 1, binding = 3) uniform */bool isTransparent = false;
/*layout(std140, set = 1, binding = 4) uniform */float alphaCutoff = 0.5;

struct SurfaceData
{
	vec4 diffuse;
	vec3 specular;
	float roughness;
	vec3 emissive;
	vec3 normal;
};

SurfaceData GetSurfaceData();

struct LightData
{
	vec3 diffuse;
	vec3 specular;
};

LightData CombineLightData(LightData a, LightData b)
{
	return LightData(a.diffuse + b.diffuse, a.specular + b.specular);
}

LightData CalcAmbientLight(
	in vec3 lightColor,
	in vec3 surfaceDiffuse)
{
	return LightData(
		surfaceDiffuse * lightColor,
		vec3(0, 0, 0)
	);
}

const int shineExp = 64;

LightData CalcDirectionalLight(
	in vec3 lightDirection, 
	in vec3 surfacePosition,
	in vec3 cameraPosition,
	in vec3 lightColor,
	in vec3 surfaceDiffuse,
	in vec3 surfaceSpecular,
	in float surfaceRoughness,
	in vec3 surfaceNormal)
{
	vec3 lightDir = normalize(lightDirection);
	vec3 viewDir = normalize(cameraPosition - surfacePosition);

	// diffuse shading
	float diffuse = max(dot(surfaceNormal, lightDir), 0.0);

	// specular shading
	vec3 halfwayDir = normalize(lightDirection + viewDir);
	float specular = pow(max(dot(surfaceNormal, halfwayDir), 0.0), (1 - surfaceRoughness) * shineExp);

	// combine results
	return LightData(
		surfaceDiffuse * diffuse,
		surfaceSpecular * specular
	);
}

float CalculateAttenuation(float dist, float radius)
{
	float attenuation = 1.0 / (1.0 + dist * dist);
	
	float q = max((radius - dist) / radius, 0);
	q = min(q * 5, 1);

	return attenuation * q;
}

LightData CalcPointLight(
	in vec3 lightPosition, 
	in vec3 surfacePosition,
	in vec3 cameraPosition,
	in vec3 lightColor,
	in float lightRadius,
	in vec3 surfaceDiffuse,
	in vec3 surfaceSpecular,
	in float surfaceRoughness,
	in vec3 surfaceNormal)
{
	vec3 lightDir = normalize(lightPosition - surfacePosition);
	vec3 viewDir = normalize(cameraPosition - surfacePosition);
	
	// diffuse shading
	float diffuse = max(dot(surfaceNormal, lightDir), 0.0);
	
	// specular shading
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float specular = pow(max(dot(surfaceNormal, halfwayDir), 0.0), (1 - surfaceRoughness) * shineExp);
	
	// attenuation
	float dist = length(lightPosition - surfacePosition);
	float attenuation = CalculateAttenuation(dist, lightRadius);

	return LightData(
		surfaceDiffuse * lightColor * (diffuse * attenuation),
		surfaceSpecular * lightColor * (specular * attenuation)
	);
}

LightData CalcSpotLight(	
	in vec3 lightPosition,
	in vec3 lightDirection,
	in vec3 surfacePosition,
	in vec3 cameraPosition,
	in vec3 lightColor,
	in float lightRadius,
	in float lightCutoffInner,
	in float lightCutoffOuter,
	in vec3 surfaceDiffuse,
	in vec3 surfaceSpecular,
	in float surfaceRoughness,
	in vec3 surfaceNormal)
{
	vec3 lightDir = normalize(lightPosition - surfacePosition);
	vec3 viewDir = normalize(cameraPosition - surfacePosition);
	
	// diffuse shading
	float diffuse = max(dot(surfaceNormal, lightDir), 0.0);
	
	// specular shading
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float specular = pow(max(dot(surfaceNormal, halfwayDir), 0.0), (1 - surfaceRoughness) * shineExp);
	
	// attenuation	
	float dist = length(lightPosition - surfacePosition);
	float attenuation = CalculateAttenuation(dist, lightRadius);

	// spot
	float theta = dot(lightDir, normalize(lightDirection));
	float epsilon = cos(lightCutoffInner) - cos(lightCutoffOuter);
	float cutoff = clamp((theta - cos(lightCutoffOuter)) / epsilon, 0.0, 1.0);

	return LightData(
		surfaceDiffuse * lightColor * (diffuse * attenuation * cutoff),
		surfaceSpecular * lightColor * (specular * attenuation * cutoff)
	);
}

void main()
{
	SurfaceData surfaceData = GetSurfaceData();
	if(isTransparent && surfaceData.diffuse.a == 0) // todo cutoff
		discard;
	surfaceData.normal = surfaceData.normal * 2.0 - 1.0;

	vec3 surfaceNormal = normalize(vertToFrag.tbn * surfaceData.normal);

	vec3 surfacePosition = vertToFrag.position;
	vec3 cameraPosition = vec3(camera.localToGlobal * vec4(0, 0, 0, 1));

	if(isLit)
	{
		LightData lightData = LightData(vec3(0, 0, 0), vec3(0, 0, 0));
		for(int i = 0; i < lightCount; i++)
		{
			Light light = lights[i];

			if(light.type == lightTypeAmbient)
			{
				lightData = CombineLightData(lightData, CalcAmbientLight(
					light.color.rgb,
					surfaceData.diffuse.rgb
				));
			}
			else if(light.type == lightTypeDirectional)
			{
				vec3 lightDirection = vec3(light.localToGlobal[2]);
				lightData = CombineLightData(lightData, CalcDirectionalLight(
					lightDirection,
					surfacePosition,
					cameraPosition,
					light.color.rgb,
					surfaceData.diffuse.rgb,
					surfaceData.specular,
					surfaceData.roughness,
					surfaceNormal
				));
			}
			else if(light.type == lightTypePoint)
			{
				vec3 lightPosition = vec3(light.localToGlobal[3]);
				lightData = CombineLightData(lightData, CalcPointLight(
					lightPosition,
					surfacePosition,
					cameraPosition,
					light.color.rgb,
					light.radius,
					surfaceData.diffuse.rgb,
					surfaceData.specular,
					surfaceData.roughness,
					surfaceNormal
				));
			}
			else if(light.type == lightTypeSpot)
			{
				vec3 lightPosition = vec3(light.localToGlobal[3]);
				vec3 lightDirection = vec3(light.localToGlobal[2]);
				lightData = CombineLightData(lightData, CalcSpotLight(
					lightPosition,
					lightDirection,
					surfacePosition,
					cameraPosition,
					light.color.rgb,
					light.radius,
					light.cutoffInner,
					light.cutoffOuter,
					surfaceData.diffuse.rgb,
					surfaceData.specular,
					surfaceData.roughness,
					surfaceNormal
				));
			}
		}
	
		if(isTransparent)
			outColor = vec4(lightData.diffuse + lightData.specular / surfaceData.diffuse.a, surfaceData.diffuse.a);
		else 
			outColor = vec4(lightData.diffuse + lightData.specular, surfaceData.diffuse.a);
	}
	else
	{
		outColor = surfaceData.diffuse;
	}
}
