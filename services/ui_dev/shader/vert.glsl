#include "common.glsl"

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec3 vertexTangent;
layout(location = 3) in vec3 vertexBitangent;
layout(location = 4) in vec4 vertexColor;
layout(location = 5) in vec2 vertexUv;

layout(location = 0) out VertToFrag vertToFrag;

void main()
{
	mat4 surfaceLocalToGlobal = surface.localToGlobal;
	vec4 worldPosition = surfaceLocalToGlobal * vec4(vertexPosition, 1);

	vertToFrag.position = vec3(worldPosition);
	vertToFrag.color = vertexColor;
	vertToFrag.uv = vertexUv;

	mat4 normalizedLocalToGlobal = transpose(inverse(surfaceLocalToGlobal));
	vec3 t = normalize(vec3(normalizedLocalToGlobal * vec4(vertexTangent,   0.0)));
	vec3 b = normalize(vec3(normalizedLocalToGlobal * vec4(vertexBitangent, 0.0)));
	vec3 n = normalize(vec3(normalizedLocalToGlobal * vec4(vertexNormal,    0.0)));
	vertToFrag.tbn = mat3(t, b, n);

	mat4 cameraGlobalToLocal = camera.globalToLocal;
	mat4 cameraLocalToProjection = camera.localToProjection;
	gl_Position = cameraLocalToProjection * cameraGlobalToLocal * worldPosition;
}
