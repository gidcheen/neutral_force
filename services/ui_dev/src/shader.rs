use std::error::Error;
use std::path::Path;
use std::process::Command;
use std::str::from_utf8;

const SHADER_DIR: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/shader");
const GLSLC: &str = "glslc";

#[cfg(debug_assertions)]
const DEBUG_ARG: &str = "-O";
#[cfg(not(debug_assertions))]
const DEBUG_ARG: &str = "-O";

pub fn compile(glsl_file: &Path, spv_file: &Path) -> Result<(), Box<dyn Error>> {
	let args = [format!("-I{SHADER_DIR}")];

	let output = Command::new(GLSLC)
		// .arg("--target-env=vulkan1.2")
		.arg(DEBUG_ARG)
		.args(args)
		.arg("-o")
		.arg(spv_file)
		.arg(glsl_file)
		.output()?;

	if !output.status.success() {
		Err(from_utf8(&output.stderr)?.into())
	} else {
		Ok(())
	}
}
