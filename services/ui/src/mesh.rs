use crate::allocator::Allocator;
use crate::device::Device;
use crate::transfer::Transfer;
use algebra::*;
use std::mem::size_of;
use std::ptr::*;
use std::sync::*;
use ui_sys::*;

pub struct Mesh {
	device: Arc<Device>,
	allocator: Arc<Allocator>,
	transfer: Arc<Transfer>,
	vertex_size: VkDeviceSize,
	index_size: VkDeviceSize,
	vertex_buffer: VkBuffer,
	index_buffer: VkBuffer,
}

#[repr(C)]
#[derive(Clone, Copy, Default, Debug)]
pub struct Vertex {
	pub position: Vector3f,
	pub normal: Vector3f,
	pub tangent: Vector3f,
	pub bitangent: Vector3f,
	pub color: Vector3f,
	pub uv: Vector2f,
}
#[repr(C)]
#[derive(Clone, Copy, Default, Debug)]
pub struct Index {
	pub face: [u32; 3],
}

impl Mesh {
	pub(crate) fn new(device: Arc<Device>, allocator: Arc<Allocator>, transfer: Arc<Transfer>, vertices: &[Vertex], indices: &[Index]) -> Self {
		unsafe {
			let vertex_size = (vertices.len() * size_of::<Vertex>()) as VkDeviceSize;
			let buffer_ci = VkBufferCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
				size: vertex_size,
				usage: VkBufferUsageFlagBits::VK_BUFFER_USAGE_TRANSFER_DST_BIT as u32 | VkBufferUsageFlagBits::VK_BUFFER_USAGE_VERTEX_BUFFER_BIT as u32,
				..Default::default()
			};
			let mut vertex_buffer = null_mut();
			vkCreateBuffer(device.device(), &buffer_ci, null(), &mut vertex_buffer);

			allocator.allocate_buffer(vertex_buffer);
			transfer.transfer_buffer(vertex_buffer, vertices, 0);

			let index_size = (indices.len() * size_of::<Index>()) as VkDeviceSize;
			let buffer_ci = VkBufferCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
				size: index_size,
				usage: VkBufferUsageFlagBits::VK_BUFFER_USAGE_TRANSFER_DST_BIT as u32 | VkBufferUsageFlagBits::VK_BUFFER_USAGE_INDEX_BUFFER_BIT as u32,
				..Default::default()
			};
			let mut index_buffer = null_mut();
			vkCreateBuffer(device.device(), &buffer_ci, null(), &mut index_buffer);

			allocator.allocate_buffer(index_buffer);
			transfer.transfer_buffer(index_buffer, indices, 0);

			Self {
				device,
				allocator,
				transfer,
				vertex_size,
				index_size,
				vertex_buffer,
				index_buffer,
			}
		}
	}

	pub fn set_vertices(&mut self, vertices: &[Vertex], offset: usize) {
		let size = (vertices.len() * size_of::<Vertex>()) as VkDeviceSize;
		let offset = offset as _;
		assert!(size + offset <= self.vertex_size);
		self.transfer.transfer_buffer(self.vertex_buffer, vertices, offset);
	}
	pub fn set_indices(&mut self, indices: &[Index], offset: usize) {
		let size = (indices.len() * size_of::<Index>()) as VkDeviceSize;
		let offset = offset as _;
		assert!(size + offset <= self.index_size);
		self.transfer.transfer_buffer(self.index_buffer, indices, offset);
	}

	pub(crate) fn vertex_buffer(&self) -> VkBuffer {
		self.vertex_buffer
	}
	pub(crate) fn index_buffer(&self) -> VkBuffer {
		self.index_buffer
	}

	pub(crate) fn index_count(&self) -> VkDeviceSize {
		self.index_size / size_of::<Index>() as VkDeviceSize
	}
}

impl Drop for Mesh {
	fn drop(&mut self) {
		unsafe {
			vkDestroyBuffer(self.device.device(), self.vertex_buffer, null());
			vkDestroyBuffer(self.device.device(), self.index_buffer, null());
			self.allocator.deallocate_buffer(self.vertex_buffer);
			self.allocator.deallocate_buffer(self.index_buffer);
		}
	}
}
