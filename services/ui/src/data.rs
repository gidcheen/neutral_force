use crate::device::Device;
use std::mem::size_of;
use std::ptr::*;
use std::sync::*;
use ui_sys::*;

pub(crate) type DataId = usize;
pub(crate) struct Data<T> {
	device: Arc<Device>,

	size: VkDeviceSize,
	buffer: VkBuffer,
	memory: VkDeviceMemory,
	elements: Mutex<Elements<T>>,
}

struct Elements<T> {
	ptr: *mut T,
	free: Vec<DataId>,
}

impl<T> Data<T> {
	pub fn new(device: Arc<Device>, count: usize) -> Self {
		unsafe {
			let size = (count * size_of::<T>()) as _;
			let buffer_ci = VkBufferCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
				size,
				usage: VkBufferUsageFlagBits::VK_BUFFER_USAGE_STORAGE_BUFFER_BIT as u32,
				..Default::default()
			};
			let mut buffer = null_mut();
			vkCreateBuffer(device.device(), &buffer_ci, null(), &mut buffer);

			let mut requirements = Default::default();
			vkGetBufferMemoryRequirements(device.device(), buffer, &mut requirements);

			let memory_index = device.memory_index(
				requirements.memoryTypeBits,
				VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_CACHED_BIT as u32 | VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_COHERENT_BIT as u32,
			);

			let mut memory = null_mut();
			vkAllocateMemory(
				device.device(),
				&VkMemoryAllocateInfo {
					sType: VkStructureType::VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO as _,
					allocationSize: size,
					memoryTypeIndex: memory_index,
					..Default::default()
				},
				null(),
				&mut memory,
			);

			let mut ptr = null_mut();
			vkMapMemory(device.device(), memory, 0, size, 0, &mut ptr);

			Self {
				device,
				size,
				buffer,
				memory,
				elements: Mutex::new(Elements {
					ptr: ptr as _,
					free: (0..count).collect(),
				}),
			}
		}
	}

	pub(crate) fn add(&self, value: T) -> DataId {
		let mut elements = self.elements.lock().unwrap();
		let id = elements.free.pop().unwrap();
		unsafe { *elements.ptr.add(id) = value };
		id
	}
	pub(crate) fn set(&self, id: DataId, value: T) {
		let mut elements = self.elements.lock().unwrap();
		unsafe { *elements.ptr.add(id) = value };
	}
	pub(crate) fn remove(&self, id: DataId) {
		let mut elements = self.elements.lock().unwrap();
		elements.free.push(id);
	}

	pub(crate) fn descriptor_info(&self) -> VkDescriptorBufferInfo {
		VkDescriptorBufferInfo {
			buffer: self.buffer,
			offset: 0,
			range: VK_WHOLE_SIZE as _,
		}
	}
}

impl<T> Drop for Data<T> {
	fn drop(&mut self) {
		unsafe {
			vkFreeMemory(self.device.device(), self.memory, null());
			vkDestroyBuffer(self.device.device(), self.buffer, null());
		}
	}
}
