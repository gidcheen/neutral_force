use crate::allocator::Allocator;
use crate::device::Device;
use crate::layout::Layout;
use std::ptr::*;
use std::sync::*;
use ui_sys::VkExtent2D;
use ui_sys::*;

pub struct Frame {
	device: Arc<Device>,
	allocator: Arc<Allocator>,
	layout: Arc<Layout>,

	extent: VkExtent2D,
	color_image: VkImage,
	depth_image: VkImage,
	color_image_view: VkImageView,
	depth_image_view: VkImageView,
	framebuffer: VkFramebuffer,
}

struct Framebuffer {}

impl Frame {
	pub(crate) fn new(device: Arc<Device>, allocator: Arc<Allocator>, layout: Arc<Layout>, width: u32, height: u32) -> Self {
		unsafe {
			let extent = VkExtent2D { width, height };

			let color_image_ci = VkImageCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
				imageType: VkImageType::VK_IMAGE_TYPE_2D,
				format: VkFormat::VK_FORMAT_R32G32B32A32_SFLOAT,
				extent: VkExtent3D { width, height, depth: 1 },
				mipLevels: 1,
				arrayLayers: 1,
				samples: VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT,
				tiling: VkImageTiling::VK_IMAGE_TILING_OPTIMAL,
				usage: VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT as u32
					| VkImageUsageFlagBits::VK_IMAGE_USAGE_SAMPLED_BIT as u32
					| VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_SRC_BIT as u32,
				initialLayout: VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
				..Default::default()
			};
			let mut color_image = null_mut();
			vkCreateImage(device.device(), &color_image_ci, null(), &mut color_image);
			allocator.allocate_image(color_image);

			let depth_image_ci = VkImageCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
				imageType: VkImageType::VK_IMAGE_TYPE_2D,
				format: VkFormat::VK_FORMAT_D32_SFLOAT,
				extent: VkExtent3D { width, height, depth: 1 },
				mipLevels: 1,
				arrayLayers: 1,
				samples: VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT,
				tiling: VkImageTiling::VK_IMAGE_TILING_OPTIMAL,
				usage: VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT as u32
					| VkImageUsageFlagBits::VK_IMAGE_USAGE_SAMPLED_BIT as u32
					| VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_SRC_BIT as u32,
				initialLayout: VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
				..Default::default()
			};
			let mut depth_image = null_mut();
			vkCreateImage(device.device(), &depth_image_ci, null(), &mut depth_image);
			allocator.allocate_image(depth_image);

			let color_image_view_ci = VkImageViewCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
				image: color_image,
				viewType: VkImageViewType::VK_IMAGE_VIEW_TYPE_2D,
				format: VkFormat::VK_FORMAT_R32G32B32A32_SFLOAT,
				subresourceRange: VkImageSubresourceRange {
					aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as _,
					baseMipLevel: 0,
					levelCount: 1,
					baseArrayLayer: 0,
					layerCount: 1,
				},
				..Default::default()
			};
			let mut color_image_view = null_mut();
			vkCreateImageView(device.device(), &color_image_view_ci, null(), &mut color_image_view);

			let depth_image_view_ci = VkImageViewCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
				image: depth_image,
				viewType: VkImageViewType::VK_IMAGE_VIEW_TYPE_2D,
				format: VkFormat::VK_FORMAT_D32_SFLOAT,
				subresourceRange: VkImageSubresourceRange {
					aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_DEPTH_BIT as _,
					baseMipLevel: 0,
					levelCount: 1,
					baseArrayLayer: 0,
					layerCount: 1,
				},
				..Default::default()
			};
			let mut depth_image_view = null_mut();
			vkCreateImageView(device.device(), &depth_image_view_ci, null(), &mut depth_image_view);

			let attachments = [color_image_view, depth_image_view];

			let framebuffer_ci = VkFramebufferCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
				renderPass: layout.render_pass(),
				attachmentCount: attachments.len() as _,
				pAttachments: attachments.as_ptr(),
				width,
				height,
				layers: 1,
				..Default::default()
			};

			let mut framebuffer = null_mut();
			vkCreateFramebuffer(device.device(), &framebuffer_ci, null(), &mut framebuffer);

			Self {
				device,
				allocator,
				layout,
				extent,
				color_image,
				depth_image,
				color_image_view,
				depth_image_view,
				framebuffer,
			}
		}
	}

	pub(crate) fn framebuffer(&self) -> VkFramebuffer {
		self.framebuffer
	}
}

impl Drop for Frame {
	fn drop(&mut self) {
		unsafe {
			vkDestroyImageView(self.device.device(), self.color_image_view, null());
			vkDestroyImageView(self.device.device(), self.depth_image_view, null());
			vkDestroyImage(self.device.device(), self.color_image, null());
			vkDestroyImage(self.device.device(), self.depth_image, null());
			vkDestroyFramebuffer(self.device.device(), self.framebuffer, null());
			self.allocator.deallocate_image(self.color_image);
			self.allocator.deallocate_image(self.depth_image);
		}
	}
}
