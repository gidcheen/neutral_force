use crate::device::Device;
use std::ptr::*;
use std::sync::*;
use ui_sys::*;

pub struct Layout {
	device: Arc<Device>,
	render_pass: VkRenderPass,
	descriptor_set_layout: VkDescriptorSetLayout,
}

impl Layout {
	pub(crate) fn new(device: Arc<Device>) -> Self {
		unsafe {
			let attachment_descriptions = [
				VkAttachmentDescription {
					format: VkFormat::VK_FORMAT_R32G32B32A32_SFLOAT,
					samples: VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT,
					loadOp: VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
					storeOp: VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
					stencilLoadOp: VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
					stencilStoreOp: VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
					initialLayout: VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
					finalLayout: VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
					..Default::default()
				},
				VkAttachmentDescription {
					format: VkFormat::VK_FORMAT_D32_SFLOAT,
					samples: VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT,
					loadOp: VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
					storeOp: VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
					stencilLoadOp: VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
					stencilStoreOp: VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
					initialLayout: VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
					finalLayout: VkImageLayout::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
					..Default::default()
				},
			];

			let color_attachments = [VkAttachmentReference {
				attachment: 0,
				layout: VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			}];

			let depth_attachment = VkAttachmentReference {
				attachment: 1,
				layout: VkImageLayout::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			};

			let subpass_descriptions = [VkSubpassDescription {
				pipelineBindPoint: VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS,
				colorAttachmentCount: color_attachments.len() as _,
				pColorAttachments: color_attachments.as_ptr(),
				pDepthStencilAttachment: &depth_attachment,
				..Default::default()
			}];

			let render_pass_ci = VkRenderPassCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
				attachmentCount: attachment_descriptions.len() as _,
				pAttachments: attachment_descriptions.as_ptr(),
				subpassCount: subpass_descriptions.len() as _,
				pSubpasses: subpass_descriptions.as_ptr(),
				..Default::default()
			};

			let mut render_pass = null_mut();
			vkCreateRenderPass(device.device(), &render_pass_ci, null(), &mut render_pass);

			let bindings = [
				// lights
				VkDescriptorSetLayoutBinding {
					binding: 0,
					descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
					descriptorCount: 1,
					stageFlags: VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT as u32 | VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT as u32,
					..Default::default()
				},
				// surfaces
				VkDescriptorSetLayoutBinding {
					binding: 1,
					descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
					descriptorCount: 1,
					stageFlags: VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT as u32 | VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT as u32,
					..Default::default()
				},
				// cameras
				VkDescriptorSetLayoutBinding {
					binding: 2,
					descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
					descriptorCount: 1,
					stageFlags: VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT as u32 | VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT as u32,
					..Default::default()
				},
			];

			let descriptor_set_layout_ci = VkDescriptorSetLayoutCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
				bindingCount: bindings.len() as _,
				pBindings: bindings.as_ptr(),
				..Default::default()
			};
			let mut descriptor_set_layout = null_mut();
			vkCreateDescriptorSetLayout(device.device(), &descriptor_set_layout_ci, null(), &mut descriptor_set_layout);

			Self {
				device,
				render_pass,
				descriptor_set_layout,
			}
		}
	}

	pub(crate) fn render_pass(&self) -> VkRenderPass {
		self.render_pass
	}
	pub(crate) fn descriptor_set_layout(&self) -> VkDescriptorSetLayout {
		self.descriptor_set_layout
	}
}

impl Drop for Layout {
	fn drop(&mut self) {
		unsafe {
			vkDestroyDescriptorSetLayout(self.device.device(), self.descriptor_set_layout, null());
			vkDestroyRenderPass(self.device.device(), self.render_pass, null());
		}
	}
}
