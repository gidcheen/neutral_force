use crate::allocator::Allocator;
use crate::device::Device;
use crate::transfer::Transfer;
use std::mem::zeroed;
use std::ptr::*;
use std::sync::*;
use ui_sys::*;

pub struct Texture {
	device: Arc<Device>,
	allocator: Arc<Allocator>,
	transfer: Arc<Transfer>,

	extent: VkExtent2D,
	image: VkImage,
	image_view: VkImageView,
	sampler: VkSampler,
}

impl Texture {
	pub(crate) fn new<T>(device: Arc<Device>, allocator: Arc<Allocator>, transfer: Arc<Transfer>, data: &[T], width: u32, height: u32) -> Self {
		let extent = VkExtent2D { width, height };
		let extent3d = VkExtent3D { width, height, depth: 1 };
		let image_ci = VkImageCreateInfo {
			sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
			imageType: VkImageType::VK_IMAGE_TYPE_2D,
			format: VkFormat::VK_FORMAT_R8G8B8A8_UNORM,
			extent: extent3d,
			mipLevels: 1,
			arrayLayers: 1,
			samples: VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT,
			tiling: VkImageTiling::VK_IMAGE_TILING_OPTIMAL,
			usage: VkImageUsageFlagBits::VK_IMAGE_USAGE_SAMPLED_BIT as u32
				| VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_DST_BIT as u32
				| VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_SRC_BIT as u32,
			initialLayout: VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
			..Default::default()
		};

		let mut image = null_mut();
		unsafe { vkCreateImage(device.device(), &image_ci, null(), &mut image) }.success();

		allocator.allocate_image(image);
		transfer.transfer_image(image, data, Default::default(), extent3d, 0, 1, VkImageLayout::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

		let image_view_ci = VkImageViewCreateInfo {
			sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			image,
			viewType: VkImageViewType::VK_IMAGE_VIEW_TYPE_2D,
			format: VkFormat::VK_FORMAT_R8G8B8A8_UNORM,
			subresourceRange: VkImageSubresourceRange {
				aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as _,
				baseMipLevel: 0,
				levelCount: 1,
				baseArrayLayer: 0,
				layerCount: 1,
			},
			..Default::default()
		};

		let mut image_view = null_mut();
		unsafe { vkCreateImageView(device.device(), &image_view_ci, null(), &mut image_view) };

		let sampler_ci = VkSamplerCreateInfo {
			sType: VkStructureType::VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
			magFilter: VkFilter::VK_FILTER_LINEAR,
			minFilter: VkFilter::VK_FILTER_LINEAR,
			mipmapMode: VkSamplerMipmapMode::VK_SAMPLER_MIPMAP_MODE_NEAREST,
			addressModeU: VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_REPEAT,
			addressModeV: VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_REPEAT,
			addressModeW: VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_REPEAT,
			compareOp: VkCompareOp::VK_COMPARE_OP_ALWAYS,
			borderColor: VkBorderColor::VK_BORDER_COLOR_INT_OPAQUE_WHITE,
			..Default::default()
		};

		let mut sampler = null_mut();
		unsafe { vkCreateSampler(device.device(), &sampler_ci, null(), &mut sampler) }.success();

		Self {
			device,
			allocator,
			transfer,
			extent,
			image,
			image_view,
			sampler,
		}
	}
	pub(crate) fn descriptor_info(&self) -> VkDescriptorImageInfo {
		VkDescriptorImageInfo {
			sampler: self.sampler,
			imageView: self.image_view,
			imageLayout: VkImageLayout::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
		}
	}
}

impl Drop for Texture {
	fn drop(&mut self) {
		unsafe { vkDestroyImage(self.device.device(), self.image, null()) };
		unsafe { vkDestroyImageView(self.device.device(), self.image_view, null()) };
		unsafe { vkDestroySampler(self.device.device(), self.sampler, null()) };
		self.allocator.deallocate_image(self.image);
	}
}
