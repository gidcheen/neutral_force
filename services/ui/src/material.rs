use crate::allocator::Allocator;
use crate::device::Device;
use crate::transfer::Transfer;
use std::mem::*;
use std::ptr::*;
use std::sync::*;
use ui_sys::*;

pub struct Material {
	device: Arc<Device>,
	allocator: Arc<Allocator>,
	transfer: Arc<Transfer>,
	size: VkDeviceSize,
	buffer: VkBuffer,
}

impl Material {
	pub(crate) fn new<T>(device: Arc<Device>, allocator: Arc<Allocator>, transfer: Arc<Transfer>, data: &[T]) -> Self {
		unsafe {
			let size = (data.len() * size_of::<T>()) as VkDeviceSize;
			let buffer_ci = VkBufferCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
				size,
				usage: VkBufferUsageFlagBits::VK_BUFFER_USAGE_TRANSFER_DST_BIT as u32 | VkBufferUsageFlagBits::VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT as u32,
				..Default::default()
			};
			let mut buffer = null_mut();
			vkCreateBuffer(device.device(), &buffer_ci, null(), &mut buffer);

			allocator.allocate_buffer(buffer);
			transfer.transfer_buffer(buffer, data, 0);

			Self {
				device,
				allocator,
				transfer,
				buffer,
				size,
			}
		}
	}

	pub fn set<T>(&mut self, data: &[T], offset: usize) {
		let size = (data.len() * size_of::<T>()) as VkDeviceSize;
		let offset = offset as _;
		assert!(size + offset <= self.size);
		self.transfer.transfer_buffer(self.buffer, data, offset);
	}

	pub(crate) fn descriptor_info(&self) -> VkDescriptorBufferInfo {
		VkDescriptorBufferInfo {
			buffer: self.buffer,
			offset: 0,
			range: VK_WHOLE_SIZE as _,
		}
	}
}

impl Drop for Material {
	fn drop(&mut self) {
		unsafe {
			vkDestroyBuffer(self.device.device(), self.buffer, null());
			self.allocator.deallocate_buffer(self.buffer);
		}
	}
}
