use std::ffi::*;
use std::os::raw::*;
use std::ptr::*;
use std::slice;
use std::sync::Mutex;
use std::sync::Once;
use ui_sys::*;

pub(crate) struct Device {
	instance: VkInstance,
	physical_device: VkPhysicalDevice,
	memory_properties: VkPhysicalDeviceMemoryProperties,
	device: VkDevice,
	queue_properties: Vec<VkQueueFamilyProperties>,
	queues: Vec<Mutex<VkQueue>>,
}

impl Device {
	pub(crate) fn new(app_name: &str) -> Self {
		let app_name = CString::new(app_name).unwrap();
		let engine_name = CString::new("Gid Engine").unwrap();

		static INIT: Once = Once::new();
		INIT.call_once(|| unsafe {
			assert!(glfwInit() == GLFW_TRUE as _);
			glfwSetErrorCallback(Some(glfw_error_cb));
			glfwWindowHint(GLFW_CLIENT_API as _, GLFW_NO_API as _);
		});

		let extensions = unsafe {
			let mut extensions_count = 0;
			let extensions = glfwGetRequiredInstanceExtensions(&mut extensions_count);
			slice::from_raw_parts(extensions, extensions_count as _)
		};

		#[cfg(debug_assertions)]
		let layers = [b"VK_LAYER_KHRONOS_validation\0".as_ptr() as _];
		#[cfg(not(debug_assertions))]
		let layers = [];

		let application_info = VkApplicationInfo {
			sType: VkStructureType::VK_STRUCTURE_TYPE_APPLICATION_INFO,
			pApplicationName: app_name.as_ptr(),
			applicationVersion: unsafe { vkMakeApiVersion(0, 1, 0, 0) },
			pEngineName: engine_name.as_ptr(),
			engineVersion: unsafe { vkMakeApiVersion(0, 1, 0, 0) },
			apiVersion: unsafe { vkMakeApiVersion(1, 2, 0, 0) },
			..Default::default()
		};
		let instance_create_info = VkInstanceCreateInfo {
			sType: VkStructureType::VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
			enabledLayerCount: layers.len() as _,
			ppEnabledLayerNames: layers.as_ptr(),
			enabledExtensionCount: extensions.len() as _,
			ppEnabledExtensionNames: extensions.as_ptr(),
			pApplicationInfo: &application_info,
			..Default::default()
		};
		let mut instance = null_mut();
		unsafe { vkCreateInstance(&instance_create_info, null(), &mut instance) }.success();

		let mut device_count = 0;
		unsafe { vkEnumeratePhysicalDevices(instance, &mut device_count, null_mut()) }.success();
		let mut physical_devices = vec![null_mut(); device_count as _];
		unsafe { vkEnumeratePhysicalDevices(instance, &mut device_count, physical_devices.as_mut_ptr()) }.success();

		let physical_device = physical_devices.iter().find(|pd| {
			let mut device_properties = Default::default();
			unsafe { vkGetPhysicalDeviceProperties(**pd, &mut device_properties) };
			device_properties.deviceType == VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU
		});
		let physical_device = physical_device.cloned().unwrap_or(physical_devices[0]);

		let mut memory_properties = Default::default();
		unsafe { vkGetPhysicalDeviceMemoryProperties(physical_device, &mut memory_properties) };

		let mut queue_property_count = 0;
		unsafe { vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &mut queue_property_count, null_mut()) };
		let mut queue_properties = vec![Default::default(); queue_property_count as usize];
		unsafe { vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &mut queue_property_count, queue_properties.as_mut_ptr()) };

		let mut queue_priority = 1.0;
		let queue_create_infos = (0..queue_properties.len()).map(|i| VkDeviceQueueCreateInfo {
			sType: VkStructureType::VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
			queueFamilyIndex: i as _,
			queueCount: 1,
			pQueuePriorities: &mut queue_priority,
			..Default::default()
		});
		let queue_create_infos = queue_create_infos.collect::<Vec<_>>();

		let device_features = VkPhysicalDeviceFeatures { ..Default::default() };

		#[cfg(not(target_os = "macos"))]
		let device_extensions = [VK_KHR_SWAPCHAIN_EXTENSION_NAME.as_ptr() as _];
		#[cfg(target_os = "macos")]
		let device_extensions = [VK_KHR_SWAPCHAIN_EXTENSION_NAME.as_ptr() as _, b"VK_KHR_portability_subset\0".as_ptr() as _];

		let device_create_info = VkDeviceCreateInfo {
			sType: VkStructureType::VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
			queueCreateInfoCount: queue_create_infos.len() as _,
			pQueueCreateInfos: queue_create_infos.as_ptr(),
			enabledExtensionCount: device_extensions.len() as _,
			ppEnabledExtensionNames: device_extensions.as_ptr(),
			pEnabledFeatures: &device_features,
			..Default::default()
		};

		let mut device = null_mut();
		unsafe { vkCreateDevice(physical_device, &device_create_info, null(), &mut device) }.success();

		let queues = (0..queue_properties.len()).map(|i| {
			let mut queue = null_mut();
			unsafe { vkGetDeviceQueue(device, i as _, 0, &mut queue) };
			Mutex::new(queue)
		});
		let queues = queues.collect();

		unsafe {
			extern "C" fn data_callback(pDevice: *mut ma_device, pOutput: *mut c_void, pInput: *const c_void, frameCount: ma_uint32) {
				// In playback mode copy data to pOutput. In capture mode read data from pInput. In full-duplex mode, both
				// pOutput and pInput will be valid and you can move data from pInput into pOutput. Never process more than
				// frameCount frames.
			}

			let mut config = ma_device_config_init(ma_device_type::ma_device_type_playback);
			config.playback.format = ma_format::ma_format_f32; // Set to ma_format_unknown to use the device's native format.
			config.playback.channels = 2; // Set to 0 to use the device's native channel count.
			config.sampleRate = 48000; // Set to 0 to use the device's native sample rate.
			config.dataCallback = Some(data_callback); // This function will be called when miniaudio needs more data.
										   // config.pUserData = pMyCustomData; // Can be accessed from the device object (device.pUserData).

			let mut device = ma_device::default();
			if ma_device_init(null_mut(), &config, &mut device) != ma_result::MA_SUCCESS {
				panic!();
				// return -1; // Failed to initialize the device.
			}

			ma_device_start(&mut device); // The device is sleeping by default so you'll need to start it manually.

			// Do something here. Probably your program's main loop.

			ma_device_uninit(&mut device); // This will stop the device so no need to do that manually.
		}

		Self {
			instance,
			physical_device,
			memory_properties,
			device,
			queue_properties,
			queues,
		}
	}

	pub(crate) fn instance(&self) -> VkInstance {
		self.instance
	}

	pub(crate) fn physical_device(&self) -> VkPhysicalDevice {
		self.physical_device
	}

	pub(crate) fn memory_properties(&self) -> &VkPhysicalDeviceMemoryProperties {
		&self.memory_properties
	}

	pub(crate) fn device(&self) -> VkDevice {
		self.device
	}

	pub(crate) fn queue(&self, flags: VkQueueFlags) -> (&Mutex<VkQueue>, u32) {
		(0..self.queues.len())
			.find(|&i| (self.queue_properties[i].queueFlags & flags) > 0)
			.map(|i| (&self.queues[i], i as _))
			.expect("no vk queue found")
	}

	pub(crate) fn memory_index(&self, bits: u32, flags: VkMemoryPropertyFlags) -> u32 {
		(0..self.memory_properties.memoryTypeCount)
			.find(|&i| {
				let f = self.memory_properties.memoryTypes[i as usize].propertyFlags;
				let bit_match = (bits & (1 << i)) > 0;
				let flag_match = (flags & f) > 0;
				bit_match && flag_match
			})
			.expect("no vk memory found")
	}
}

impl Drop for Device {
	fn drop(&mut self) {
		unsafe { vkDestroyDevice(self.device, null()) };
		unsafe { vkDestroyInstance(self.instance, null()) };
	}
}

unsafe extern "C" fn glfw_error_cb(id: c_int, message: *const c_char) {
	let message = CStr::from_ptr(message).to_str().unwrap();
	let log = format!("glfw error message {id}: {message}");
	panic!("{log}");
}
