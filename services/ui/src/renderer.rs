use crate::data::Data;
use crate::device::Device;
use crate::frame::Frame;
use crate::layout::Layout;
use crate::material::Material;
use crate::mesh::Mesh;
use crate::pipeline::Pipeline;
use crate::scene::*;
use crate::texture::Texture;
use algebra::*;
use std::mem::size_of;
use std::ptr::*;
use std::sync::*;
use ui_sys::*;

pub(crate) struct Renderer {
	device: Arc<Device>,
	layout: Arc<Layout>,
	cameras: Data<CameraData>,
	lights: Data<LightData>,
	surfaces: Data<SurfaceData>,
	executor: Mutex<Executor>,
}

struct Executor {
	command_pool: VkCommandPool,
	descriptor_pool: VkDescriptorPool,
	draw_fence: VkFence,
	// aquired_semaphore:VkSemaphore,
	// present_fence:VkFence,
}

#[repr(C)]
#[derive(Clone, Copy, Default)]
pub(crate) struct CameraData {
	pub global_to_local: Matrix4f,
	pub local_to_global: Matrix4f,
	pub local_to_projection: Matrix4f,
}
#[repr(C)]
#[derive(Clone, Copy, Default)]
pub(crate) struct LightData {
	pub global_to_local: Matrix4f,
	pub local_to_global: Matrix4f,
	pub color: [f32; 4],
	pub light_type: i32,
	pub cutoff_inner: f32,
	pub cutoff_outer: f32,
	pub _pad: i32,
}
#[repr(C)]
#[derive(Clone, Copy, Default)]
pub(crate) struct SurfaceData {
	pub local_to_global: Matrix4f,
}

pub(crate) struct Draw {
	camera: Camera,
	lights: Vec<Light>,
	surface: Surface,

	pipeline: Arc<Pipeline>,
	mesh: Arc<Mesh>,
	material: Arc<Material>,
	images: Vec<Arc<Texture>>,
}

struct ObjectIndices {
	light_count: u16,
	lights: [u16; 11],
	surface: u16,
	camera: u16,
}

impl Renderer {
	const MAX_CAMERAS: usize = 128;
	const MAX_LIGHTS: usize = 4098;
	const MAX_SURFACES: usize = 4098;
	const MAX_MATERIALS: usize = 4098;
	const MAX_TEXTURES: usize = 4098;

	pub(crate) fn new(device: Arc<Device>, layout: Arc<Layout>) -> Self {
		let cameras = Data::new(device.clone(), Self::MAX_CAMERAS);
		let lights = Data::new(device.clone(), Self::MAX_LIGHTS);
		let surfaces = Data::new(device.clone(), Self::MAX_SURFACES);

		unsafe {
			let (_, queue_index) = device.queue(VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT as _);

			let command_pool_ci = VkCommandPoolCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
				queueFamilyIndex: queue_index,
				..Default::default()
			};

			let mut command_pool = null_mut();
			vkCreateCommandPool(device.device(), &command_pool_ci, null(), &mut command_pool);

			let descriptor_pool_sizes = [
				// lights
				VkDescriptorPoolSize {
					type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
					descriptorCount: 1,
				},
				// surfaces
				VkDescriptorPoolSize {
					type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
					descriptorCount: 1,
				},
				// cameras
				VkDescriptorPoolSize {
					type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
					descriptorCount: 1,
				},
				// material
				VkDescriptorPoolSize {
					type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					descriptorCount: Self::MAX_MATERIALS as _,
				},
				// textures
				VkDescriptorPoolSize {
					type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
					descriptorCount: Self::MAX_TEXTURES as _,
				},
			];

			let descriptor_pool_ci = VkDescriptorPoolCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
				maxSets: 1024 * 2 * 2,
				poolSizeCount: descriptor_pool_sizes.len() as _,
				pPoolSizes: descriptor_pool_sizes.as_ptr(),
				..Default::default()
			};

			let fence_ci = VkFenceCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
				..Default::default()
			};
			let mut fence = null_mut();
			vkCreateFence(device.device(), &fence_ci, null(), &mut fence);

			let mut descriptor_pool = null_mut();
			vkCreateDescriptorPool(device.device(), &descriptor_pool_ci, null(), &mut descriptor_pool);

			Self {
				device,
				layout,
				cameras,
				lights,
				surfaces,
				executor: Mutex::new(Executor {
					command_pool,
					descriptor_pool,
					draw_fence: fence,
				}),
			}
		}
	}

	pub fn render(&self, frame: Arc<Frame>, draws: &[Draw], width: u32, height: u32) {
		unsafe {
			let executor = self.executor.lock().unwrap();

			let mut command_buffer = null_mut();
			let command_pool_ai = VkCommandBufferAllocateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
				commandPool: executor.command_pool,
				level: VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY,
				commandBufferCount: 1,
				..Default::default()
			};
			vkAllocateCommandBuffers(self.device.device(), &command_pool_ai, &mut command_buffer);

			let command_buffer_begin_info = VkCommandBufferBeginInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
				..Default::default()
			};
			vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);

			let viewport = VkViewport {
				x: 0.0,
				y: 0.0,
				width: width as _,
				height: height as _,
				minDepth: 0.0,
				maxDepth: 1.0,
			};
			vkCmdSetViewport(command_buffer, 0, 1, &viewport);
			let scissor = VkRect2D {
				offset: VkOffset2D { x: 0, y: 0 },
				extent: VkExtent2D { width, height },
			};
			vkCmdSetScissor(command_buffer, 0, 1, &scissor);

			// render pass

			let clear_values = [
				VkClearValue {
					color: VkClearColorValue { float32: [0.5, 0.0, 0.5, 1.0] },
				},
				VkClearValue {
					depthStencil: VkClearDepthStencilValue { depth: 1.0, stencil: 0 },
				},
			];
			let render_pass_bi = VkRenderPassBeginInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
				renderPass: self.layout.render_pass(),
				framebuffer: frame.framebuffer(),
				renderArea: VkRect2D {
					offset: VkOffset2D { x: 0, y: 0 },
					extent: VkExtent2D { width, height },
				},
				clearValueCount: clear_values.len() as _,
				pClearValues: clear_values.as_ptr(),
				..Default::default()
			};
			vkCmdBeginRenderPass(command_buffer, &render_pass_bi, VkSubpassContents::VK_SUBPASS_CONTENTS_INLINE);

			// commmon descriptors

			let common_descriptor_set_layout = self.layout.descriptor_set_layout();
			let common_descriptor_set_ai = VkDescriptorSetAllocateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
				descriptorPool: executor.descriptor_pool,
				descriptorSetCount: 1,
				pSetLayouts: &common_descriptor_set_layout,
				..Default::default()
			};
			let mut common_descriptor_set = null_mut();
			vkAllocateDescriptorSets(self.device.device(), &common_descriptor_set_ai, &mut common_descriptor_set);

			let lights_descriptor_buffer_info = self.lights.descriptor_info();
			let surface_descriptor_buffer_info = self.surfaces.descriptor_info();
			let cameras_descriptor_buffer_info = self.cameras.descriptor_info();

			let common_write_descriptor_set = [
				VkWriteDescriptorSet {
					sType: VkStructureType::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
					dstSet: common_descriptor_set,
					dstBinding: 0,
					dstArrayElement: 0,
					descriptorCount: 1,
					descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
					pBufferInfo: &lights_descriptor_buffer_info,
					..Default::default()
				},
				VkWriteDescriptorSet {
					sType: VkStructureType::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
					dstSet: common_descriptor_set,
					dstBinding: 1,
					dstArrayElement: 0,
					descriptorCount: 1,
					descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
					pBufferInfo: &surface_descriptor_buffer_info,
					..Default::default()
				},
				VkWriteDescriptorSet {
					sType: VkStructureType::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
					dstSet: common_descriptor_set,
					dstBinding: 2,
					dstArrayElement: 0,
					descriptorCount: 1,
					descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
					pBufferInfo: &cameras_descriptor_buffer_info,
					..Default::default()
				},
			];
			vkUpdateDescriptorSets(
				self.device.device(),
				common_write_descriptor_set.len() as _,
				common_write_descriptor_set.as_ptr(),
				0,
				null(),
			);

			// draws

			let mut last_pipeline = None;
			for draw in draws.iter() {
				if last_pipeline.is_none() || !Arc::ptr_eq(&draw.pipeline, &last_pipeline.as_ref().unwrap_unchecked()) {
					vkCmdBindPipeline(command_buffer, VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS, draw.pipeline.pipeline());
					last_pipeline = Some(draw.pipeline.clone());
				}

				let vertex_buffer = draw.mesh.vertex_buffer();
				let vertex_buffer_offset = 0;
				vkCmdBindVertexBuffers(command_buffer, 0, 1, &vertex_buffer, &vertex_buffer_offset);
				let index_buffer = draw.mesh.index_buffer();
				vkCmdBindIndexBuffer(command_buffer, index_buffer, 0, VkIndexType::VK_INDEX_TYPE_UINT32);

				let instance_descriptor_set_layout = draw.pipeline.descriptor_set_layout();
				let instance_descriptor_set_allocate_info = VkDescriptorSetAllocateInfo {
					sType: VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
					descriptorPool: executor.descriptor_pool,
					descriptorSetCount: 1,
					pSetLayouts: &instance_descriptor_set_layout,
					..Default::default()
				};
				let mut instance_descriptor_set = null_mut();
				vkAllocateDescriptorSets(self.device.device(), &instance_descriptor_set_allocate_info, &mut instance_descriptor_set);

				let material_descriptor_buffer_info = draw.material.descriptor_info();

				let mut common_write_descriptor_set = vec![VkWriteDescriptorSet {
					sType: VkStructureType::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
					dstSet: instance_descriptor_set,
					dstBinding: 10,
					dstArrayElement: 0,
					descriptorCount: 1,
					descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					pBufferInfo: &material_descriptor_buffer_info,
					..Default::default()
				}];
				let mut descriptor_image_infos = vec![];
				let mut image_array_index = 0;
				for image in draw.images.iter() {
					let image_descriptor_image_info = Arc::new(image.descriptor_info());
					descriptor_image_infos.push(image_descriptor_image_info.clone());

					common_write_descriptor_set.push(VkWriteDescriptorSet {
						sType: VkStructureType::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
						dstSet: instance_descriptor_set,
						dstBinding: 11,
						dstArrayElement: image_array_index,
						descriptorCount: 1,
						descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
						pImageInfo: image_descriptor_image_info.as_ref() as *const _,
						..Default::default()
					});
					image_array_index += 1;
				}

				vkUpdateDescriptorSets(
					self.device.device(),
					common_write_descriptor_set.len() as _,
					common_write_descriptor_set.as_ptr(),
					0,
					null(),
				);
				let descritpro_sets = [common_descriptor_set, instance_descriptor_set];
				vkCmdBindDescriptorSets(
					command_buffer,
					VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS,
					draw.pipeline.pipeline_layout(),
					0,
					descritpro_sets.len() as _,
					descritpro_sets.as_ptr(),
					0,
					null(),
				);

				let mut object_indices = ObjectIndices {
					light_count: draw.lights.len() as _,
					surface: draw.surface.id() as _,
					camera: draw.camera.id() as _,
					lights: [0; 11],
				};
				for (light_index, light) in draw.lights.iter().enumerate() {
					object_indices.lights[light_index] = light.id() as _;
				}
				vkCmdPushConstants(
					command_buffer,
					draw.pipeline.pipeline_layout(),
					VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT as u32 | VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT as u32,
					0,
					size_of::<ObjectIndices>() as _,
					&object_indices as *const _ as _,
				);

				vkCmdDrawIndexed(command_buffer, draw.mesh.index_count() as u32 * 3, 1, 0, 0, 0);
			}

			vkCmdEndRenderPass(command_buffer);
			vkEndCommandBuffer(command_buffer);

			{
				let (queue, _) = self.device.queue(VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT as _);
				let queue = queue.lock().unwrap();
				let submit_info = VkSubmitInfo {
					sType: VkStructureType::VK_STRUCTURE_TYPE_SUBMIT_INFO,
					commandBufferCount: 1,
					pCommandBuffers: &command_buffer,
					..Default::default()
				};
				vkQueueSubmit(*queue, 1, &submit_info, executor.draw_fence);
			}

			vkWaitForFences(self.device.device(), 1, &executor.draw_fence, VK_TRUE, u64::MAX);
			vkResetFences(self.device.device(), 1, &executor.draw_fence);

			vkResetDescriptorPool(self.device.device(), executor.descriptor_pool, 0);
			vkFreeCommandBuffers(self.device.device(), executor.command_pool, 1, &command_buffer);
			vkResetCommandPool(self.device.device(), executor.command_pool, 0);
		}
	}

	pub(crate) fn cameras(&self) -> &Data<CameraData> {
		&self.cameras
	}
	pub(crate) fn lights(&self) -> &Data<LightData> {
		&self.lights
	}
	pub(crate) fn surfaces(&self) -> &Data<SurfaceData> {
		&self.surfaces
	}
}

impl Drop for Renderer {
	fn drop(&mut self) {
		unsafe {
			let executor = self.executor.lock().unwrap();
			vkDestroyCommandPool(self.device.device(), executor.command_pool, null());
			vkDestroyDescriptorPool(self.device.device(), executor.descriptor_pool, null());
			vkDestroyFence(self.device.device(), executor.draw_fence, null());
		}
	}
}
