use ui_sys::*;

// #[derive(Copy, Clone, Default, Debug)]
// pub struct WindowSize {
// 	pub width: usize,
// 	pub height: usize,
// }

// #[derive(Copy, Clone, Default, Debug)]
// pub struct WindowPosition {
// 	pub x: usize,
// 	pub y: usize,
// }

#[derive(Clone, Copy, Default, Debug)]
pub struct MousePosition {
	pub x: f64,
	pub y: f64,
}

#[derive(Clone, Copy, Default, Debug)]
pub struct MouseScroll {
	pub x: f64,
	pub y: f64,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum InputAction {
	Down = GLFW_PRESS as _,
	Up = GLFW_RELEASE as _,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum InputModifier {
	Shift = GLFW_MOD_SHIFT as _,
	Control = GLFW_MOD_CONTROL as _,
	Alt = GLFW_MOD_ALT as _,
	Super = GLFW_MOD_SUPER as _,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum MouseButton {
	Left = GLFW_MOUSE_BUTTON_LEFT as _,
	Right = GLFW_MOUSE_BUTTON_RIGHT as _,
	Middle = GLFW_MOUSE_BUTTON_MIDDLE as _,

	Button4 = GLFW_MOUSE_BUTTON_4 as _,
	Button5 = GLFW_MOUSE_BUTTON_5 as _,
	Button6 = GLFW_MOUSE_BUTTON_6 as _,
	Button7 = GLFW_MOUSE_BUTTON_7 as _,
	Button8 = GLFW_MOUSE_BUTTON_8 as _,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum KeyboardKey {
	Key1 = GLFW_KEY_1 as _,
	Key2 = GLFW_KEY_2 as _,
	Key3 = GLFW_KEY_3 as _,
	Key4 = GLFW_KEY_4 as _,
	Key5 = GLFW_KEY_5 as _,
	Key6 = GLFW_KEY_6 as _,
	Key7 = GLFW_KEY_7 as _,
	Key8 = GLFW_KEY_8 as _,
	Key9 = GLFW_KEY_9 as _,
	Key0 = GLFW_KEY_0 as _,

	KeyA = GLFW_KEY_A as _,
	KeyB = GLFW_KEY_B as _,
	KeyC = GLFW_KEY_C as _,
	KeyD = GLFW_KEY_D as _,
	KeyE = GLFW_KEY_E as _,
	KeyF = GLFW_KEY_F as _,
	KeyG = GLFW_KEY_G as _,
	KeyH = GLFW_KEY_H as _,
	KeyI = GLFW_KEY_I as _,
	KeyJ = GLFW_KEY_J as _,
	KeyK = GLFW_KEY_K as _,
	KeyL = GLFW_KEY_L as _,
	KeyM = GLFW_KEY_M as _,
	KeyN = GLFW_KEY_N as _,
	KeyO = GLFW_KEY_O as _,
	KeyP = GLFW_KEY_P as _,
	KeyQ = GLFW_KEY_Q as _,
	KeyR = GLFW_KEY_R as _,
	KeyS = GLFW_KEY_S as _,
	KeyT = GLFW_KEY_T as _,
	KeyU = GLFW_KEY_U as _,
	KeyV = GLFW_KEY_V as _,
	KeyW = GLFW_KEY_W as _,
	KeyX = GLFW_KEY_X as _,
	KeyY = GLFW_KEY_Y as _,
	KeyZ = GLFW_KEY_Z as _,

	Space = GLFW_KEY_SPACE as _,
	GraveAccent = GLFW_KEY_GRAVE_ACCENT as _,
	Minus = GLFW_KEY_MINUS as _,
	Equal = GLFW_KEY_EQUAL as _,
	LeftBracket = GLFW_KEY_LEFT_BRACKET as _,
	RightBracket = GLFW_KEY_RIGHT_BRACKET as _,
	Semicolon = GLFW_KEY_SEMICOLON as _,
	Apostrophe = GLFW_KEY_APOSTROPHE as _,
	BackSlash = GLFW_KEY_BACKSLASH as _,
	Comma = GLFW_KEY_COMMA as _,
	Period = GLFW_KEY_PERIOD as _,
	Slash = GLFW_KEY_SLASH as _,

	LeftShift = GLFW_KEY_LEFT_SHIFT as _,
	LeftControl = GLFW_KEY_LEFT_CONTROL as _,
	LeftAlt = GLFW_KEY_LEFT_ALT as _,
	LeftSuper = GLFW_KEY_LEFT_SUPER as _,

	RightShift = GLFW_KEY_RIGHT_SHIFT as _,
	RightControl = GLFW_KEY_RIGHT_CONTROL as _,
	RightAlt = GLFW_KEY_RIGHT_ALT as _,
	RightSuper = GLFW_KEY_RIGHT_SUPER as _,

	Print = GLFW_KEY_PRINT_SCREEN as _,
	ScrollLock = GLFW_KEY_SCROLL_LOCK as _,
	Pause = GLFW_KEY_PAUSE as _,

	Tab = GLFW_KEY_TAB as _,
	CapsLock = GLFW_KEY_CAPS_LOCK as _,
	BackSpace = GLFW_KEY_BACKSPACE as _,
	Enter = GLFW_KEY_ENTER as _,

	Escape = GLFW_KEY_ESCAPE as _,

	F1 = GLFW_KEY_F1 as _,
	F2 = GLFW_KEY_F2 as _,
	F3 = GLFW_KEY_F3 as _,
	F4 = GLFW_KEY_F4 as _,
	F5 = GLFW_KEY_F5 as _,
	F6 = GLFW_KEY_F6 as _,
	F7 = GLFW_KEY_F7 as _,
	F8 = GLFW_KEY_F8 as _,
	F9 = GLFW_KEY_F9 as _,
	F10 = GLFW_KEY_F10 as _,
	F11 = GLFW_KEY_F11 as _,
	F12 = GLFW_KEY_F12 as _,

	Insert = GLFW_KEY_INSERT as _,
	Delete = GLFW_KEY_DELETE as _,
	Home = GLFW_KEY_HOME as _,
	End = GLFW_KEY_END as _,
	PageUp = GLFW_KEY_PAGE_UP as _,
	PageDown = GLFW_KEY_PAGE_DOWN as _,

	Left = GLFW_KEY_LEFT as _,
	Right = GLFW_KEY_RIGHT as _,
	Up = GLFW_KEY_UP as _,
	Down = GLFW_KEY_DOWN as _,

	NumLock = GLFW_KEY_NUM_LOCK as _,
	Num0 = GLFW_KEY_KP_0 as _,
	Num1 = GLFW_KEY_KP_1 as _,
	Num2 = GLFW_KEY_KP_2 as _,
	Num3 = GLFW_KEY_KP_3 as _,
	Num4 = GLFW_KEY_KP_4 as _,
	Num5 = GLFW_KEY_KP_5 as _,
	Num6 = GLFW_KEY_KP_6 as _,
	Num7 = GLFW_KEY_KP_7 as _,
	Num8 = GLFW_KEY_KP_8 as _,
	Num9 = GLFW_KEY_KP_9 as _,
	NumDivide = GLFW_KEY_KP_DIVIDE as _,
	NumMultiply = GLFW_KEY_KP_MULTIPLY as _,
	NumSubtract = GLFW_KEY_KP_SUBTRACT as _,
	NumAdd = GLFW_KEY_KP_ADD as _,
	NumDecimal = GLFW_KEY_KP_DECIMAL as _,
	NumEnter = GLFW_KEY_KP_ENTER as _,
}
