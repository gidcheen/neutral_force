use crate::data::DataId;
use crate::renderer::*;
use algebra::*;
use spatial::trs;
use std::sync::*;

pub struct Transform {
	pub translation: Vector3f,
	pub rotation: Quaternionf,
	pub scale: Vector3f,
}

impl Default for Transform {
	fn default() -> Self {
		Self {
			translation: Vector3f::zeroed(),
			rotation: Quaternionf::identity(),
			scale: Vector3f::new([1.0, 1.0, 1.0]),
		}
	}
}

pub struct Camera {
	renderer: Arc<Renderer>,
	id: DataId,
	transform: Transform,
	config: CameraConfig,
}

pub struct CameraConfig {
	pub fov: f32,
	pub aspect: f32,
	pub near: f32,
	pub far: f32,
}

impl Camera {
	pub(crate) fn new(renderer: Arc<Renderer>, transform: Transform, config: CameraConfig) -> Self {
		let id = renderer.cameras().add(Self::camera_data(&transform, &config));
		Self { renderer, id, transform, config }
	}

	pub fn set_transform(&mut self, transform: Transform) {
		self.transform = transform;
		self.renderer.cameras().set(self.id, Self::camera_data(&self.transform, &self.config));
	}
	pub fn set_config(&mut self, config: CameraConfig) {
		self.config = config;
		self.renderer.cameras().set(self.id, Self::camera_data(&self.transform, &self.config));
	}

	pub(crate) fn id(&self) -> DataId {
		self.id
	}

	fn camera_data(transform: &Transform, config: &CameraConfig) -> CameraData {
		let local_to_global = trs(transform.translation, transform.rotation, transform.scale);
		let global_to_local = local_to_global.inverse();
		let local_to_projection = spatial::perspective(config.fov, config.aspect, config.near, config.far);
		CameraData {
			global_to_local,
			local_to_global,
			local_to_projection,
		}
	}
}

impl Drop for Camera {
	fn drop(&mut self) {
		self.renderer.cameras().remove(self.id);
	}
}

pub struct Light {
	renderer: Arc<Renderer>,
	id: DataId,
	transform: Transform,
	config: LightConfig,
}

#[derive(Copy, Clone)]
pub enum LightType {
	Ambient = 1,
	Directional = 2,
	Point = 3,
	Spot = 4,
}

pub struct LightConfig {
	pub light_type: LightType,
	pub color: [f32; 4],
	pub cutoff_inner: f32,
	pub cutoff_outer: f32,
}

impl Light {
	pub(crate) fn new(renderer: Arc<Renderer>, transform: Transform, config: LightConfig) -> Self {
		let id = renderer.lights().add(Self::light_data(&transform, &config));
		Self { renderer, id, transform, config }
	}

	pub fn set_transform(&mut self, transform: Transform) {
		self.transform = transform;
		self.renderer.lights().set(self.id, Self::light_data(&self.transform, &self.config));
	}
	pub fn set_config(&mut self, config: LightConfig) {
		self.config = config;
		self.renderer.lights().set(self.id, Self::light_data(&self.transform, &self.config));
	}

	pub(crate) fn id(&self) -> DataId {
		self.id
	}

	fn light_data(transform: &Transform, config: &LightConfig) -> LightData {
		let local_to_global = trs(transform.translation, transform.rotation, transform.scale);
		let global_to_local = local_to_global.inverse();
		LightData {
			global_to_local,
			local_to_global,
			color: config.color,
			light_type: config.light_type as _,
			cutoff_inner: config.cutoff_inner,
			cutoff_outer: config.cutoff_outer,
			_pad: 0,
		}
	}
}

impl Drop for Light {
	fn drop(&mut self) {
		self.renderer.lights().remove(self.id);
	}
}

pub struct Surface {
	renderer: Arc<Renderer>,
	id: DataId,
	transform: Transform,
}

pub struct SurfaceConfig {}

impl Surface {
	pub(crate) fn new(renderer: Arc<Renderer>, transform: Transform) -> Self {
		let id = renderer.surfaces().add(Self::surface_data(&transform));
		Self { renderer, id, transform }
	}

	pub fn set_transform(&mut self, transform: Transform) {
		self.transform = transform;
		self.renderer.surfaces().set(self.id, Self::surface_data(&self.transform));
	}

	pub(crate) fn id(&self) -> DataId {
		self.id
	}

	fn surface_data(transform: &Transform) -> SurfaceData {
		let local_to_global = trs(transform.translation, transform.rotation, transform.scale);
		SurfaceData { local_to_global }
	}
}

impl Drop for Surface {
	fn drop(&mut self) {
		self.renderer.surfaces().remove(self.id);
	}
}
