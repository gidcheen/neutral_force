use crate::device::Device;
use crate::layout::Layout;
use crate::mesh::Vertex;
use algebra::Vector3f;
use std::mem::size_of;
use std::ptr::*;
use std::sync::*;
use ui_sys::*;

pub struct Pipeline {
	device: Arc<Device>,
	layout: Arc<Layout>,

	descriptor_set_layout: VkDescriptorSetLayout,
	pipeline_layout: VkPipelineLayout,
	pipeline: VkPipeline,
}

impl Pipeline {
	pub(crate) fn new(device: Arc<Device>, layout: Arc<Layout>, texture_count: u32, transparent: bool, vert_data: &[u8], frag_data: &[u8]) -> Self {
		unsafe {
			let bindings = [
				// material
				VkDescriptorSetLayoutBinding {
					binding: 10,
					descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					descriptorCount: 1,
					stageFlags: VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT as _,
					..Default::default()
				},
				// textures
				VkDescriptorSetLayoutBinding {
					binding: 11,
					descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
					descriptorCount: texture_count,
					stageFlags: VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT as _,
					..Default::default()
				},
			];

			let descriptor_set_layout_ci = VkDescriptorSetLayoutCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
				bindingCount: bindings.len() as _,
				pBindings: bindings.as_ptr(),
				..Default::default()
			};

			let mut descriptor_set_layout = null_mut();
			vkCreateDescriptorSetLayout(device.device(), &descriptor_set_layout_ci, null(), &mut descriptor_set_layout);

			let push_constants = [VkPushConstantRange {
				stageFlags: VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT as u32 | VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT as u32,
				offset: 0,
				size: (size_of::<u32>() * 7) as _,
				..Default::default()
			}];

			let descriptor_set_layouts = [layout.descriptor_set_layout(), descriptor_set_layout];

			let pipeline_layout_ci = VkPipelineLayoutCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
				setLayoutCount: descriptor_set_layouts.len() as _,
				pSetLayouts: descriptor_set_layouts.as_ptr(),
				pushConstantRangeCount: push_constants.len() as _,
				pPushConstantRanges: push_constants.as_ptr(),
				..Default::default()
			};

			let mut pipeline_layout = null_mut();
			vkCreatePipelineLayout(device.device(), &pipeline_layout_ci, null(), &mut pipeline_layout);

			let vert_shader_module_ci = VkShaderModuleCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
				codeSize: vert_data.len() as _,
				pCode: vert_data.as_ptr().cast(),
				..Default::default()
			};
			let mut vert_shader_module = null_mut();
			vkCreateShaderModule(device.device(), &vert_shader_module_ci, null(), &mut vert_shader_module);

			let frag_shader_module_ci = VkShaderModuleCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
				codeSize: frag_data.len() as _,
				pCode: frag_data.as_ptr().cast(),
				..Default::default()
			};
			let mut frag_shader_module = null_mut();
			vkCreateShaderModule(device.device(), &frag_shader_module_ci, null(), &mut frag_shader_module);

			let shader_stages = [
				VkPipelineShaderStageCreateInfo {
					sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
					stage: VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT,
					module: vert_shader_module,
					pName: b"main\0".as_ptr() as _,
					..Default::default()
				},
				VkPipelineShaderStageCreateInfo {
					sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
					stage: VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT,
					module: frag_shader_module,
					pName: b"main\0".as_ptr() as _,
					..Default::default()
				},
			];

			let vertex_binding_descriptions = [VkVertexInputBindingDescription {
				binding: 0,
				stride: size_of::<Vertex>() as _,
				inputRate: VkVertexInputRate::VK_VERTEX_INPUT_RATE_VERTEX,
			}];

			let vertex_attribute_descriptions = [
				VkVertexInputAttributeDescription {
					location: 0,
					binding: 0,
					format: VkFormat::VK_FORMAT_R32G32B32_SFLOAT,
					offset: size_of::<Vector3f>() as u32 * 0,
				},
				VkVertexInputAttributeDescription {
					location: 1,
					binding: 0,
					format: VkFormat::VK_FORMAT_R32G32B32_SFLOAT,
					offset: size_of::<Vector3f>() as u32 * 1,
				},
				VkVertexInputAttributeDescription {
					location: 2,
					binding: 0,
					format: VkFormat::VK_FORMAT_R32G32B32_SFLOAT,
					offset: size_of::<Vector3f>() as u32 * 2,
				},
				VkVertexInputAttributeDescription {
					location: 3,
					binding: 0,
					format: VkFormat::VK_FORMAT_R32G32B32_SFLOAT,
					offset: size_of::<Vector3f>() as u32 * 3,
				},
				VkVertexInputAttributeDescription {
					location: 4,
					binding: 0,
					format: VkFormat::VK_FORMAT_R32G32B32_SFLOAT,
					offset: size_of::<Vector3f>() as u32 * 4,
				},
				VkVertexInputAttributeDescription {
					location: 5,
					binding: 0,
					format: VkFormat::VK_FORMAT_R32G32_SFLOAT,
					offset: size_of::<Vector3f>() as u32 * 5,
				},
			];

			let vertex_input_state = VkPipelineVertexInputStateCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
				vertexBindingDescriptionCount: vertex_binding_descriptions.len() as _,
				pVertexBindingDescriptions: vertex_binding_descriptions.as_ptr(),
				vertexAttributeDescriptionCount: vertex_attribute_descriptions.len() as _,
				pVertexAttributeDescriptions: vertex_attribute_descriptions.as_ptr(),
				..Default::default()
			};

			let input_assembly_state = VkPipelineInputAssemblyStateCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
				topology: VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
				..Default::default()
			};

			let view_ports = [VkViewport {
				x: 0.0,
				y: 0.0,
				width: 1.0,
				height: 1.0,
				minDepth: 0.0,
				maxDepth: 1.0,
			}];

			let scissors = [VkRect2D {
				offset: VkOffset2D { x: 0, y: 0 },
				extent: VkExtent2D { width: 1, height: 1 },
			}];

			let viewport_state = VkPipelineViewportStateCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
				viewportCount: view_ports.len() as _,
				pViewports: view_ports.as_ptr(),
				scissorCount: scissors.len() as _,
				pScissors: scissors.as_ptr(),
				..Default::default()
			};

			let rasterization_state = VkPipelineRasterizationStateCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
				polygonMode: VkPolygonMode::VK_POLYGON_MODE_FILL,
				cullMode: VkCullModeFlagBits::VK_CULL_MODE_FRONT_BIT as _,
				frontFace: VkFrontFace::VK_FRONT_FACE_CLOCKWISE,
				lineWidth: 1.0,
				..Default::default()
			};

			let multisample_state = VkPipelineMultisampleStateCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
				rasterizationSamples: VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT,
				..Default::default()
			};

			let depth_stencil_state = VkPipelineDepthStencilStateCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
				depthTestEnable: VK_TRUE,
				depthWriteEnable: if transparent { VK_FALSE } else { VK_TRUE },
				depthCompareOp: VkCompareOp::VK_COMPARE_OP_LESS_OR_EQUAL,
				depthBoundsTestEnable: 0,
				stencilTestEnable: 0,
				front: VkStencilOpState {
					compareOp: VkCompareOp::VK_COMPARE_OP_ALWAYS,
					..Default::default()
				},
				back: VkStencilOpState {
					compareOp: VkCompareOp::VK_COMPARE_OP_ALWAYS,
					..Default::default()
				},
				minDepthBounds: 0.0,
				maxDepthBounds: 1.0,
				..Default::default()
			};

			let color_blend_attachments = [VkPipelineColorBlendAttachmentState {
				blendEnable: if transparent { VK_TRUE } else { VK_FALSE },
				srcColorBlendFactor: VkBlendFactor::VK_BLEND_FACTOR_SRC_ALPHA,
				dstColorBlendFactor: VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
				colorBlendOp: VkBlendOp::VK_BLEND_OP_ADD,
				srcAlphaBlendFactor: VkBlendFactor::VK_BLEND_FACTOR_ONE,
				dstAlphaBlendFactor: VkBlendFactor::VK_BLEND_FACTOR_ZERO,
				alphaBlendOp: VkBlendOp::VK_BLEND_OP_ADD,
				colorWriteMask: VkColorComponentFlagBits::VK_COLOR_COMPONENT_R_BIT as u32
					| VkColorComponentFlagBits::VK_COLOR_COMPONENT_G_BIT as u32
					| VkColorComponentFlagBits::VK_COLOR_COMPONENT_B_BIT as u32
					| VkColorComponentFlagBits::VK_COLOR_COMPONENT_A_BIT as u32,
			}];

			let color_blend_state = VkPipelineColorBlendStateCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
				attachmentCount: color_blend_attachments.len() as _,
				pAttachments: color_blend_attachments.as_ptr(),
				..Default::default()
			};

			let dynamic_states = [VkDynamicState::VK_DYNAMIC_STATE_VIEWPORT, VkDynamicState::VK_DYNAMIC_STATE_SCISSOR];

			let dynamic_state = VkPipelineDynamicStateCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
				dynamicStateCount: dynamic_states.len() as _,
				pDynamicStates: dynamic_states.as_ptr(),
				..Default::default()
			};

			let pipeline_ci = VkGraphicsPipelineCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
				stageCount: shader_stages.len() as _,
				pStages: shader_stages.as_ptr(),
				pVertexInputState: &vertex_input_state,
				pInputAssemblyState: &input_assembly_state,
				pViewportState: &viewport_state,
				pRasterizationState: &rasterization_state,
				pMultisampleState: &multisample_state,
				pDepthStencilState: &depth_stencil_state,
				pColorBlendState: &color_blend_state,
				pDynamicState: &dynamic_state,
				layout: pipeline_layout,
				renderPass: layout.render_pass(),
				..Default::default()
			};

			let mut pipeline = null_mut();
			vkCreateGraphicsPipelines(device.device(), null_mut(), 1, &pipeline_ci, null(), &mut pipeline);

			vkDestroyShaderModule(device.device(), vert_shader_module, null());
			vkDestroyShaderModule(device.device(), frag_shader_module, null());

			Self {
				device,
				layout,
				descriptor_set_layout,
				pipeline_layout,
				pipeline,
			}
		}
	}

	pub(crate) fn descriptor_set_layout(&self) -> VkDescriptorSetLayout {
		self.descriptor_set_layout
	}
	pub(crate) fn pipeline_layout(&self) -> VkPipelineLayout {
		self.pipeline_layout
	}
	pub(crate) fn pipeline(&self) -> VkPipeline {
		self.pipeline
	}
}

impl Drop for Pipeline {
	fn drop(&mut self) {
		unsafe {
			vkDestroyDescriptorSetLayout(self.device.device(), self.descriptor_set_layout, null());
			vkDestroyPipelineLayout(self.device.device(), self.pipeline_layout, null());
			vkDestroyPipeline(self.device.device(), self.pipeline, null());
		}
	}
}
