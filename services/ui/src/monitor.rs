use crate::*;
use std::ffi::CStr;

#[derive(Clone, Copy)]
pub struct Monitor {
	monitor: *mut GLFWmonitor,
}

impl Monitor {
	pub(crate) fn new(monitor: *mut GLFWmonitor) -> Self {
		Self { monitor }
	}

	pub(crate) fn monitor(&self) -> *mut GLFWmonitor {
		self.monitor
	}

	pub fn get_monitor_size(&self) -> (u32, u32) {
		unsafe {
			let video_mode = &*glfwGetVideoMode(self.monitor);
			(video_mode.width as _, video_mode.height as _)
		}
	}

	pub fn get_monitor_position(&self) -> (u32, u32) {
		unsafe {
			let mut x = 0;
			let mut y = 0;
			glfwGetMonitorPos(self.monitor, &mut x, &mut y);
			(x as _, y as _)
		}
	}

	pub fn name(&self) -> String {
		unsafe { CStr::from_ptr(glfwGetMonitorName(self.monitor)).to_str().unwrap().to_owned() }
	}
}
