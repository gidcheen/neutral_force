mod shader;
mod material;
mod model;
mod particles;
mod image;
mod skybox;
mod sprite;
mod font;
mod effect;
mod data;

use std::mem::size_of;
use std::rc::Rc;
pub use shader::*;
pub use material::*;
pub use model::*;
pub use particles::*;
pub use image::*;
pub use skybox::*;
pub use sprite::*;
pub use font::*;
pub use effect::*;
pub use data::*;

use storage::*;
use algebra::*;
use graphics::*;
pub use graphics::window::*;
pub use graphics::input::*;
pub use graphics::Graphics;

pub struct Renderer {
	graphics: Rc<Graphics>,

	nodes: Data<Node>,

	cameras: Data<Camera>,
	lights: Data<Light>,
	surfaces: Data<Surface>,

	layouts: Data<Layout>,
	buttons: Data<LayoutElement>,

	transforms: Data<Matrix4f>,
	transforms_buffer: Rc<Buffer>,
}

pub struct Node {
	transform: Matrix4f,
}

pub struct Camera {
	kind: CameraKind,
	global_to_local: Id<Matrix4f>,
	local_to_global: Id<Matrix4f>,
	global_to_projection: Id<Matrix4f>,
}
pub enum CameraKind {
	Perspective { fov: f32 },
	Orthographic { height: f32 },
}

pub struct Light {
	kind: LightKind,
	color: Color,
	global_to_local: Id<Matrix4f>,
	local_to_global: Id<Matrix4f>,
	global_to_projection: Id<Matrix4f>,
}
pub enum LightKind {
	Ambient { sky: Skybox },
	Directional {},
	Point {},
	Spot { inner: f32, outer: f32 },
}

pub struct Surface {
	node: Id<Node>,
	local_to_global: Id<Matrix4f>,
}

pub struct Layout {
	node: Id<Node>,
	size: Size,
	kind: LayoutKind,
}
pub enum LayoutKind {
	Vertical { width: f32 },
	Horizontal { height: f32 },
	Grid { width: f32, height: f32, columns: f32, rows: f32 },
}

pub struct LayoutElement {
	layout: Id<Layout>,
	kind: ElementKind,
}
pub enum ElementKind {
	Empty,
	Pic { bg: Sprite },
	Button { text: String, bg: Sprite },
	Layout(Layout),
}

#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct Color {
	pub r: f32,
	pub g: f32,
	pub b: f32,
	pub a: f32,
}

#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct Size {
	pub width: f32,
	pub height: f32,
}

impl Renderer {
	pub fn new(graphics: Rc<Graphics>) -> Self {
		Self {
			graphics: graphics.clone(),
			nodes: Data::new(1024),
			cameras: Data::new(1024),
			lights: Data::new(1024),
			surfaces: Data::new(1024),
			layouts: Data::new(1024),
			buttons: Data::new(1024),
			transforms: Data::new(1024),
			transforms_buffer: Buffer::new(graphics, 1024 * size_of::<Matrix4f>(), BufferKind::Storage),
		}
	}

	pub fn node(&mut self, t: Vector3f, r: Quaternionf, s: Vector3f, parent: Option<Id<Node>>) -> Id<Node> {
		let transform = if let Some(parent) = parent {
			self.transforms[*parent] * trs(t, r, s)
		} else {
			trs(t, r, s)
		};

		self.nodes.add(Node { transform })
	}

	pub fn render(&mut self) {
		self.nodes.clear();
		self.cameras.clear();
		self.lights.clear();
		self.surfaces.clear();
		self.layouts.clear();
		self.buttons.clear();
		self.transforms.clear();
	}
}
