pub struct Widget {
	kind: WidgetKind,
}

pub struct WidgetTransform {
	anchor: Alignment,
	position: Alignment,
	size: Alignment,
}

pub enum Alignment {
	Absolut { x: f32, y: f32 },
	Relative { x: f32, y: f32 },
}

pub enum WidgetKind {
	Layout(Layout),
	Color { r: f32, g: f32, b: f32, a: f32 },
	Image {},
	Button {},
}

pub enum Layout {
	Grid(Vec<Widget>),
	Table(Vec<Vec<Widget>>),
	Horizontal(Vec<Widget>),
	Vertical(Vec<Widget>),
}
