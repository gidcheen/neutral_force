use crate::device::Device;
use std::collections::HashMap;
use std::ptr::*;
use std::sync::Arc;
use std::sync::Mutex;
use ui_sys::*;

pub(crate) struct Allocator {
	device: Arc<Device>,
	bucket_size: VkDeviceSize,
	buckets: Mutex<Vec<Bucket>>, // todo RwLock
	buffers: Mutex<HashMap<VkBuffer, Binding>>,
	images: Mutex<HashMap<VkImage, Binding>>,
}

struct Bucket {
	memory: VkDeviceMemory,
	memory_index: u32,
	allocations: Vec<Allocation>,
}

struct Allocation {
	offset: VkDeviceSize,
	size: VkDeviceSize,
}

struct Binding {
	memory: VkDeviceMemory,
	offset: VkDeviceSize,
}

// todo
// enum MemoryType {
// 	Host,
// 	Shared,
// 	Device,
// }

impl Allocator {
	pub(crate) fn new(device: Arc<Device>, bucket_size: usize) -> Self {
		Self {
			device,
			bucket_size: bucket_size as _,
			buckets: Mutex::new(Vec::new()),
			buffers: Mutex::new(HashMap::new()),
			images: Mutex::new(HashMap::new()),
		}
	}

	pub(crate) fn allocate_buffer(&self, buffer: VkBuffer) {
		let mut requirements = Default::default();
		unsafe { vkGetBufferMemoryRequirements(self.device.device(), buffer, &mut requirements) };
		let binding = self.allocate(requirements);
		unsafe { vkBindBufferMemory(self.device.device(), buffer, binding.memory, binding.offset) }.success();
		let mut buffers = self.buffers.lock().unwrap();
		buffers.insert(buffer, binding);
	}
	pub(crate) fn allocate_image(&self, image: VkImage) {
		let mut requirements = Default::default();
		unsafe { vkGetImageMemoryRequirements(self.device.device(), image, &mut requirements) };
		let binding = self.allocate(requirements);
		unsafe { vkBindImageMemory(self.device.device(), image, binding.memory, binding.offset) }.success();
		let mut images = self.images.lock().unwrap();
		images.insert(image, binding);
	}

	pub(crate) fn deallocate_buffer(&self, buffer: VkBuffer) {
		let binding = {
			let mut buffers = self.buffers.lock().unwrap();
			buffers.remove(&buffer).unwrap()
		};
		self.deallocate(binding);
	}
	pub(crate) fn deallocate_image(&self, image: VkImage) {
		let binding = {
			let mut images = self.images.lock().unwrap();
			images.remove(&image).unwrap()
		};
		self.deallocate(binding);
	}

	fn allocate(&self, requirements: VkMemoryRequirements) -> Binding {
		let memory_index = self
			.device
			.memory_index(requirements.memoryTypeBits, VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT as _);

		let mut buckets = self.buckets.lock().unwrap();
		for bucket in buckets.iter_mut().filter(|b| b.memory_index == memory_index) {
			for i in 0..bucket.allocations.len() {
				let start_unaligned = bucket.allocations[i].offset + bucket.allocations[i].size;
				let alignment = requirements.alignment;
				let alignment_offset = start_unaligned % alignment;
				let start = start_unaligned + if alignment_offset > 0 { alignment - alignment_offset } else { 0 };
				let end = bucket.allocations.get(i + 1).map(|a| a.offset).unwrap_or(self.bucket_size);

				if end > start && end - start >= requirements.size as _ {
					let allocation = Allocation {
						offset: start,
						size: requirements.size as _,
					};
					bucket.allocations.insert(i + 1, allocation);

					return Binding {
						memory: bucket.memory,
						offset: start as _,
					};
				}
			}
		}
		// no spot found

		// overcommitment
		let size = self.bucket_size.max(requirements.size);

		let mut memory = null_mut();
		let memory_ai = VkMemoryAllocateInfo {
			sType: VkStructureType::VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO as _,
			allocationSize: size,
			memoryTypeIndex: memory_index,
			..Default::default()
		};
		unsafe { vkAllocateMemory(self.device.device(), &memory_ai, null(), &mut memory) }.success();

		buckets.push(Bucket {
			memory,
			memory_index,
			allocations: vec![Allocation { offset: 0, size }],
		});

		Binding { memory, offset: 0 }
	}

	fn deallocate(&self, binding: Binding) {
		let mut buckets = self.buckets.lock().unwrap();
		let bucket_index = buckets.iter().position(|b| b.memory == binding.memory).unwrap();
		buckets[bucket_index].allocations.retain(|a| a.offset != binding.offset);
		if buckets[bucket_index].allocations.is_empty() {
			let bucket = buckets.swap_remove(bucket_index);
			unsafe { vkFreeMemory(self.device.device(), bucket.memory, null()) }
		}
	}
}

impl Drop for Allocator {
	fn drop(&mut self) {
		let buckets = self.buckets.lock().unwrap();
		assert!(buckets.is_empty())
	}
}
