mod allocator;
mod data;
mod device;
mod layout;
mod renderer;
mod transfer;

pub mod frame;
pub mod material;
pub mod mesh;
pub mod monitor;
pub mod pipeline;
pub mod scene;
pub mod texture;
pub mod window;

use allocator::*;
use device::*;
use frame::*;
use layout::*;
use material::*;
use mesh::*;
use monitor::*;
use pipeline::*;
use renderer::*;
use scene::*;
use std::sync::*;
use texture::*;
use transfer::*;
use ui_sys::*;
use window::*;

static mut DEVICE: Option<Device> = None;

#[derive(Clone)]
pub struct Ui {
	device: Arc<Device>,
	allocator_s: Arc<Allocator>,
	allocator_l: Arc<Allocator>,
	transfer: Arc<Transfer>,
	layout: Arc<Layout>,
	renderer: Arc<Renderer>,
}

impl Ui {
	pub fn new(app_name: &str) -> Self {
		let device = Arc::new(Device::new(app_name));
		let allocator_s = Arc::new(Allocator::new(device.clone(), 4096));
		let allocator_l = Arc::new(Allocator::new(device.clone(), 4096 * 4096 * 4 * 2));
		let transfer = Arc::new(Transfer::new(device.clone(), 4096 * 4096 * 4));
		let layout = Arc::new(Layout::new(device.clone()));
		let renderer = Arc::new(Renderer::new(device.clone(), layout.clone()));

		Self {
			device,
			allocator_s,
			allocator_l,
			transfer,
			layout,
			renderer,
		}
	}

	pub fn poll_events(&self) {
		unsafe { glfwPollEvents() }
	}

	pub fn wait_events(&self) {
		unsafe { glfwWaitEvents() }
	}

	pub fn primary_monitor(&self) -> Monitor {
		unsafe { Monitor::new(glfwGetPrimaryMonitor()) }
	}

	pub fn all_monitors(&self) -> Vec<Monitor> {
		unsafe {
			let mut count = 0;
			let monitors = glfwGetMonitors(&mut count);
			(0..count as usize).map(|i| Monitor::new(*monitors.add(i))).collect()
		}
	}

	pub fn window(&self, title: &str, size: WindowSize, state: WindowState, events: Events) -> Window {
		Window::new(self.device.clone(), title, size, state, events)
	}

	pub fn material<T>(&self, data: &[T]) -> Material {
		Material::new(self.device.clone(), self.allocator_s.clone(), self.transfer.clone(), data)
	}

	pub fn texture<T>(&self, data: &[T], width: u32, height: u32) -> Texture {
		Texture::new(self.device.clone(), self.allocator_l.clone(), self.transfer.clone(), data, width, height)
	}

	pub fn mesh(&self, vertices: &[Vertex], indices: &[Index]) -> Mesh {
		Mesh::new(self.device.clone(), self.allocator_l.clone(), self.transfer.clone(), vertices, indices)
	}

	pub fn pipeline(&self, texture_count: u32, transparent: bool, vert_data: &[u8], frag_data: &[u8]) -> Pipeline {
		Pipeline::new(self.device.clone(), self.layout.clone(), texture_count, transparent, vert_data, frag_data)
	}

	pub fn frame(&self, width: u32, height: u32) -> Frame {
		Frame::new(self.device.clone(), self.allocator_l.clone(), self.layout.clone(), width, height)
	}

	pub fn camera(&self, transform: Transform, config: CameraConfig) -> Camera {
		Camera::new(self.renderer.clone(), transform, config)
	}

	pub fn light(&self, transform: Transform, config: LightConfig) -> Light {
		Light::new(self.renderer.clone(), transform, config)
	}

	pub fn surface(&self, transform: Transform) -> Surface {
		Surface::new(self.renderer.clone(), transform)
	}
}
