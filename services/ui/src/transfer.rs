use crate::device::Device;
use std::ffi::c_void;
use std::mem::size_of;
use std::mem::zeroed;
use std::ptr::*;
use std::sync::*;
use ui_sys::*;

pub(crate) struct Transfer {
	device: Arc<Device>,

	stage_size: VkDeviceSize,
	stage: Mutex<Stage>,
}

struct Stage {
	command_pool: VkCommandPool,
	fence: VkFence,
	buffer: VkBuffer,
	memory: VkDeviceMemory,
	ptr: *mut c_void,
}

impl Transfer {
	pub fn new(device: Arc<Device>, stage_size: usize) -> Self {
		let stage_size = stage_size as VkDeviceSize;
		unsafe {
			let (_, queue_index) = device.queue(VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT as _);

			let command_pool_ci = VkCommandPoolCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
				queueFamilyIndex: queue_index,
				..Default::default()
			};
			let mut command_pool = null_mut();
			vkCreateCommandPool(device.device(), &command_pool_ci, null(), &mut command_pool);

			let fence_ci = VkFenceCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
				..Default::default()
			};
			let mut fence = null_mut();
			vkCreateFence(device.device(), &fence_ci, null(), &mut fence);

			let buffer_ci = VkBufferCreateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
				size: stage_size,
				usage: VkBufferUsageFlagBits::VK_BUFFER_USAGE_TRANSFER_SRC_BIT as _,
				..Default::default()
			};
			let mut buffer = null_mut();
			vkCreateBuffer(device.device(), &buffer_ci, null(), &mut buffer);

			let mut memory_requirements = zeroed();
			vkGetBufferMemoryRequirements(device.device(), buffer, &mut memory_requirements);

			let memory_index = device.memory_index(
				memory_requirements.memoryTypeBits,
				VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_CACHED_BIT as VkMemoryPropertyFlags
					| VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_COHERENT_BIT as VkMemoryPropertyFlags,
			);

			let memory_ai = VkMemoryAllocateInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
				allocationSize: memory_requirements.size,
				memoryTypeIndex: memory_index,
				..Default::default()
			};
			let mut memory = null_mut();
			vkAllocateMemory(device.device(), &memory_ai, null(), &mut memory);
			vkBindBufferMemory(device.device(), buffer, memory, 0);

			let mut ptr = null_mut();
			vkMapMemory(device.device(), memory, 0, stage_size, 0, &mut ptr);

			Self {
				device,
				stage_size,
				stage: Mutex::new(Stage {
					command_pool,
					fence,
					buffer,
					memory,
					ptr,
				}),
			}
		}
	}

	pub(crate) fn transfer_buffer<T>(&self, buffer: VkBuffer, data: &[T], offset: VkDeviceSize) {
		let size = (data.len() * size_of::<T>()) as VkDeviceSize;
		let ptr = data.as_ptr() as *const c_void;
		assert!(size <= self.stage_size);

		unsafe {
			let stage = self.stage.lock().unwrap();
			copy_nonoverlapping(ptr, stage.ptr, size as _);
			let command_buffer = self.begin_command_buffer(&stage);
			let buffer_copy = VkBufferCopy {
				srcOffset: 0,
				dstOffset: offset,
				size,
			};
			vkCmdCopyBuffer(command_buffer, stage.buffer, buffer, 1, &buffer_copy);
			self.end_command_buffer(&stage, command_buffer);
		}
	}

	pub(crate) fn transfer_image<T>(&self, image: VkImage, data: &[T], offset: VkOffset3D, extent: VkExtent3D, layer_offset: u32, layer_count: u32, layout: VkImageLayout) {
		let size = (data.len() * size_of::<T>()) as VkDeviceSize;
		let ptr = data.as_ptr() as *const c_void;
		assert!(size <= self.stage_size);

		unsafe {
			let stage = self.stage.lock().unwrap();
			copy_nonoverlapping(ptr, stage.ptr, size as _);
			let command_buffer = self.begin_command_buffer(&stage);

			let (_, queue_index) = self.device.queue(VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT as _);
			let barrier_pre = VkImageMemoryBarrier {
				sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
				srcAccessMask: 0,
				dstAccessMask: VkAccessFlagBits::VK_ACCESS_TRANSFER_WRITE_BIT as _,
				oldLayout: VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
				newLayout: VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				srcQueueFamilyIndex: queue_index,
				dstQueueFamilyIndex: queue_index,
				image,
				subresourceRange: VkImageSubresourceRange {
					aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as _,
					baseMipLevel: 0,
					levelCount: 1,
					baseArrayLayer: layer_offset,
					layerCount: layer_count,
				},
				..Default::default()
			};
			vkCmdPipelineBarrier(
				command_buffer,
				VkPipelineStageFlagBits::VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT as _,
				VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TRANSFER_BIT as _,
				0,
				0,
				null(),
				0,
				null(),
				1,
				&barrier_pre,
			);
			let buffer_image_copy = VkBufferImageCopy {
				bufferOffset: 0,
				imageSubresource: VkImageSubresourceLayers {
					aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as _,
					layerCount: 1,
					..Default::default()
				},
				imageOffset: offset,
				imageExtent: extent,
				..Default::default()
			};
			vkCmdCopyBufferToImage(
				command_buffer,
				stage.buffer,
				image,
				VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				1,
				&buffer_image_copy,
			);
			let barrier_post = VkImageMemoryBarrier {
				sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
				srcAccessMask: 0,
				dstAccessMask: VkAccessFlagBits::VK_ACCESS_SHADER_READ_BIT as _,
				oldLayout: VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				newLayout: layout,
				srcQueueFamilyIndex: queue_index,
				dstQueueFamilyIndex: queue_index,
				image,
				subresourceRange: VkImageSubresourceRange {
					aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as _,
					baseMipLevel: 0,
					levelCount: 1,
					baseArrayLayer: layer_offset,
					layerCount: layer_count,
				},
				..Default::default()
			};
			vkCmdPipelineBarrier(
				command_buffer,
				VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TRANSFER_BIT as _,
				VkPipelineStageFlagBits::VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT as _,
				0,
				0,
				null(),
				0,
				null(),
				1,
				&barrier_post,
			);

			self.end_command_buffer(&stage, command_buffer);
		}
	}

	unsafe fn begin_command_buffer(&self, stage: &Stage) -> VkCommandBuffer {
		let mut command_buffer = null_mut();
		let command_buffer_ai = VkCommandBufferAllocateInfo {
			sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
			commandPool: stage.command_pool,
			level: VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY,
			commandBufferCount: 1,
			..Default::default()
		};
		vkAllocateCommandBuffers(self.device.device(), &command_buffer_ai, &mut command_buffer);

		let command_buffer_bi = VkCommandBufferBeginInfo {
			sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
			..Default::default()
		};
		vkBeginCommandBuffer(command_buffer, &command_buffer_bi);
		command_buffer
	}

	unsafe fn end_command_buffer(&self, stage: &Stage, command_buffer: VkCommandBuffer) {
		vkEndCommandBuffer(command_buffer);
		let (queue, _) = self.device.queue(VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT as _);
		{
			let submit_info = VkSubmitInfo {
				sType: VkStructureType::VK_STRUCTURE_TYPE_SUBMIT_INFO,
				commandBufferCount: 1,
				pCommandBuffers: &command_buffer,
				..Default::default()
			};
			let queue = queue.lock().unwrap();
			vkQueueSubmit(*queue, 1, &submit_info, stage.fence).success();
		}

		vkWaitForFences(self.device.device(), 1, &stage.fence, VK_TRUE, u64::MAX);
		vkResetFences(self.device.device(), 1, &stage.fence);
		vkFreeCommandBuffers(self.device.device(), stage.command_pool, 1, &command_buffer);
	}
}

impl Drop for Transfer {
	fn drop(&mut self) {
		unsafe {
			let stage = self.stage.lock().unwrap();
			vkDestroyCommandPool(self.device.device(), stage.command_pool, null());
			vkDestroyFence(self.device.device(), stage.fence, null());
			vkDestroyBuffer(self.device.device(), stage.buffer, null());
			vkFreeMemory(self.device.device(), stage.memory, null());
		}
	}
}
