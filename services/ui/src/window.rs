use crate::device::Device;
use crate::monitor::Monitor;
pub use input::*;
use std::ffi::CString;
use std::ops::DerefMut;
use std::os::raw::c_int;
use std::pin::Pin;
use std::ptr;
use std::ptr::*;
use std::sync::*;
use ui_sys::*;

mod input;

pub struct Window {
	device: Arc<Device>,
	window: *mut GLFWwindow,
	events: Pin<Box<Events>>,

	surface: VkSurfaceKHR,
	swapchain: Mutex<Swapchain>,
}

pub struct Swapchain {
	swapchain: VkSwapchainKHR,
	extent: VkExtent2D,
}

pub enum WindowState {
	Windowed,
	Minimized,
	Maximized,
	Fullscreen(Monitor),
}

#[derive(Debug)]
pub struct WindowSize {
	pub width: u32,
	pub height: u32,
}

#[derive(Debug)]
pub struct WindowPosition {
	pub x: u32,
	pub y: u32,
}

pub struct Events {
	pub position: Box<dyn Fn(WindowPosition)>,
	// pub size: Box<dyn Fn(Size)>,
}

impl Window {
	pub(crate) fn new(device: Arc<Device>, title: &str, size: WindowSize, state: WindowState, events: Events) -> Self {
		unsafe {
			let c_title = CString::new(title).unwrap();

			let monitor = if let WindowState::Fullscreen(monitor) = &state {
				monitor.monitor()
			} else {
				ptr::null_mut()
			};

			if let WindowState::Maximized = &state {
				glfwWindowHint(GLFW_MAXIMIZED as _, GLFW_TRUE as _);
			}

			let window = glfwCreateWindow(size.width as _, size.height as _, c_title.as_ptr(), monitor, ptr::null_mut());
			assert!(!window.is_null());

			glfwWindowHint(GLFW_MAXIMIZED as _, GLFW_FALSE as _);

			let mut events = Box::pin(events);
			glfwSetWindowUserPointer(window, events.deref_mut().deref_mut() as *mut Events as *mut _);

			glfwSetWindowPosCallback(window, Some(Self::window_position_cb));
			// glfwSetWindowSizeCallback(window, Some(Self::window_size_cb));
			// glfwSetWindowMaximizeCallback(window, Some(Self::window_maximize_cb));
			// glfwSetWindowIconifyCallback(window, Some(Self::window_minimize_cb));
			// glfwSetMouseButtonCallback(window, Some(Self::mouse_button_cb));
			// glfwSetCursorPosCallback(window, Some(Self::mouse_position_cb));
			// glfwSetScrollCallback(window, Some(Self::mouse_scroll_cb));
			// glfwSetKeyCallback(window, Some(Self::keyboard_key_cb));
			// glfwSetCharCallback(window, Some(Self::keyboard_char_cb));

			let mut surface = null_mut();
			glfwCreateWindowSurface(device.instance(), window, null(), &mut surface).success();

			let (_, queue_index) = device.queue(VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT as _);

			let mut is_supported = VK_FALSE;
			vkGetPhysicalDeviceSurfaceSupportKHR(device.physical_device(), queue_index, surface, &mut is_supported).success();
			if is_supported == VK_FALSE {
				panic!("not supported");
			}

			let swapchain = Self::create_swapchain(device.clone(), surface, null_mut());

			Self {
				device,
				window,
				events,
				surface,
				swapchain: Mutex::new(swapchain),
			}
		}
	}

	pub(crate) fn window(&self) -> *mut GLFWwindow {
		self.window
	}

	pub fn should_close(&self) -> bool {
		unsafe { glfwWindowShouldClose(self.window) == GLFW_TRUE as _ }
	}

	pub fn close(&self) {
		unsafe {
			glfwSetWindowShouldClose(self.window, GLFW_TRUE as _);
		}
	}

	pub fn set_title(&self, title: &str) {
		unsafe {
			let title = CString::new(title).unwrap();
			glfwSetWindowTitle(self.window, title.as_ptr())
		};
	}

	pub fn size(&self) -> WindowSize {
		unsafe {
			let mut width = 0;
			let mut height = 0;
			glfwGetWindowSize(self.window, &mut width, &mut height);
			WindowSize {
				width: width as _,
				height: height as _,
			}
		}
	}

	pub fn set_size(&self, size: WindowSize) {
		unsafe {
			glfwSetWindowSize(self.window, size.width as _, size.height as _);
		}
	}

	pub fn position(&self) -> WindowPosition {
		unsafe {
			let mut x = 0;
			let mut y = 0;
			glfwGetWindowPos(self.window, &mut x, &mut y);
			WindowPosition { x: x as _, y: y as _ }
		}
	}

	pub fn set_position(&self, position: WindowPosition) {
		unsafe {
			glfwSetWindowPos(self.window, position.x as _, position.y as _);
		}
	}

	// 	pub fn is_maximized(&self) -> bool {
	// 		unsafe { glfwGetWindowAttrib(self.window, GLFW_MAXIMIZED as _) != 0 }
	// 	}
	// 	pub fn is_minimized(&self) -> bool {
	// 		unsafe { glfwGetWindowAttrib(self.window, GLFW_ICONIFIED as _) != 0 }
	// 	}
	// 	pub fn is_restored(&self) -> bool {
	// 		unsafe {
	// 			glfwGetWindowAttrib(self.window, GLFW_ICONIFIED as _) == 0 && glfwGetWindowAttrib(self.window, GLFW_MAXIMIZED as _) == 0 && glfwGetWindowMonitor(self.window).is_null()
	// 		}
	// 	}
	// 	pub fn is_fullscreen(&self) -> bool {
	// 		unsafe { !glfwGetWindowMonitor(self.window).is_null() }
	// 	}

	// 	pub fn maximize(&self) {
	// 		unsafe {
	// 			glfwMaximizeWindow(self.window);
	// 		}
	// 	}
	// 	pub fn minimize(&self) {
	// 		unsafe {
	// 			glfwIconifyWindow(self.window);
	// 		}
	// 	}
	// 	pub fn restore(&self) {
	// 		unsafe {
	// 			glfwRestoreWindow(self.window);
	// 		}
	// 	}
	// 	pub fn make_fullscreen(&self, index: usize, size: Option<Size>) {
	// 		unsafe {
	// 			let mut count = 0;
	// 			let monitors = glfwGetMonitors(&mut count);
	// 			assert!(index < count as _);

	// 			let monitor = *monitors.add(index);

	// 			let mut x = 0;
	// 			let mut y = 0;
	// 			glfwGetMonitorPos(monitor, &mut x, &mut y);
	// 			let video_mode = &*glfwGetVideoMode(monitor);
	// 			let (width, height) = if let Some(size) = size {
	// 				(size.width as _, size.height as _)
	// 			} else {
	// 				(video_mode.width, video_mode.height)
	// 			};
	// 			glfwSetWindowMonitor(self.window, monitor, x, y, width, height, GLFW_DONT_CARE);
	// 		}
	// 	}
	// 	pub fn leave_fullscreen(&self, pos: Position, size: Size) {
	// 		unsafe {
	// 			if !glfwGetWindowMonitor(self.window).is_null() {
	// 				let x = pos.x as _;
	// 				let y = pos.y as _;
	// 				let width = size.width as _;
	// 				let height = size.height as _;
	// 				glfwRestoreWindow(self.window);
	// 				glfwSetWindowMonitor(self.window, null_mut(), x, y, width, height, GLFW_DONT_CARE);
	// 			}
	// 		}
	// 	}

	unsafe extern "C" fn window_position_cb(window: *mut GLFWwindow, x: c_int, y: c_int) {
		let events = Self::events_from_ptr(window);
		(*events.position)(WindowPosition { x: x as _, y: y as _ });
	}
	// 	unsafe extern "C" fn window_size_cb(window: *mut GLFWwindow, width: c_int, height: c_int) {
	// 		let events = Self::events_from_ptr(window);
	// 		events.borrow_mut().push(Event::WindowSize(Size {
	// 			width: width as _,
	// 			height: height as _,
	// 		}));
	// 	}
	// 	unsafe extern "C" fn window_maximize_cb(window: *mut GLFWwindow, maximized: c_int) {
	// 		let events = Self::events_from_ptr(window);
	// 		events.borrow_mut().push(Event::WindowMaximize(maximized == GLFW_TRUE as _));
	// 	}
	// 	unsafe extern "C" fn window_minimize_cb(window: *mut GLFWwindow, minimized: c_int) {
	// 		let events = Self::events_from_ptr(window);
	// 		events.borrow_mut().push(Event::WindowMinimize(minimized == GLFW_TRUE as _));
	// 	}
	// 	unsafe extern "C" fn mouse_button_cb(window: *mut GLFWwindow, button: c_int, action: c_int, _mods: c_int) {
	// 		let events = Self::events_from_ptr(window);
	// 		let button = match button as c_uint {
	// 			GLFW_MOUSE_BUTTON_LEFT => MouseButton::Left,
	// 			GLFW_MOUSE_BUTTON_RIGHT => MouseButton::Left,
	// 			GLFW_MOUSE_BUTTON_MIDDLE => MouseButton::Left,
	// 			GLFW_MOUSE_BUTTON_4 => MouseButton::Button4,
	// 			GLFW_MOUSE_BUTTON_5 => MouseButton::Button5,
	// 			GLFW_MOUSE_BUTTON_6 => MouseButton::Button6,
	// 			GLFW_MOUSE_BUTTON_7 => MouseButton::Button7,
	// 			GLFW_MOUSE_BUTTON_8 => MouseButton::Button8,
	// 			_ => return,
	// 		};
	// 		let state = match action as c_uint {
	// 			GLFW_PRESS => InputState::Down,
	// 			GLFW_RELEASE => InputState::Up,
	// 			_ => return,
	// 		};
	// 		events.borrow_mut().push(Event::MouseButton(button, state));
	// 	}
	// 	unsafe extern "C" fn mouse_position_cb(window: *mut GLFWwindow, x: f64, y: f64) {
	// 		let events = Self::events_from_ptr(window);
	// 		events.borrow_mut().push(Event::MousePosition(MousePosition { x, y }));
	// 	}
	// 	unsafe extern "C" fn mouse_scroll_cb(window: *mut GLFWwindow, x: f64, y: f64) {
	// 		let events = Self::events_from_ptr(window);
	// 		events.borrow_mut().push(Event::MouseScroll(MouseScroll { x, y }));
	// 	}
	// 	unsafe extern "C" fn keyboard_key_cb(window: *mut GLFWwindow, key: c_int, _scancode: c_int, action: c_int, _mods: c_int) {
	// 		let events = Self::events_from_ptr(window);
	// 		let key = match key as c_uint {
	// 			GLFW_KEY_SPACE => KeyboardKey::Space,
	// 			GLFW_KEY_APOSTROPHE => KeyboardKey::Apostrophe,
	// 			GLFW_KEY_COMMA => KeyboardKey::Comma,
	// 			GLFW_KEY_MINUS => KeyboardKey::Minus,
	// 			GLFW_KEY_PERIOD => KeyboardKey::Period,
	// 			GLFW_KEY_SLASH => KeyboardKey::Slash,
	// 			GLFW_KEY_0 => KeyboardKey::Key0,
	// 			GLFW_KEY_1 => KeyboardKey::Key1,
	// 			GLFW_KEY_2 => KeyboardKey::Key2,
	// 			GLFW_KEY_3 => KeyboardKey::Key3,
	// 			GLFW_KEY_4 => KeyboardKey::Key4,
	// 			GLFW_KEY_5 => KeyboardKey::Key5,
	// 			GLFW_KEY_6 => KeyboardKey::Key6,
	// 			GLFW_KEY_7 => KeyboardKey::Key7,
	// 			GLFW_KEY_8 => KeyboardKey::Key8,
	// 			GLFW_KEY_9 => KeyboardKey::Key9,
	// 			GLFW_KEY_SEMICOLON => KeyboardKey::Semicolon,
	// 			GLFW_KEY_EQUAL => KeyboardKey::Equal,
	// 			GLFW_KEY_A => KeyboardKey::KeyA,
	// 			GLFW_KEY_B => KeyboardKey::KeyB,
	// 			GLFW_KEY_C => KeyboardKey::KeyC,
	// 			GLFW_KEY_D => KeyboardKey::KeyD,
	// 			GLFW_KEY_E => KeyboardKey::KeyE,
	// 			GLFW_KEY_F => KeyboardKey::KeyF,
	// 			GLFW_KEY_G => KeyboardKey::KeyG,
	// 			GLFW_KEY_H => KeyboardKey::KeyH,
	// 			GLFW_KEY_I => KeyboardKey::KeyI,
	// 			GLFW_KEY_J => KeyboardKey::KeyJ,
	// 			GLFW_KEY_K => KeyboardKey::KeyK,
	// 			GLFW_KEY_L => KeyboardKey::KeyL,
	// 			GLFW_KEY_M => KeyboardKey::KeyM,
	// 			GLFW_KEY_N => KeyboardKey::KeyN,
	// 			GLFW_KEY_O => KeyboardKey::KeyO,
	// 			GLFW_KEY_P => KeyboardKey::KeyP,
	// 			GLFW_KEY_Q => KeyboardKey::KeyQ,
	// 			GLFW_KEY_R => KeyboardKey::KeyR,
	// 			GLFW_KEY_S => KeyboardKey::KeyS,
	// 			GLFW_KEY_T => KeyboardKey::KeyT,
	// 			GLFW_KEY_U => KeyboardKey::KeyU,
	// 			GLFW_KEY_V => KeyboardKey::KeyV,
	// 			GLFW_KEY_W => KeyboardKey::KeyW,
	// 			GLFW_KEY_X => KeyboardKey::KeyX,
	// 			GLFW_KEY_Y => KeyboardKey::KeyY,
	// 			GLFW_KEY_Z => KeyboardKey::KeyZ,
	// 			GLFW_KEY_LEFT_BRACKET => KeyboardKey::LeftBracket,
	// 			GLFW_KEY_BACKSLASH => KeyboardKey::BackSlash,
	// 			GLFW_KEY_RIGHT_BRACKET => KeyboardKey::RightBracket,
	// 			GLFW_KEY_GRAVE_ACCENT => KeyboardKey::GraveAccent,
	// 			GLFW_KEY_ESCAPE => KeyboardKey::Escape,
	// 			GLFW_KEY_ENTER => KeyboardKey::Enter,
	// 			GLFW_KEY_TAB => KeyboardKey::Tab,
	// 			GLFW_KEY_BACKSPACE => KeyboardKey::BackSpace,
	// 			GLFW_KEY_INSERT => KeyboardKey::Insert,
	// 			GLFW_KEY_DELETE => KeyboardKey::Delete,
	// 			GLFW_KEY_RIGHT => KeyboardKey::Right,
	// 			GLFW_KEY_LEFT => KeyboardKey::Left,
	// 			GLFW_KEY_DOWN => KeyboardKey::Down,
	// 			GLFW_KEY_UP => KeyboardKey::Up,
	// 			GLFW_KEY_PAGE_UP => KeyboardKey::PageUp,
	// 			GLFW_KEY_PAGE_DOWN => KeyboardKey::PageDown,
	// 			GLFW_KEY_HOME => KeyboardKey::Home,
	// 			GLFW_KEY_END => KeyboardKey::End,
	// 			GLFW_KEY_CAPS_LOCK => KeyboardKey::CapsLock,
	// 			GLFW_KEY_SCROLL_LOCK => KeyboardKey::ScrollLock,
	// 			GLFW_KEY_NUM_LOCK => KeyboardKey::NumLock,
	// 			GLFW_KEY_PRINT_SCREEN => KeyboardKey::Print,
	// 			GLFW_KEY_PAUSE => KeyboardKey::Pause,
	// 			GLFW_KEY_F1 => KeyboardKey::F1,
	// 			GLFW_KEY_F2 => KeyboardKey::F2,
	// 			GLFW_KEY_F3 => KeyboardKey::F3,
	// 			GLFW_KEY_F4 => KeyboardKey::F4,
	// 			GLFW_KEY_F5 => KeyboardKey::F5,
	// 			GLFW_KEY_F6 => KeyboardKey::F6,
	// 			GLFW_KEY_F7 => KeyboardKey::F7,
	// 			GLFW_KEY_F8 => KeyboardKey::F8,
	// 			GLFW_KEY_F9 => KeyboardKey::F9,
	// 			GLFW_KEY_F10 => KeyboardKey::F10,
	// 			GLFW_KEY_F11 => KeyboardKey::F11,
	// 			GLFW_KEY_F12 => KeyboardKey::F12,
	// 			GLFW_KEY_KP_0 => KeyboardKey::Num0,
	// 			GLFW_KEY_KP_1 => KeyboardKey::Num1,
	// 			GLFW_KEY_KP_2 => KeyboardKey::Num2,
	// 			GLFW_KEY_KP_3 => KeyboardKey::Num3,
	// 			GLFW_KEY_KP_4 => KeyboardKey::Num4,
	// 			GLFW_KEY_KP_5 => KeyboardKey::Num5,
	// 			GLFW_KEY_KP_6 => KeyboardKey::Num6,
	// 			GLFW_KEY_KP_7 => KeyboardKey::Num7,
	// 			GLFW_KEY_KP_8 => KeyboardKey::Num8,
	// 			GLFW_KEY_KP_9 => KeyboardKey::Num9,
	// 			GLFW_KEY_KP_DECIMAL => KeyboardKey::NumDecimal,
	// 			GLFW_KEY_KP_DIVIDE => KeyboardKey::NumDivide,
	// 			GLFW_KEY_KP_MULTIPLY => KeyboardKey::NumMultiply,
	// 			GLFW_KEY_KP_SUBTRACT => KeyboardKey::NumSubtract,
	// 			GLFW_KEY_KP_ADD => KeyboardKey::NumAdd,
	// 			GLFW_KEY_KP_ENTER => KeyboardKey::NumEnter,
	// 			GLFW_KEY_LEFT_SHIFT => KeyboardKey::LeftShift,
	// 			GLFW_KEY_LEFT_CONTROL => KeyboardKey::LeftControl,
	// 			GLFW_KEY_LEFT_ALT => KeyboardKey::LeftAlt,
	// 			GLFW_KEY_LEFT_SUPER => KeyboardKey::LeftSuper,
	// 			GLFW_KEY_RIGHT_SHIFT => KeyboardKey::RightShift,
	// 			GLFW_KEY_RIGHT_CONTROL => KeyboardKey::RightControl,
	// 			GLFW_KEY_RIGHT_ALT => KeyboardKey::RightAlt,
	// 			GLFW_KEY_RIGHT_SUPER => KeyboardKey::RightSuper,
	// 			_ => return,
	// 		};
	// 		let state = match action as c_uint {
	// 			GLFW_PRESS => InputState::Down,
	// 			GLFW_RELEASE => InputState::Up,
	// 			_ => return,
	// 		};
	// 		events.borrow_mut().push(Event::KeyboardKey(key, state));
	// 	}
	// 	unsafe extern "C" fn keyboard_char_cb(window: *mut GLFWwindow, code_point: c_uint) {
	// 		let events = Self::events_from_ptr(window);
	// 		events.borrow_mut().push(Event::KeyboardChar(char::from_u32(code_point).unwrap()));
	// 	}

	unsafe fn events_from_ptr<'a>(window: *mut GLFWwindow) -> &'a mut Events {
		let ptr = glfwGetWindowUserPointer(window) as *mut _;
		// assert!(!ptr.is_null());
		&mut *ptr
	}

	unsafe fn create_swapchain(device: Arc<Device>, surface: VkSurfaceKHR, old_swapchain: VkSwapchainKHR) -> Swapchain {
		let mut physical_device_surface_capabilities = VkSurfaceCapabilitiesKHR::default();
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device.physical_device(), surface, &mut physical_device_surface_capabilities);

		let mut surface_formats_count = 0;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device.physical_device(), surface, &mut surface_formats_count, null_mut());
		let mut surface_formats = vec![Default::default(); surface_formats_count as _];
		vkGetPhysicalDeviceSurfaceFormatsKHR(device.physical_device(), surface, &mut surface_formats_count, surface_formats.as_mut_ptr());

		let mut surface_format = surface_formats[0];
		for i in 0..surface_formats_count as usize {
			let format = surface_formats[i];
			if format.format == VkFormat::VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VkColorSpaceKHR::VK_COLOR_SPACE_SRGB_NONLINEAR_KHR {
				surface_format = format;
				break;
			}
		}

		let swapchain_ci = VkSwapchainCreateInfoKHR {
			sType: VkStructureType::VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
			surface,
			minImageCount: physical_device_surface_capabilities.minImageCount + 1,
			imageFormat: surface_format.format,
			imageColorSpace: surface_format.colorSpace,
			imageExtent: physical_device_surface_capabilities.currentExtent,
			imageArrayLayers: 1,
			imageUsage: VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_DST_BIT as _,
			imageSharingMode: VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
			preTransform: physical_device_surface_capabilities.currentTransform,
			compositeAlpha: VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
			presentMode: VkPresentModeKHR::VK_PRESENT_MODE_FIFO_KHR,
			clipped: VK_TRUE,
			oldSwapchain: old_swapchain,
			..Default::default()
		};

		let mut swapchain = null_mut();
		vkCreateSwapchainKHR(device.device(), &swapchain_ci, null(), &mut swapchain);
		Swapchain {
			swapchain,
			extent: physical_device_surface_capabilities.currentExtent,
		}
	}

	pub(crate) fn recreate(&self) {
		let mut swapchain = self.swapchain.lock().unwrap();
		*swapchain = unsafe { Self::create_swapchain(self.device.clone(), self.surface, swapchain.swapchain) };
	}
}

impl Drop for Window {
	fn drop(&mut self) {
		unsafe {
			let swapchain = self.swapchain.lock().unwrap();
			vkDestroySwapchainKHR(self.device.device(), swapchain.swapchain, null());
			vkDestroySurfaceKHR(self.device.instance(), self.surface, null());
			glfwDestroyWindow(self.window)
		}
	}
}
