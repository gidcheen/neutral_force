use crate::bounding_box::BoundingBox;
use algebra::*;

pub struct Frustum {
	planes: [FrustumPlane; 6],
}

struct FrustumPlane {
	normal: Vector3f,
	depth: f32,
}

impl Frustum {
	pub fn new(transform: Matrix4f) -> Self {
		let row0 = Vector4f::new(transform.row(0));
		let row1 = Vector4f::new(transform.row(1));
		let row2 = Vector4f::new(transform.row(2));
		let row3 = Vector4f::new(transform.row(3));

		let planes = [
			FrustumPlane::new(row3 - row0),
			FrustumPlane::new(row3 + row0),
			FrustumPlane::new(row3 - row1),
			FrustumPlane::new(row3 + row1),
			FrustumPlane::new(row3 - row2),
			FrustumPlane::new(row3 + row2),
		];

		Self {
			planes,
		}
	}

	pub fn set(&mut self, transform: Matrix4f) {
		*self = Self::new(transform);
	}

	pub fn contains(&self, bounding_box: BoundingBox) -> bool {
		let center = bounding_box.center();
		let radius = bounding_box.radius();

		for plane in &self.planes {
			let distance = center.dot(plane.normal) + plane.depth + radius;
			if distance < 0.0 {
				return false;
			}
		}
		return true;
	}
}

impl FrustumPlane {
	fn new(row: Vector4f) -> Self {
		let normal = Vector3f::new([row[0], row[1], row[2]]);
		let depth = row[3];
		let length = 1.0 / normal.length();
		Self {
			normal: normal * length,
			depth: depth * length,
		}
	}
}
