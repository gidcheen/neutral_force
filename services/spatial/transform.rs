use algebra::value::Val;
use algebra::*;

pub fn forward<T: Val>() -> Vector<T, 3> {
	Vector::new([T::ZERO, T::ZERO, T::ONE])
}

pub fn backward<T: Val>() -> Vector<T, 3> {
	Vector::new([T::ZERO, T::ZERO, -T::ONE])
}

pub fn left<T: Val>() -> Vector<T, 3> {
	Vector::new([T::ONE, T::ZERO, T::ZERO])
}

pub fn right<T: Val>() -> Vector<T, 3> {
	Vector::new([-T::ONE, T::ZERO, T::ZERO])
}

pub fn up<T: Val>() -> Vector<T, 3> {
	Vector::new([T::ZERO, T::ONE, T::ZERO])
}

pub fn down<T: Val>() -> Vector<T, 3> {
	Vector::new([T::ZERO, -T::ONE, T::ZERO])
}

pub fn orthographic<T: Val>(left: T, right: T, bottom: T, top: T, near: T, far: T) -> Matrix<T, 4, 4> {
	let mut ret = Matrix::zeroed();

	ret[0][0] = T::TWO / (right - left);
	ret[1][1] = T::TWO / (top - bottom);
	ret[2][2] = -T::TWO / (far - near);
	ret[3][3] = T::ONE;

	ret[3][0] = -(right + left) / (right - left);
	ret[3][1] = -(top + bottom) / (top - bottom);
	ret[3][2] = -(far + near) / (far - near);

	ret
}

pub fn perspective<T: Val>(fov: T, aspect: T, near: T, far: T) -> Matrix<T, 4, 4> {
	let mut ret = Matrix::zeroed();

	ret[0][0] = T::ONE / ((fov * T::HALF).tan() * aspect);
	ret[1][1] = -T::ONE / (fov * T::HALF).tan();
	ret[2][2] = -(far + near) / (far - near);

	ret[3][2] = -(T::TWO * far * near) / (far - near);
	ret[2][3] = -T::ONE;

	ret
}

pub fn translation<T: Val>(translation: Vector<T, 3>) -> Matrix<T, 4, 4> {
	let mut ret = Matrix::identity();

	ret[3][0] = translation.x();
	ret[3][1] = translation.y();
	ret[3][2] = translation.z();

	ret
}

pub fn rotation<T: Val>(rotation: Quaternion<T>) -> Matrix<T, 4, 4> {
	let mut ret = Matrix::identity();

	let x = rotation.x();
	let y = rotation.y();
	let z = rotation.z();
	let w = rotation.w();

	let x2 = x * x;
	let y2 = y * y;
	let z2 = z * z;

	ret[0][0] = T::ONE - T::TWO * y2 - T::TWO * z2;
	ret[0][1] = T::TWO * x * y + T::TWO * z * w;
	ret[0][2] = T::TWO * x * z - T::TWO * y * w;

	ret[1][0] = T::TWO * x * y - T::TWO * z * w;
	ret[1][1] = T::ONE - T::TWO * x2 - T::TWO * z2;
	ret[1][2] = T::TWO * y * z + T::TWO * x * w;

	ret[2][0] = T::TWO * x * z + T::TWO * y * w;
	ret[2][1] = T::TWO * y * z - T::TWO * x * w;
	ret[2][2] = T::ONE - T::TWO * x2 - T::TWO * y2;

	ret
}

pub fn scale<T: Val>(scale: Vector<T, 3>) -> Matrix<T, 4, 4> {
	let mut ret = Matrix::identity();

	ret[0][0] = scale.x();
	ret[1][1] = scale.y();
	ret[2][2] = scale.z();

	ret
}

pub fn trs<T: Val>(t: Vector<T, 3>, r: Quaternion<T>, s: Vector<T, 3>) -> Matrix<T, 4, 4> {
	translation(t) * rotation(r) * scale(s)
}

pub fn matrix_to_position<T: Val>(matrix: &Matrix<T, 4, 4>) -> Vector<T, 3> {
	Vector::new([matrix[3][0], matrix[3][1], matrix[3][2]])
}

pub fn matrix_to_rotation<T: Val>(matrix: &Matrix<T, 4, 4>) -> Quaternion<T> {
	let w = (T::ONE + matrix[0][0] + matrix[1][1] + matrix[2][2]).max(T::ZERO).sqrt() * T::HALF;
	let x = (T::ONE + matrix[0][0] - matrix[1][1] - matrix[2][2]).max(T::ZERO).sqrt() * T::HALF;
	let y = (T::ONE - matrix[0][0] + matrix[1][1] - matrix[2][2]).max(T::ZERO).sqrt() * T::HALF;
	let z = (T::ONE - matrix[0][0] - matrix[1][1] + matrix[2][2]).max(T::ZERO).sqrt() * T::HALF;

	let x = x.copysign(matrix[1][2] - matrix[2][1]);
	let y = y.copysign(matrix[2][0] - matrix[0][2]);
	let z = z.copysign(matrix[0][1] - matrix[1][0]);

	Quaternion::new([x, y, z, w])
}

pub fn matrix_to_scale<T: Val>(matrix: &Matrix<T, 4, 4>) -> Vector<T, 3> {
	Vector::new([matrix[0][0], matrix[1][1], matrix[2][2]])
}

pub fn quaternion_to_euler<T: Val>(q: &Quaternion<T>) -> Vector<T, 3> {
	let qx = q.x();
	let qy = q.y();
	let qz = q.z();
	let qw = q.w();

	let sinx = T::TWO * (qw * qx + qy * qz);
	let cosx = T::ONE - T::TWO * (qx * qx + qy * qy);
	let x = sinx.atan2(cosx);

	let siny = T::TWO * (qw * qy - qz * qx);
	let y = if siny.abs() >= T::ONE {
		(T::PI * T::HALF).copysign(siny)
	} else {
		siny.asin()
	};

	let sinz = T::TWO * (qw * qz + qx * qy);
	let cosz = T::ONE - T::TWO * (qy * qy + qz * qz);
	let z = sinz.atan2(cosz);

	Vector::new([x, y, z])
}

pub fn euler_to_quaternion<T: Val>(euler: &Vector<T, 3>) -> Quaternion<T> {
	let x = Quaternion::from_axis_angle(Vector::new([T::ZERO, T::ZERO, T::ONE]), euler.x());
	let y = Quaternion::from_axis_angle(Vector::new([T::ZERO, T::ONE, T::ZERO]), euler.y());
	let z = Quaternion::from_axis_angle(Vector::new([T::ONE, T::ZERO, T::ZERO]), euler.z());

	x * y * z
}
