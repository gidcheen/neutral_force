use algebra::*;

#[derive(Copy, Clone, Debug, Default)]
pub struct BoundingBox {
	pub min: Vector3f,
	pub max: Vector3f,
}

impl BoundingBox {
	pub fn min_max(min: Vector3f, max: Vector3f) -> Self {
		Self {
			min,
			max,
		}
	}

	pub fn size(&self) -> Vector3f {
		self.max - self.min
	}
	pub fn extend(&self) -> Vector3f {
		self.size() * 0.5
	}
	pub fn center(&self) -> Vector3f {
		self.min + self.extend()
	}
	pub fn radius(&self) -> f32 {
		self.extend().length()
	}
	pub fn radius_squared(&self) -> f32 {
		self.extend().length_squared()
	}

	pub fn is_inside(&self, point: Vector3f) -> bool {
		let x = point.x() >= self.min.x() && point.x() <= self.max.x();
		let y = point.y() >= self.min.y() && point.y() <= self.max.y();
		let z = point.z() >= self.min.z() && point.z() <= self.max.z();
		x && y && z
	}

	pub fn overlaps(&self, other: BoundingBox) -> bool {
		let x = self.min.x() <= other.max.x() && self.max.x() >= other.min.x();
		let z = self.min.y() <= other.max.y() && self.max.y() >= other.min.y();
		let y = self.min.z() <= other.max.z() && self.max.z() >= other.min.z();
		x && y && z
	}

	pub fn add_point(&mut self, point: Vector3f) {
		self.min = self.min.min(point);
		self.max = self.max.min(point);
	}

	pub fn add_bounding_box(&mut self, other: BoundingBox) {
		self.min = self.min.min(other.min);
		self.max = self.max.min(other.max);
	}

	pub fn transform(&mut self, transform: Matrix4f) {
		let mut corners = self.corners();
		for i in 0..8 {
			let corner = corners[i];
			let transformed = transform * Vector4f::new([corner.x(), corner.y(), corner.z(), 1.0]).to_matrix();
			corners[i] = Vector3f::new([transformed[0][0], transformed[0][1], transformed[0][2]]);
		}
		self.min = corners[0];
		self.max = corners[0];
		for i in 1..8 {
			self.min = self.min.min(corners[i]);
			self.max = self.max.max(corners[i]);
		}
	}

	pub fn transformed(mut self, transform: Matrix4f) -> Self {
		self.transform(transform);
		self
	}

	pub fn corners(&self) -> [Vector3f; 8] {
		let mut corners = [Vector3f::zeroed(); 8];
		let center = self.center();
		let extent = self.extend();

		for i in 0..8 {
			let corner_mult = Vector3f::new([
				if (i & (1 << 2)) == 0 {
					1.0
				} else {
					-1.0
				},
				if (i & (1 << 1)) == 0 {
					1.0
				} else {
					-1.0
				},
				if (i & (1 << 0)) == 0 {
					1.0
				} else {
					-1.0
				},
			]);
			corners[i] = (center + extent).scaled(corner_mult);
		}

		corners
	}
}
